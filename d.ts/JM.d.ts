
declare namespace jm {
    /**安全模式 */
    var SafeMode: boolean;
    //调试用的
    class ViewMgr {
        public static hideViewByKey(key: string, ...param);
        public static clearViewByKey(key: string);
        /**场景视图是否显示 */
        public static sceneIsShow(scene: any): boolean;
    }
    /**字体样式 */
    enum LogFontStyle {
        /**警告字体颜色*/
        Warn = 'color:#e6b74d',
        /**异常字体颜色 */
        Error = 'color:#f00',
        /**通用字体颜色 */
        Adopt = 'color:#0fe029',
        /**淡绿字体颜色 */
        LightGreen = 'color:#31dcad',
        /**弱体字体颜色 */
        Slight = 'color:#999999',
    }
    class Log {
        public static log(...param: any[]): void;
        public static warn(...param: any[]): void;
        public static error(...param: any[]): void;
        public static log_sys(...param: any[]): void;
        public static log_socket(...param: any[]): void;
    }
    /**
     * 事件对象
     */
    class ElementEvent {
        callback: Function;
        target: Object;
        isOnce: boolean;
        constructor(callback: Function, target: Object, isOnce?: boolean)
    }
    /**基础事件 */
    class BaseEvent {
        public hasEventListener(eventKey: string, callback: Function, target: Object): boolean;

        /**
         * 是否存在事件key
         * @param eventKey 
         */
        public hasEventKey(eventKey: string): boolean;
        /**
         * 监听事件
         * @param eventKey 事件名
         * @param callback 方法
         * @param target 绑定对象
         * @param isOnce 是否只执行一次
         */
        public on(eventKey: string, callback: Function, target: Object, isOnce?: boolean);
        /**
         * 推送事件
         * @param eventKey 事件名
         * @param param 参数
         */
        public emit(eventKey: string, ...param: any);
        /**
         * 取消监听
         * @param eventKey 事件名
         * @param callback 方法
         * @param target 绑定对象
         */
        public off(eventKey: string, callback: Function, target: Object)
        /**
         * 取消绑定在对象上的所有事件
         * @param target 绑定对象
         */
        public targetOff(target: Object);
        /**
         * 清理事件通过事件名
         * @param eventName 事件名
         */
        public clearEvent(eventKey: string);
        /**
         * 获取所有事件
         */
        public getAllEvents()
    }
    /**事件管理 */
    class Event {
        /**
         * 是否存在事件key
         * @param eventKey 
         */
        public static hasEventKey(eventKey: string | CMDFORMAT): boolean;
        /**
         * 监听事件
         * @param eventKey 事件名
         * @param callback 方法
         * @param target 绑定对象
         * @param isOnce 是否只执行一次
         */
        public static on(eventKey: string | CMDFORMAT, callback: Function, target: Object, isOnce?: boolean);

        /**
         * 推送事件
         * @param eventKey 事件名
         * @param param 参数
         */
        public static emit(eventKey: string | CMDFORMAT, ...param: any);

        /**
         * 取消监听
         * @param eventKey 事件名
         * @param callback 方法
         * @param target 绑定对象
         */
        public static off(eventKey: string | CMDFORMAT, callback: Function, target: Object);

        /**
         * 取消绑定在对象上的所有事件
         * @param target 绑定对象
         */
        public static targetOff(target: Object);
        /**
         * 清理事件通过事件名
         * @param eventName 事件名
         */
        public static clearEvent(eventKey: string | CMDFORMAT);

        /**
         * 获取所有事件
         */
        public static getAllEvents()
    }
    /**
    * 调用原生接口
    * @param funcName 方法名
    * @param param 参数
    * @returns 返回的是string字符串 为undefined表示接口不存在或者原生接口处理不符合规则
    */
    function toNative(funcName: string, param?: string | object | number): string;
}

declare class MobileDetect {
    constructor(str: string)
    os(): string;
    version(str: string): string;
    mobile(): string;
}

declare namespace ClipboardJS {
    export function copy(content: string, callback?: (isok: boolean) => void): void;
}

