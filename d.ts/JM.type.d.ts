
declare type SingletonType = 'data' | 'mgr' | 'unknown' | 'sysMgr';
/**
 * cmd消息
 *  cmd: cmd值
 *  action:action 只针对http
 *  socketName: socket名字（消息冲突的时候用到）
 *  type:4(发送超时重发强制发送接受 如果强制接受不到会自动重连网络 强制一次只能发一条消息) 未开发默认1[1（发送|接受）2(发送)3（接受）]
 *  loading: 显示转圈 只针对http
 *  loadingDelayed loading显示的延时 只针对http
 *  desc: 转圈提示内容 只针对http
 *  ver:版本
 *  token:是否需要token
 *  post:是否post 只针对http
 *  isForce:是否强制请求 强制请求失败后会自动替换URL地址 并重新请求 只针对http
 *  cacheTime:数据缓存时间 默认不缓存，毫秒计算 只针对http
 */
declare type CMDFORMAT = {
    cmd?: number,
    socketName?: string,
    type?: 1 | 2 | 3 | 4,
    action?: string,
    loading?: false | true,
    loadingDelayed?: number,
    desc?: string,
    ver?: string,
    token?: boolean,
    post?: boolean,
    isForce?: boolean,
    cacheTime?: number,
};
/**
 * 网络类型
 * wifi
 * 2G：2G网络
 * 3G：3G网络
 * 4G：4G网络
 * unknown：Android 下不常见的网络类型
 * none：无网络
 */
declare type NetworkType = 'wifi' | '2G' | '3G' | '4G' | 'unknown' | 'none'
