import { game } from "cc";

/**
 * 基础配置
 */
export class JMConfig {
    /**框架视图预制路径 */
    public static sysViewPath = 'jmframe/prefab/view/';
    /**心跳发送间隔时间 10s */
    public static socketHeartbeatInterval = 10000;
    /**心跳超时间隔 2s */
    public static socketHeartbeatOverTime = 2000;
    /**开启debug模式 */
    public static Debug = !!(game.config as any).DEBUG;
}
//预览环境开启debug
if (window['CC_PREVIEW']) {
    JMConfig.Debug = true;
    window['JMConfig'] = JMConfig;
}