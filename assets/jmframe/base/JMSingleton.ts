import { js } from "cc";
import { JMBaseEvent } from "../event/JMEvent";
import { JMConfig } from "../JMConfig";
import JMSingletonMgr from "../manager/JMSingletonMgr";

/**
 * 单例模式基类
 */
export default class JMSingleton {
    protected static singletonType: SingletonType = 'unknown';
    //是否可用
    public isValid: boolean = false;
    private _classname: string = '';
    private _jmevent: JMBaseEvent = null;
    /**单例模式支持事件的分发，分开事件分发有助于提高性能 */
    private get jmevent(): JMBaseEvent {
        if (!this._jmevent) {
            this._jmevent = new JMBaseEvent();
        }
        return this._jmevent;
    }
    public static instance<T extends {}>(this: new () => T): T {
        if (!(<any>this)._instance) {
            (<any>this)._instance = new this();
            (<any>this)._instance.__xdctor__ && (<any>this)._instance.__xdctor__();
            JMSingletonMgr.addSingleton((<any>this));
            let name = js.getClassName(<any>this) || (<any>this).name;
            (<any>this)._instance._classname = name;
            (<any>this)._instance.isValid = true;
            (<any>this)._instance.onInit();
            if (JMConfig.Debug) {
                window[name] = (<any>this);
            }
        }
        return (<any>this)._instance;
    }
    public getClassName() {
        return this._classname
    }
    protected onInit(): void { }
    protected onRecycle(): void { }
    /**
     * 回收
     */
    public static recycle(): void {
        if ((<any>this)._instance) {
            (<any>this)._instance.onRecycle();
            (<any>this)._instance.isValid = false;
            let name = js.getClassName(<any>this) || (<any>this).name;
            JMSingletonMgr.clearSingleton(name);
            delete (<any>this)._instance;
        }
    }
    public hasEventListener(eventKey: string, callback: Function, target: Object): boolean {
        return this.jmevent.hasEventListener(eventKey, callback, target);
    }
    /**
     * 是否存在事件key
     * @param eventKey 
     */
    public hasEventKey(eventKey: string): boolean {
        return this.jmevent.hasEventKey(eventKey);
    }
    /**
     * 监听事件
     * @param eventKey 事件名
     * @param callback 方法
     * @param target 绑定对象
     * @param isOnce 是否只执行一次
     */
    public on(eventKey: string, callback: Function, target: Object, isOnce?: boolean) {
        this.jmevent.on(eventKey, callback, target, isOnce);
    }
    /**
     * 推送事件
     * @param eventKey 事件名
     * @param param 参数
     */
    public emit(eventKey: string, ...param: any) {
        this.jmevent.emit(eventKey, ...param);
    }
    /**
     * 取消监听
     * @param eventKey 事件名
     * @param callback 方法
     * @param target 绑定对象
     */
    public off(eventKey: string, callback: Function, target: Object) {
        this.jmevent.off(eventKey, callback, target);
    }
    /**
     * 取消绑定在对象上的所有事件
     * @param target 绑定对象
     */
    public targetOff(target: Object) {
        this.jmevent.targetOff(target);
    }
    /**
     * 清理事件通过事件名
     * @param eventName 事件名
     */
    public clearEvent(eventKey: string) {
        this.jmevent.clearEvent(eventKey);
    }
    /**
     * 获取所有事件
     */
    public getAllEvents() {
        return this.jmevent.getAllEvents();
    }
}