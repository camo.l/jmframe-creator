import { sys } from "cc";
import JMSingleton from "./JMSingleton";
/**
* 数据模块
*/
export class JMBaseMgr extends JMSingleton {
    protected static singletonType: SingletonType = 'mgr';
    private _isBind: boolean = false;
    /**
     * 数据初始化
     */
    protected onInit(): void {
        if (!this._isBind) {
            this._isBind = true;
            this.bindEvents();
        }
    }
    /**
     * 移除单例
     */
    protected onRecycle(): void {
        if (this._isBind) {
            this._isBind = false;
            this.removeEvents();
            jm.Event.targetOff(this);
        }
    }
    /**移除事件 */
    protected removeEvents() { }
    /**
     * 绑定事件
     */
    protected bindEvents() { }
}

/**
 * 数据模块
 */
export default class JMBaseData extends JMBaseMgr {
    protected static singletonType: SingletonType = 'data';
    private _baseLocalData: object = null;
    private _baseKey: string = null;

    private _getLocalKey(): string {
        if (!this._baseKey) {
            let _libraryName = this.getLibraryName();
            let _tableName = this.getTableName();
            if (!_libraryName) {
                jm.Log.error('getLibraryName return null');
                return;
            }
            if (!_tableName) {
                jm.Log.error('getTableName return null');
                return;
            }
            this._baseKey = `${_libraryName}-${_tableName}`;
        }
        return this._baseKey;
    }
    /**
     * 获取本地数据通过key值
     * @param key 
     */
    public getLocalData(key: number | string): any {
        if (this._baseLocalData) {
            return this._baseLocalData[key];
        }
        let localKey = this._getLocalKey();
        if (localKey) {
            let localDataStr = sys.localStorage.getItem(localKey);
            if (localDataStr) {
                this._baseLocalData = JSON.parse(localDataStr);
            } else {
                this._baseLocalData = {};
            }
            return this._baseLocalData[key];
        }
        return null;
    }
    /**
     * 设置本地数据
     * @param key 
     * @param data 
     */
    public saveLocalData(key: number | string, data: any) {
        let localKey = this._getLocalKey();
        if (localKey) {
            if (!this._baseLocalData) {
                let localDataStr = sys.localStorage.getItem(localKey);
                if (localDataStr) {
                    this._baseLocalData = JSON.parse(localDataStr);
                } else {
                    this._baseLocalData = {};
                }
            }
            if (typeof data == 'undefined') {
                delete this._baseLocalData[key];
            } else {
                this._baseLocalData[key] = data;
            }
            sys.localStorage.setItem(localKey, JSON.stringify(this._baseLocalData));
        }
    }
    public clearLocalDataByKey(key: number | string) {
        this.saveLocalData(key, undefined);
    }
    /**
     * 清理本地数据
     */
    public clearLocalData() {
        let localKey = this._getLocalKey();
        if (localKey) {
            sys.localStorage.removeItem(localKey);
        }
    }
    /**
     * 库名
     */
    protected getLibraryName(): string { return 'jmframe'; }
    /**
     * 表名
     */
    private getTableName(): string { return this.getClassName(); }
}
