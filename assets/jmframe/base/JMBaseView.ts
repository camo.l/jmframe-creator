import { SpriteFrame, Node, Sprite, color, BlockInputEvents, tween, Vec3, _decorator, Enum, UITransform, js, Button } from "cc";
import JMAdapNode from "../component/JMAdapNode";
import { JMPlatform } from "../JMSys";
import JMAssetsMgr from "../manager/JMAssetsMgr";
import JMUIUtil from "../utils/JMUIUtil";

export enum JMViewType {
    Bind,/**绑定在场景的层 */
    /**Page 暂时不做，使用场景切换*/
    /**弹框层 */
    Pop,
    /**提示层 */
    Tips,
    /**引导层 */
    Guide,
    /**系统tips层 为常驻节点 */
    SysTips,
    /**通知层  为常驻节点*/
    Notice,
    /**loading层 为常驻节点*/
    Loading,
    /**软提示层 为常驻节点*/
    SoftTips,
    /**退出游戏弹框 为常驻节点*/
    ExitTips,
    /**debug层 为常驻节点*/
    Debug,
    /**最大数 为常驻节点*/
    Max,
}
//遮罩类型
const ShadeType = Enum({
    none: 1,//无背景
    black: 2,//黑背景
    sprite: 3,//图片
})
/**
 * 基础视图对象
 */
const { ccclass, property } = _decorator;

let defaultSpriteFrame: { [key: string]: SpriteFrame } = js.createMap();

@ccclass
export default class baseView extends JMAdapNode {
    protected static prefabUrl: string;
    //Bundle 名 默认为resources bundle
    protected static bundleName: string = 'resources';
    protected static viewType: JMViewType = JMViewType.Pop;
    protected static viewZorder: number = 0;
    /**去除的平台，平台下不能显示 */
    protected static wipePlatforms: Array<JMPlatform> = [];
    /**限制后的提示内容 */
    protected static wipePlatformsDesc: string = null;

    @property({
        displayName: '开启点击阻塞',
    })
    public openBlock = true;
    /**是否阻塞 */
    set isBlock(value: boolean) {
        this.openBlock = value;
        if (this.getComponent(Button)) {
            this.getComponent(Button).interactable = this.openBlock;
            return;
        }
        if (!this.maskBlockNode) {
            this.createBlockNode();
        }
        this.maskBlockNode.getComponent(BlockInputEvents).enabled = this.openBlock;
    }
    get isBlock(): boolean {
        if (this.getComponent(Button)) return this.getComponent(Button).interactable;
        return this.openBlock;
    }

    @property({
        type: ShadeType,
        displayName: '开启遮罩',
        tooltip: `none 无背景
        black 黑背景
        sprite 图片
        `,
    })
    public openShade = ShadeType.black;

    @property({
        displayName: '启用过渡效果',
    })
    public enableAnim = false;

    @property({
        displayName: "背景图路径",
        type: '',
        multiline: true,
        formerlySerializedAs: '_N$string'
    })
    imgUrl = 'hall/img/bg/commonBg';

    private maskBlockNode: Node = null;
    private lay_imgBack: Node = null;
    onLoad() {
        super.onLoad();
        if (this.openBlock) {
            this.createBlockNode();
        }
    }
    /**隐藏图片 */
    public hideBackImg() {
        if (this.lay_imgBack) this.lay_imgBack.active = false;
    }
    public showBackImg() {
        if (this.lay_imgBack) this.lay_imgBack.active = true;
    }
    /**加载背景外层不要调用，框架层调用方法 */
    public loadBackImg(func: (type: string) => void) {
        if (this.openShade == ShadeType.black || this.openShade == ShadeType.sprite) {
            let maskNode = new Node('img_back');
            this.lay_imgBack = maskNode;
            let transform = maskNode.addComponent(UITransform);
            maskNode.parent = this.node;
            maskNode.setSiblingIndex(0);
            if (this.openShade == ShadeType.black) {
                this.imgUrl = 'xdframe/img/sprite_splash'
            }

            let callback = (spriteframe: SpriteFrame) => {
                let sprite = maskNode.addComponent(Sprite);
                sprite.spriteFrame = spriteframe;
                if (this.openShade == ShadeType.black) {
                    sprite.color = color(0, 0, 0, 255 * 0.7)
                    transform.setContentSize(JMUIUtil.getViewSize());
                } else if (this.openShade == ShadeType.sprite) {
                    let winSize = JMUIUtil.getViewSize();
                    let _xs = winSize.width / transform.width;
                    let _ys = winSize.height / transform.height;
                    let _bl = _xs > _ys ? _xs : _ys;
                    transform.width = _bl * transform.width;
                    transform.height = _bl * transform.height;
                }
                func && func('success')
            }

            let defaultFrame = defaultSpriteFrame[this.imgUrl] || null
            if (!defaultFrame || !defaultFrame.isValid) {
                JMAssetsMgr.loadBundleRes({
                    bundleName: 'resources',
                    path: this.imgUrl,
                    type: SpriteFrame,
                    success: (spriteframe: SpriteFrame) => {
                        if (!defaultFrame) {
                            defaultSpriteFrame[this.imgUrl] = spriteframe;
                        }
                        callback(spriteframe)
                    },
                    fail: (err: any) => {
                        func && func('fail')
                    }
                })
            } else {
                callback(defaultFrame)
            }
        } else {
            func && func('success')
        }
    }
    private createBlockNode() {
        if (this.getComponent(Button)) return;
        this.maskBlockNode = new Node('maskBlockNode');
        let transform = this.maskBlockNode.addComponent(UITransform);
        transform.setContentSize(JMUIUtil.getViewSize());
        this.maskBlockNode.parent = this.node;
        this.maskBlockNode.setSiblingIndex(0);
        this.maskBlockNode.addComponent(BlockInputEvents);
    }
    public onShow(...param: any) { };
    public onHide() { };
    public onClear(...param: any) { };

    /**
     * 获取视图路径
     */
    public static getPrefabUrl(): string {
        return this.prefabUrl;
    }
    /**
     * 获取bundle 名
     */
    public static getBundleName(): string {
        return this.bundleName;
    }
    /**
     * 获取视图类型（父级层级）
     */
    public static getViewType(): JMViewType {
        return this.viewType;
    }
    /**
     * 获取视图层级
     */
    public static getViewZorder(): number {
        return this.viewZorder;
    }
    /**去除的平台 */
    public static getWipePlatforms(): Array<JMPlatform> {
        return this.wipePlatforms;
    }
    public static getWipePlatformsDesc(): string {
        return this.wipePlatformsDesc;
    }

    onEnable() {
        super.onEnable()
        if (this.enableAnim == true) {
            tween(this.node)
                .to(0.15, { scale: new Vec3(1.15, 1.15, 1) })
                .to(0.1, { scale: new Vec3(1, 1, 1) })
                .start();
        }
        this._isOnHide = false;
    }
    /**是否执行了onHide */
    private _isOnHide: boolean = false;
    onDisable() {
        super.onDisable();
        if (!this._isOnHide) {
            this._isOnHide = true;
            this.onHide();
        }
    }
    onDestroy() {
        super.onDestroy();
        if (!this._isOnHide) {
            this._isOnHide = true;
            this.onHide();
        }
    }
    private _viewKey: string;
    /**
     * 设置视图key
     * @param key 
     */
    setViewKey(key: string) {
        this._viewKey = key;
    }
    /**
     * 获取视图key
     */
    getViewKey(): string {
        return this._viewKey;
    }
    hide() {
        jm.ViewMgr.hideViewByKey(this._viewKey);
    }
    clear() {
        jm.ViewMgr.clearViewByKey(this._viewKey);
    }
}