import JMBaseNode from "./JMBaseNode";
import { _decorator, game } from "cc";

const { ccclass, menu } = _decorator;
//默认相机
const defaultCamera = ['sysView', 'viewUI'];
@ccclass
@menu('JM Frame/BaseScene')
export default class JMBaseScene extends JMBaseNode {
    protected static sceneUrl: string;
    //Bundle 名 默认为resources bundle
    protected static bundleName: string = 'resources';
    public static getSceneUrl(): string {
        return this.sceneUrl;
    }
    public static getBundleName(): string {
        return this.bundleName;
    }
    onLoad() {
        super.onLoad();
        // for (let i = 0, length = defaultCamera.length; i < length; i++) {
        //     let idx = (game as any).groupList.indexOf(defaultCamera[i]);
        //     let _cullingMask = 1 << idx;
        //     let isAdd = true;
        //     if (idx == -1) {
        //         xd.Log.warn('不存在的group:', defaultCamera[i]);
        //         continue;
        //     }
        //     // for (let j = 0, jlength = Camera.cameras.length; j < jlength; j++) {
        //     //     const coment = cc.Camera.cameras[j];
        //     //     //是否已经存在的渲染
        //     //     if ((_cullingMask & coment.cullingMask) > 0) {
        //     //         isAdd = false;
        //     //         //渲染存在
        //     //         xd.Log.warn('渲染已存在:', defaultCamera[i], coment);
        //     //         break;
        //     //     }
        //     // }
        //     // if (isAdd) {
        //     //     let node = new cc.Node('cam' + defaultCamera[i]);
        //     //     let camera = node.addComponent(cc.Camera);
        //     //     //渲染最高级
        //     //     camera.cullingMask = 1 << idx;
        //     //     camera.depth = idx;
        //     //     node.parent = cc.Canvas.instance.node;
        //     // }
        // }
    }
    public onShow(...param: any) { };

    public static isShow(): boolean {
        return jm.ViewMgr.sceneIsShow(this);
    }
}
