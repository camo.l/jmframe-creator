export enum JMSocketState {
    /**空闲状态 */
    IDLE = 1,
    /**连接中 */
    CONNECTING,
    /**已连接 */
    OPEN,
    /**关闭状态 */
    CLOSED,//
    /**关闭中状态 */
    CLOSING,
}

export interface JMBaseSocketDelegate {
    //连接成功
    onOpen();
    //接受消息
    onMessage(data: string | ArrayBuffer);
    //错误
    onError(err);
    //关闭成功
    onClose(msg: string);
}

export interface JMBaseSocket {
    //连接
    connect();
    //发送消息
    send(data: string | ArrayBuffer): boolean;
    //关闭
    close();
    //获取状态
    getState(): JMSocketState;
    //获取名字
    getName(): string;
    //设置链接地址
    setUrl(url: string);
    //获取链接地址
    getUrl(): string;
}

export class JMWebSocket implements JMBaseSocket {
    //代理事件
    private _delegate: JMBaseSocketDelegate;
    //链接地址
    private _url: string;
    //链接器名字
    private _name: string;
    //socket对象
    private _webSocket: WebSocket;
    //链接器状态
    private _state: JMSocketState = JMSocketState.IDLE;
    //数据类型
    private _binaryType: BinaryType;
    private _timeOutConnect: number = -1;
    constructor(name: string, url: string, delegate: JMBaseSocketDelegate, binaryType: BinaryType = 'blob') {
        this._name = name;
        this._url = url;
        this._delegate = delegate;
        this._binaryType = binaryType;
    }
    /**
     * 设置URL 也可以重定向URL地址
     * @param url 
     */
    setUrl(url: string) {
        this._url = url;
    }
    /**
     * 获取链接地址
     */
    getUrl(): string {
        return this._url;
    }
    private startTimeOutConnect() {
        this.stopTimeOutConnect();
        this._timeOutConnect = setTimeout(() => {
            if (this._state == JMSocketState.CONNECTING) {
                this._state = JMSocketState.CLOSED;
                this.connect();
            }
        }, 500);
    }
    private stopTimeOutConnect() {
        if (this._timeOutConnect) {
            this._timeOutConnect = -1;
            clearTimeout(this._timeOutConnect);
        }
    }
    connect() {
        if (this._state == JMSocketState.CONNECTING) {
            jm.Log.log_socket(`%c[${this._name}] 正在连接中 : ${this._url}`, jm.LogFontStyle.Slight);
            return;
        }
        if (this._state == JMSocketState.OPEN) {
            jm.Log.log_socket(`%c[${this._name}] 已连接 : ${this._url}`, jm.LogFontStyle.Slight);
            return;
        }
        jm.Log.log_socket(`%c[${this._name}] 正在连接:${this._url}`, jm.LogFontStyle.Slight);
        this._state = JMSocketState.CONNECTING;
        if (this._webSocket) return;
        let ws = this._webSocket = new WebSocket(this._url);
        ws.binaryType = this._binaryType;
        ws.onopen = (ev: Event) => {
            jm.Log.log_socket(`%c[${this._name}] 连接成功`, jm.LogFontStyle.Slight);
            this._state = JMSocketState.OPEN;
            this._delegate.onOpen();
        };
        ws.onmessage = (ev: MessageEvent) => {
            this._delegate.onMessage(ev.data);
        };
        ws.onclose = (ev: CloseEvent) => {
            this._webSocket = undefined;
            if (this._state == JMSocketState.CLOSING) {
                jm.Log.log_socket(`%c[${this._name}] 主动断开`, jm.LogFontStyle.Slight);
            } else {
                jm.Log.log_socket(`%c[${this._name}] 异常中断`, jm.LogFontStyle.Slight);
            }
            /**连接错误移除监听 */
            ws.onopen = () => { };
            ws.onmessage = () => { };
            ws.onclose = () => { };
            ws.onerror = () => { };
            if (this._state == JMSocketState.CONNECTING) {
                this._delegate.onClose(JSON.stringify(ev));
                this.startTimeOutConnect();
            } else {
                this.stopTimeOutConnect();
                this._state = JMSocketState.CLOSED;
                this._delegate.onClose(JSON.stringify(ev));
            }
        };
        ws.onerror = (ev: Event) => {
            this._webSocket = undefined;
            jm.Log.log_socket(`%c[${this._name}] 连接错误`, jm.LogFontStyle.Slight);
            /**连接错误移除监听 */
            ws.onopen = () => { };
            ws.onmessage = () => { };
            ws.onclose = () => { };
            ws.onerror = () => { };
            if (this._state == JMSocketState.CONNECTING) {
                this._delegate.onError(null);
                this.startTimeOutConnect();
            } else {
                this.stopTimeOutConnect();
                this._state = JMSocketState.CLOSED;
                this._delegate.onError(null);
            }
        }
    }
    send(data: string | ArrayBuffer): boolean {
        if (this._state != JMSocketState.OPEN) {
            jm.Log.log_socket(`%c[${this._name}] 未连接 : ${this._url}`, jm.LogFontStyle.Slight);
            return false;
        }
        if (this._webSocket && this._webSocket.readyState == WebSocket.OPEN) {
            this._webSocket.send(data);
            return true;
        }
        return false;
    }
    close() {
        this.stopTimeOutConnect();
        if (this._state == JMSocketState.IDLE) {
            jm.Log.log_socket(`%c[${this._name}] 空闲中 : ${this._url}`, jm.LogFontStyle.Slight);
            return;
        }
        if (this._state == JMSocketState.CLOSED) {
            jm.Log.log_socket(`%c[${this._name}] 已关闭 : ${this._url}`, jm.LogFontStyle.Slight);
            return;
        }
        if (this._state == JMSocketState.CLOSING) {
            jm.Log.log_socket(`%c[${this._name}] 关闭中 : ${this._url}`, jm.LogFontStyle.Slight);
            return;
        }
        if (this._webSocket) {
            this._state = JMSocketState.CLOSING;
            this._webSocket.close();
        } else {
            this._state = JMSocketState.CLOSED;
        }
    }
    /**
     * 获取socket 状态
     */
    getState(): JMSocketState {
        return this._state;
    }
    /**
     * 获取socket name
     */
    getName(): string {
        return this._name;
    }
}

export class JMWxSocket implements JMBaseSocket {
    private _delegate: JMBaseSocketDelegate;
    private _url: string;
    private _name: string;
    private _socketTask: wx.SocketTask;
    private _state: JMSocketState = JMSocketState.IDLE;
    private _timeOutConnect: number = -1;
    constructor(name: string, url: string, delegate: JMBaseSocketDelegate) {
        this._name = name;
        this._url = url;
        this._delegate = delegate;
    }
    /**
     * 设置URL 也可以重定向URL地址
     * @param url 
     */
    setUrl(url: string) {
        this._url = url;
    }
    /**
     * 获取链接地址
     */
    getUrl(): string {
        return this._url;
    }
    /**开启延时重连，主要是为了防止socket连接重复刷造成的内存消耗 */
    private startTimeOutConnect() {
        this.stopTimeOutConnect();
        this._timeOutConnect = setTimeout(() => {
            if (this._state == JMSocketState.CONNECTING) {
                this._state = JMSocketState.CLOSED;
                this.connect();
            }
        }, 500);
    }
    private stopTimeOutConnect() {
        if (this._timeOutConnect) {
            this._timeOutConnect = -1;
            clearTimeout(this._timeOutConnect);
        }
    }
    connect() {
        if (this._state == JMSocketState.CONNECTING) {
            jm.Log.log_socket(`%c[${this._name}] 正在连接中 : ${this._url}`, jm.LogFontStyle.Slight);
            return;
        }
        if (this._state == JMSocketState.OPEN) {
            jm.Log.log_socket(`%c[${this._name}] 已连接 : ${this._url}`, jm.LogFontStyle.Slight);
            return;
        }
        jm.Log.log_socket(`%c[${this._name}] 正在连接:${this._url}`, jm.LogFontStyle.Slight);
        this._state = JMSocketState.CONNECTING;
        if (this._socketTask) return;
        let ws = this._socketTask = wx.connectSocket({
            url: this._url,
        })
        ws.onClose(() => {
            this._socketTask = undefined;
            if (this._state == JMSocketState.CLOSING) {
                jm.Log.log_socket(`%c[${this._name}] 主动断开`, jm.LogFontStyle.Slight);
            } else {
                jm.Log.log_socket(`%c[${this._name}] 异常中断`, jm.LogFontStyle.Slight);
            }
            if (this._state == JMSocketState.CONNECTING) {
                this._delegate.onClose(null);
                this.startTimeOutConnect();
            } else {
                this.stopTimeOutConnect();
                this._state = JMSocketState.CLOSED;
                this._delegate.onClose(null);
            }
        })
        ws.onError(() => {
            this._socketTask = undefined;
            jm.Log.log_socket(`%c[${this._name}] 连接错误`, jm.LogFontStyle.Slight);
            if (this._state == JMSocketState.CONNECTING) {
                this._delegate.onError(null);
                this.startTimeOutConnect();
            } else {
                this.stopTimeOutConnect();
                this._state = JMSocketState.CLOSED;
                this._delegate.onError(null);
            }
        })
        ws.onMessage((res) => {
            this._delegate.onMessage(res.data);
        })
        ws.onOpen((res) => {
            this._state = JMSocketState.OPEN;
            this._delegate.onOpen();
        })
    }
    send(data: string | ArrayBuffer): boolean {
        if (this._state != JMSocketState.OPEN) {
            jm.Log.log_socket(`%c[${this._name}] 未连接 : ${this._url}`, jm.LogFontStyle.Slight);
            return false;
        }
        if (this._socketTask) {
            this._socketTask.send({
                data: data
            });
            return true;
        }
        return false;
    }
    close() {
        this.stopTimeOutConnect();
        if (this._state == JMSocketState.IDLE) {
            jm.Log.log_socket(`%c[${this._name}] 空闲中 : ${this._url}`, jm.LogFontStyle.Slight);
            return;
        }
        if (this._state == JMSocketState.CLOSED) {
            jm.Log.log_socket(`%c[${this._name}] 已关闭 : ${this._url}`, jm.LogFontStyle.Slight);
            return;
        }
        if (this._state == JMSocketState.CLOSING) {
            jm.Log.log_socket(`%c[${this._name}] 关闭中 : ${this._url}`, jm.LogFontStyle.Slight);
            return;
        }
        if (this._socketTask) {
            this._state = JMSocketState.CLOSING;
            this._socketTask.close({});
        } else {
            this._state = JMSocketState.CLOSED;
        }
    }
    /**
     * 获取socket 状态
     */
    getState(): JMSocketState {
        return this._state;
    }
    /**
     * 获取socket name
     */
    getName(): string {
        return this._name;
    }

}