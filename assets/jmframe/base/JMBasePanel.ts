import { _decorator, Component, Node, js, Prefab, instantiate } from 'cc';
import JMAssetsMgr from '../manager/JMAssetsMgr';
import JMUIUtil from '../utils/JMUIUtil';
import JMBaseNode from './JMBaseNode';
const { ccclass } = _decorator;

interface xdPanelClass {
    getPrefabUrl(): string;
    getBundleName(): string;
}
interface xdPanelThen<T extends xdBasePanel> {
    /**show完成后执行的代码 */
    then: (showFunc: (prefabClass: T) => void) => xdPanelThen<T>,
    /**开启loading显示 */
    openLoading: () => void;
}
enum panelLoadStatus {
    none,//未知
    Err,//加载失败
    Success,//加载成功
    Load,//正在加载
    Destroy,//已销毁
}
interface xdPanelProperty {
    status: panelLoadStatus;
    active: boolean;//视图状态
    node: Node;//节点对象
    showFunc?: (prefabClass: any) => void,//执行onshow后调用的方法
    prefabClass: any,//预制板顶的类，给showFunc使用
}
/**panel的父节点对象，管理panel层 */
class xdParentPanel extends Component {
    private panelList: { [key: string]: xdPanelProperty } = js.createMap();
    /**获取属性 */
    private getProperty(prefabClass: xdPanelClass): xdPanelProperty {
        let key = prefabClass.getBundleName() + prefabClass.getPrefabUrl();
        if (!this.panelList[key]) {
            this.panelList[key] = {
                status: panelLoadStatus.none,
                active: false,//视图状态
                node: null,//节点对象
                prefabClass: prefabClass,
            }
        }
        return this.panelList[key];
    }
    private loadPanel(prefabClass: xdPanelClass) {
        let url = prefabClass.getPrefabUrl();
        let bundleName = prefabClass.getBundleName();
        let key = prefabClass.getBundleName() + prefabClass.getPrefabUrl();
        if (this.panelList[key].status == panelLoadStatus.Load) {
            return;
        }
        this.panelList[key].status = panelLoadStatus.Load;
        JMAssetsMgr.loadBundleRes({
            bundleName: bundleName,
            path: url,
            type: Prefab,
            onProgress: (finish, total) => {
                //加载过程
            },
            success: (assets: Prefab) => {
                if (!this.panelList[key]) {
                    return
                };
                this.hideLoading(this.panelList[key].prefabClass);
                //加载成功
                if (this.panelList[key].status != panelLoadStatus.Destroy) {
                    this.panelList[key].status = panelLoadStatus.Success;
                    let node: Node = instantiate(assets);
                    let baseTs = node.getComponent(xdBasePanel);
                    node.parent = this.node;
                    node.active = this.panelList[key].active;
                    this.panelList[key].node = node;
                    //适配es6写法
                    this.panelList[key].showFunc && this.panelList[key].prefabClass && this.panelList[key].showFunc(baseTs.getComponent(this.panelList[key].prefabClass));
                }
                delete this.panelList[key].showFunc;
            },
            fail: (err) => {
                if (!this.panelList[key]) return;
                jm.Log.error(err);
                this.panelList[key].status = panelLoadStatus.Err;
                delete this.panelList[key].showFunc;
            },
        })
    }
    /**显示加载loading */
    private showLoading(prefabClass: xdPanelClass) {

    }
    /**隐藏加载loading */
    private hideLoading(prefabClass: xdPanelClass) {

    }
    show<T extends xdBasePanel>(prefabClass: xdPanelClass, mutex: boolean): xdPanelThen<T> {
        //隐藏其他的类
        if (mutex) {
            let key = prefabClass.getBundleName() + prefabClass.getPrefabUrl();
            for (var pkey in this.panelList) {
                if (pkey != key) {
                    this.hide(this.panelList[pkey].prefabClass);
                }
            }
        }
        let property = this.getProperty(prefabClass);
        let _system: xdPanelThen<T> = {
            then: (showFunc: (prefabClass2: T) => void): xdPanelThen<T> => {
                property.showFunc = showFunc;
                let node = property.node;
                if (JMUIUtil.nodeIsValid(node) && node.active == true) {
                    property.showFunc && property.showFunc(node.getComponent(property.prefabClass));
                    delete property.showFunc;
                }
                return _system;
            },
            openLoading: () => {
                this.showLoading(prefabClass);
                return _system;
            }
        }
        property.active = true;
        if (JMUIUtil.nodeIsValid(property.node)) {
            property.node.active = property.active;
        } else {
            this.loadPanel(prefabClass);
        }
        return _system;
    }
    hide(prefabClass: xdPanelClass) {
        let property = this.getProperty(prefabClass);
        property.active = false;
        if (JMUIUtil.nodeIsValid(property.node)) {
            property.node.active = property.active;
        }
    }
    clear(prefabClass: xdPanelClass) {
        let property = this.getProperty(prefabClass);
        property.status = panelLoadStatus.Destroy;
        if (JMUIUtil.nodeIsValid(property.node)) {
            property.node.destroy();
            let key = prefabClass.getBundleName() + prefabClass.getPrefabUrl();
            delete this.panelList[key];
        }
    }
    then<T extends xdBasePanel>(prefabClass: xdPanelClass, callback: (comp: T) => void) {
        let property = this.getProperty(prefabClass);
        if (JMUIUtil.nodeIsValid(property.node)) {
            let _comp = property.node.getComponent(<any>prefabClass);
            if (_comp && property.node.active) {
                callback(<any>_comp);
            }
        }
    }
    isLoadEnd(prefabClass: xdPanelClass): boolean {
        let property = this.getProperty(prefabClass);
        return property.status == panelLoadStatus.Success;
    }
    /**是否显示 如果正在加载中也属于显示 */
    isShow(prefabClass: xdPanelClass): boolean {
        let property = this.getProperty(prefabClass);
        return property.active;
    }
}
/**获取父节点对象 */
let getParentPanel = function (node: Node): xdParentPanel {
    let t = node.getComponent(xdParentPanel);
    if (!t) { t = node.addComponent(xdParentPanel) };
    return t;
}

@ccclass('xdBasePanel')
export class xdBasePanel extends JMBaseNode {
    protected static prefabUrl: string;
    //Bundle 名 默认为resources bundle
    protected static bundleName: string = 'resources';
    /**
     * 显示节点
     * @param this 类
     * @param node 节点
     * @param mutex 是否互斥 默认开启互斥，不开启互斥的情况下不会关闭其他panel
     */
    public static show<T extends xdBasePanel>(this: new () => T, node: Node, mutex: boolean = true): xdPanelThen<T> {
        return getParentPanel(node).show(<any>this, mutex);
    }
    public static clear<T extends xdBasePanel>(this: new () => T, node: Node) {
        getParentPanel(node).clear(<any>this);
    }
    public static hide<T extends xdBasePanel>(this: new () => T, node: Node) {
        getParentPanel(node).hide(<any>this);
    }
    /**是否显示 如果正在加载中也属于显示 */
    public static isShow<T extends xdBasePanel>(this: new () => T, node: Node): boolean {
        return getParentPanel(node).isShow(<any>this);
    }
    /**执行方法 只有当节点显示，并且已经加载过后才有效
     * 
     * ***View.then( node , (t) => {
     *     t.demo();
     * })
     */
    public static then<T extends xdBasePanel>(this: new () => T, node: Node, callback: (comp: T) => void) {
        return getParentPanel(node).then(<any>this, callback);
    }
    /**是否加载完成 */
    public static isLoadEnd<T extends xdBasePanel>(this: new () => T, node: Node): boolean {
        return getParentPanel(node).isLoadEnd(<any>this);
    }
    /**
     * 获取视图路径
     */
    public static getPrefabUrl(): string {
        return this.prefabUrl;
    }
    /**
     * 获取bundle 名
     */
    public static getBundleName(): string {
        return this.bundleName;
    }
    public hide() {
        this.node.active = false;
    }
}
