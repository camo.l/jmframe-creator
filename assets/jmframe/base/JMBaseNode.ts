
import { _decorator, Component } from "cc";

const { ccclass, property } = _decorator;
@ccclass
export default class JMBaseNode extends Component {
    private _baseData: any;
    private _isBaseBindEvent: Boolean = false;
    /**添加事件绑定 */
    protected bindEvents() { };
    /**移除事件绑定 */
    protected removeEvents() { };
    /**加载 */
    protected onUILoad() { };
    /**启动 */
    protected onUIStart() { };
    /**显示 */
    protected onUIEnable() { };
    /**隐藏 */
    protected onUIDisable() { };
    /**销毁 */
    protected onUIDestroy() { };
    //加载
    onLoad() {
        this.onUILoad();
    }
    //启动
    start() {
        this.onUIStart();
    }
    //显示
    onEnable() {
        this._bindEvents();
        this.onUIEnable();
    }
    //销毁
    onDestroy() {
        this.onUIDestroy();
        this._removeEvents();
    }
    //隐藏
    onDisable() {
        this.onUIDisable();
        this._removeEvents();
    }
    private _bindEvents() {
        this._isBaseBindEvent = true;
        this.bindEvents();
    }
    private _removeEvents() {
        if (this._isBaseBindEvent) {
            this._isBaseBindEvent = false;
            jm.Event.targetOff(this);
            this.removeEvents();
        }
    }
    /**
     * 获取当前组件名
     */
    public getName(): string {
        return this.name;
    }
    /**
     * 设置临时数据
     * @param arg 缓存数据
     */
    public setData(arg: any) {
        this._baseData = arg;
    }
    /**
     * 获取临时数据
     */
    public getData(): any {
        return this._baseData;
    }
}
