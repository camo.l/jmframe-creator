import { sys } from "cc";
import JMDataUtil from "./utils/JMDataUtil";

export enum JMPlatform {
    /**原生平台 */
    JSB = 1,
    /**web平台 */
    WEB,
    /**微信web平台 */
    WXWEB,
    /**微信小游戏平台 */
    WXGAME,
}

export enum JMSysOs {
    Windows = 'Windows',//Windows
    Android = 'Android',//Android
    iOS = 'iOS',//iOS
}

export class JMSys {
    private static _platform: JMPlatform;
    private static _model: string;
    private static _system: string;
    private static _brand: string;
    private static _isInit: boolean = false;
    /**
     * web端数据获取
     */
    private static initWebMobileDetect() {
        if (this._isInit) return;
        this._isInit = true;
        if ((window as any)['MobileDetect']) {
            var device_type = window.navigator.userAgent;
            var md = new MobileDetect(device_type);
            switch (md.os()) {
                case 'iOS':
                    this._system = md.os() + md.version('iPhone');
                    this._model = this._brand = JMDataUtil.trim(md.mobile());
                    break;
                case 'AndroidOS':
                    this._system = md.os() + md.version('Android');
                    var arr = device_type.split(';');
                    for (var i = 0; i < arr.length; i++) {
                        var idx = arr[i].indexOf('Build/');
                        if (idx > 0) {
                            this._model = this._brand = JMDataUtil.trim(arr[i].substr(0, idx));
                            break;
                        }
                    }
                    break;
            }
        }
    }
    /**微信小游戏端 */
    private static initWXGameInfo() {
        if (this._isInit) return;
        this._isInit = true;
        let info = wx.getSystemInfoSync();
        this._model = info.model;
        this._system = info.system;
        this._brand = info.brand;
    }
    /**
     * 平台
     * JSB  原生平台
     * WEB   web平台
     * WXWEB  微信web平台
     * WXGAME 微信小游戏平台
     * XQWEB  小Qweb平台
     * 
     */
    public static get platform(): JMPlatform {
        if (!this._platform) {
            if (sys.platform === sys.Platform.WECHAT_GAME) {//微信小游戏
                this._platform = JMPlatform.WXGAME;
            } else if (sys.isNative) {//JSB平台
                this._platform = JMPlatform.JSB;
            } else if (typeof window['wx'] == 'object') {
                this._platform = JMPlatform.WXWEB;
            } else {
                this._platform = JMPlatform.WEB;
            }
        }
        return this._platform;
    }
    /**
     * 系统
     */
    public static get os(): JMSysOs {
        return sys.os as any;
    }
    /**
     * 手机品牌
     */
    public static get brand(): string {
        if (this._brand) {
            return this._brand;
        }

        if (this.platform == JMPlatform.WEB
            || this.platform == JMPlatform.WXWEB) {
            this.initWebMobileDetect();
        } else if (this.platform == JMPlatform.WXGAME) {
            this.initWXGameInfo();
        } else if (this.platform == JMPlatform.JSB) {
            this._brand = jm.toNative('getBrand');
        } else {
            this._brand = 'none';
        }
        return this._brand;
    }
    /**
     * 操作系统版本
     */
    public static get system(): string {
        if (this._system) {
            return this._system;
        }
        if (this.platform == JMPlatform.WEB
            || this.platform == JMPlatform.WXWEB) {
            this.initWebMobileDetect();
        } else if (this.platform == JMPlatform.WXGAME) {
            this.initWXGameInfo();
        } else if (this.platform == JMPlatform.JSB) {
            this._system = jm.toNative('getSystemVersion');
        } else {
            this._system = 'none';
        }
        return this._system;
    }
    /**
     * 型号
     */
    public static get model(): string {
        if (this._model) {
            return this._model;
        }
        if (this.platform == JMPlatform.WEB
            || this.platform == JMPlatform.WXWEB) {
            this.initWebMobileDetect();
        } else if (this.platform == JMPlatform.WXGAME) {
            this.initWXGameInfo();
        } else if (this.platform == JMPlatform.JSB) {
            this._model = jm.toNative('getBrand');
        } else {
            this._model = 'none';
        }
        return this._model;
    }
}