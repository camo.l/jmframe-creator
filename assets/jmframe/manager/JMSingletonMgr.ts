/**
 * 单例管理器
 */

import { js } from "cc";
import JMSingleton from "../base/JMSingleton";
import JMPlugInMgr from "./JMPlugInMgr";

interface singletonsInterface { [key: string]: JMSingleton }
interface singletonsInterfaces { [key: string]: singletonsInterface }

interface singletonClass<T extends JMSingleton> {
    new(): T;
    recycle(): void;
}

export default class xdSingletonManager {
    private static singletons: singletonsInterfaces = js.createMap();
    /**
     * 添加单例对象
     * @param _class 
     */
    public static addSingleton<T extends JMSingleton>(_class: T) {
        let type = (<any>_class).singletonType;
        if (!this.singletons[type]) this.singletons[type] = {};
        let name = js.getClassName((<any>_class)) || (<any>_class).name;
        if (this.singletons[type][name]) {
            return;
        }
        this.singletons[type][name] = _class;
    }
    /**
     * 清除单例对象
     * @param _class 
     */
    public static clearSingleton<T extends JMSingleton>(_class: singletonClass<T>) {
        let type = (<any>_class).singletonType;
        if (!this.singletons[type]) {
            return;
        }
        let name = js.getClassName((<any>_class)) || (<any>_class).name;
        if (this.singletons[type][name]) {
            delete this.singletons[type][name];
            _class.recycle();
        }
    }
    /**
     * 获取所有单例对象
     */
    public static getAllSingleton(): singletonsInterfaces {
        return this.singletons;
    }
    /**获取单例 */
    public static getSingleton<T extends JMSingleton>(_class: T): T {
        let type = (<any>_class).singletonType;
        if (!this.singletons[type]) {
            return;
        }
        let name = js.getClassName((<any>_class)) || (<any>_class).name;
        return this.singletons[type][name] as any;
    }
    /**获取单例 */
    public static getSingletonByName(name: string): any {
        let _class = js.getClassByName(name);
        let type = (<any>_class).singletonType;
        if (!this.singletons[type]) {
            return;
        }
        return this.singletons[type][name];
    }
    /**
     * 清除所有单例对象
     */
    public static clearAllSingleton() {
        for (const key in this.singletons) {
            this.clearSingletonByType(key as any)
        }
    }
    /**
     * 清除指定类型单例对象
     * @param _type 类型
     */
    public static clearSingletonByType(_type: SingletonType) {
        for (const key in (this.singletons[_type] || {})) {
            const element = this.singletons[_type][key];
            this.clearSingleton(element as any)
        }
        if (_type == 'mgr') {
            JMPlugInMgr.destoryBaseMgrPlugIn();
        }
    }
}