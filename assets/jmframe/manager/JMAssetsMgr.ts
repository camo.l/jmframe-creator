import { loader, SpriteFrame, Sprite, Asset, SceneAsset, director, Scene, Texture2D, js, resources, AssetManager, assetManager, ImageAsset } from "cc"
import JMFile from "../file/JMFile"
import { JMPlatform, JMSys } from "../JMSys"
import JMHttp from "../net/JMHttp"
import JMDataUtil from "../utils/JMDataUtil"

/**
 * 资源管理器
 */
type loadBundleParam = {
    bundleName: string,
    success?: (bundle: AssetManager.Bundle) => void,
    fail?: (err) => void,
}
type loadBundleResParam<T extends Asset> = {
    bundleName: string,
    path: string,
    type?: typeof Asset,
    onProgress?: (finished: number, total: number, item: AssetManager.RequestItem) => void,
    success?: (assets: T) => void,
    fail?: (err) => void,
}
type preloadBundleResParam = {
    bundleName: string,
    path: string,
    type?: typeof Asset,
    onProgress?: (finished: number, total: number, item: AssetManager.RequestItem) => void,
    success?: (items?: AssetManager.RequestItem[]) => void,
    fail?: (err) => void,
}
type loadBundleSceneParam = {
    bundleName: string,
    path: string,
    onProgress?: (finished: number, total: number, item: AssetManager.RequestItem) => void,
    loadSuccess?: () => void,
    success?: (scene: Scene) => void,
    fail?: (err) => void,
}

type loadBundleFromDirParam = {
    bundleName: string,
    path: string,
    type: typeof Asset,
    success?: (asset: any) => void,
    fail?: (err) => void,
}

interface loadSpriteFrameIF {
    sprite?: Sprite,
    url: string,
    success?: (spriteFrame: SpriteFrame) => void,
    fail?: (path: string) => void,
}

interface downloaderAssetsIF {
    url: string,
    suffix: string,
    success?: (path: string) => void,
    fail?: () => void,
}
interface loadParamIF {
    url: string,
    type: string,
    noType: boolean,
    nativeUrl?: string,
}
export default class JMAssetsMgr {
    private static _downloaderTask: { [key: string]: Array<downloaderAssetsIF> } = js.createMap();
    /**下载资源 这个方法目前只能jsb调用 */
    public static downloaderAssets(option: downloaderAssetsIF) {
        if (!this._downloaderTask[option.url]) this._downloaderTask[option.url] = [];
        let taskList = this._downloaderTask[option.url];
        taskList.push(option);
        let fileName = JMDataUtil.md5(option.url) + '.' + option.suffix;
        let cachePath = JMFile.getWritablePath() + 'cache/' + fileName;
        let sendEnd = (path) => {
            if (path) {
                taskList.forEach((value) => {
                    value.success && value.success(path);
                })
            } else {
                taskList.forEach((value) => {
                    value.fail && value.fail();
                })
            }
            this._downloaderTask[option.url] = [];
        }
        if (JMFile.isFileExist(cachePath)) {
            sendEnd(cachePath);
            return;
        }
        //减少请求次数
        if (taskList.length == 1) {
            JMHttp.send({
                url: option.url,
                method: 'GET',
                responseType: 'arraybuffer',
                success: (data) => {
                    if (JMFile.writeDataToFile(new Uint8Array(data), cachePath)) {
                        sendEnd(cachePath);
                    } else {
                        sendEnd(null);
                    }
                },
                fail: () => {
                    sendEnd(null);
                },
            })
        }
    }
    private static _loadImageAsset(option: loadSpriteFrameIF, loadParam: loadParamIF) {
        let url = loadParam.nativeUrl || loadParam.url;
        let loadEndCallback = (err: Error | null, imageAsset: ImageAsset) => {
            if (option.sprite && option.sprite.isValid) {
                (option.sprite as any)._isLoadUrl = false;
                if ((option.sprite as any)._loadUrl != option.url) return;
                if (!err && imageAsset) {
                    let spriteframe = SpriteFrame.createWithImage(imageAsset);
                    option.sprite.spriteFrame = spriteframe;
                    option.success && option.success(spriteframe);
                } else {
                    option.fail && option.fail(option.url);
                }
            }
        }
        if (loadParam.noType || JMSys.platform == JMPlatform.JSB) {
            assetManager.loadRemote<ImageAsset>(url, { ext: loadParam.type }, loadEndCallback);
        } else {
            assetManager.loadRemote<ImageAsset>(url, loadEndCallback);
        }
    }
    /**
     * 加载精灵视图
     * @param option loadSpriteFrameIF
     */
    public static loadSpriteFrame(option: loadSpriteFrameIF) {
        //避免重复调用影响性能
        if ((option.sprite as any)._isLoadUrl && (option.sprite as any)._loadUrl == option.url) return;
        //防止资源替换冲突
        (option.sprite as any)._loadUrl = option.url;
        (option.sprite as any)._isLoadUrl = true;
        //外部链接
        if (JMDataUtil.isURL(option.url)) {
            // 强制追加参数, 模拟JPEG格式(规避引擎BUG)
            let loadParam: loadParamIF = {
                url: option.url,
                type: 'png',
                noType: false,
            };
            if (option.url.endsWith('jpg')) {
                loadParam.type = 'jpg';
            } else if (option.url.endsWith('png')) {
                loadParam.type = 'png';
            } else {
                loadParam.noType = true;
            }
            if (JMSys.platform == JMPlatform.JSB) {
                this.downloaderAssets({
                    url: option.url,
                    suffix: loadParam.type,
                    success: (path: string) => {
                        loadParam.nativeUrl = path;
                        this._loadImageAsset(option, loadParam);
                    },
                    fail: () => {
                        if (option.sprite && option.sprite.isValid) {
                            (option.sprite as any)._isLoadUrl = false;
                        }
                        option.fail && option.fail(option.url);
                    },
                });
            } else {
                this._loadImageAsset(option, loadParam);
            }
        } else {
            //内部资源
            let bundleName: string, url: string;
            if (option.url.indexOf(':') != -1) {
                let arr: Array<string> = option.url.split(':');
                bundleName = arr[0];
                url = arr[1];
            } else {
                bundleName = 'resources'
                url = option.url;
            }
            if (url == '') return;
            this.loadBundleRes({
                bundleName: bundleName,
                path: option.url,
                type: SpriteFrame,
                success: (spriteframe: SpriteFrame) => {
                    if (option.sprite && option.sprite.isValid) {
                        (option.sprite as any)._isLoadUrl = false;
                        if ((option.sprite as any)._loadUrl != option.url) return;
                        option.sprite.spriteFrame = spriteframe;
                        option.success && option.success(spriteframe);
                    }
                },
                fail: (err: any) => {
                    jm.Log.warn(option);
                    if (option.sprite && option.sprite.isValid) {
                        (option.sprite as any)._isLoadUrl = false;
                        if ((option.sprite as any)._loadUrl != option.url) return;
                        option.fail && option.fail(option.url);
                    }
                }
            })
        }
    }
    /**
     * 加载bundle
     * @param option loadBundleParam 类型
     */
    public static loadBundle(option: loadBundleParam) {
        if (!option.bundleName) option.success && option.success(resources);
        let bundle: AssetManager.Bundle = assetManager.getBundle(option.bundleName);
        if (bundle) {
            option.success(bundle);
        } else {
            assetManager.loadBundle(option.bundleName, null, (err, bundle) => {
                if (err) {
                    option.fail && option.fail(err);
                } else {
                    option.success && option.success(bundle);
                }
            });
        }
    }
    /**
     * 加载bundle 资源
     * @param option loadBundleParam 类型
     */
    public static loadBundleRes<T extends Asset>(option: loadBundleResParam<T>) {
        this.loadBundle({
            bundleName: option.bundleName,
            success: (bundle: AssetManager.Bundle) => {
                let url = option.path;
                if (option.type == SpriteFrame) {
                    if (url.indexOf('/spriteFrame') == -1) {
                        url += '/spriteFrame';
                    }
                } else if (option.type == Texture2D) {
                    if (url.indexOf('/texture') == -1) {
                        url += '/texture';
                    }
                }
                let asset = bundle.get(url, option.type) as T;

                if (asset) {
                    option.success && option.success(asset);
                    return;
                }
                bundle.load(option.path, option.type,
                    (finished: number, total: number, item: AssetManager.RequestItem) => {
                        //加载过程
                        option.onProgress && option.onProgress(finished, total, item);
                    },
                    (err, assets: T) => {
                        //加载成功
                        if (err) {
                            jm.Log.error(err);
                            option.fail && option.fail(err);
                        } else {
                            option.success && option.success(assets);
                        }
                    });
            },
            fail: option.fail,
        })
    }
    /**
     * 预加载bundle 资源
     * @param option 
     */
    public static preloadBundleRes(option: preloadBundleResParam) {
        this.loadBundle({
            bundleName: option.bundleName,
            success: (bundle: AssetManager.Bundle) => {
                if (option.type === SceneAsset) {
                    bundle.preloadScene(option.path, option.type,
                        (finished: number, total: number, item: AssetManager.RequestItem) => {
                            //加载过程
                            option.onProgress && option.onProgress(finished, total, item);
                        },
                        (err: Error) => {
                            //加载成功
                            if (err) {
                                jm.Log.warn(err);
                                option.fail && option.fail(err);
                            } else {
                                option.success && option.success();
                            }
                        })
                } else {
                    bundle.preload(option.path, option.type,
                        (finish, total, item) => {
                            //加载过程
                            option.onProgress && option.onProgress(finish, total, item);
                        },
                        (err: Error, items: AssetManager.RequestItem[]) => {
                            //加载成功
                            if (err) {
                                jm.Log.warn(err);
                                option.fail && option.fail(err);
                            } else {
                                option.success && option.success(items);
                            }
                        });
                }
            },
            fail: option.fail,
        })
    }
    /**
     * 加载场景
     * @param option 
     */
    public static loadBundleScene(option: loadBundleSceneParam) {
        this.loadBundle({
            bundleName: option.bundleName,
            success: () => {
                director.preloadScene(option.path,
                    (finished: number, total: number, item: AssetManager.RequestItem) => {
                        //加载过程
                        option.onProgress && option.onProgress(finished, total, item);
                    },
                    (error: null | Error, sceneAsset?: SceneAsset) => {
                        //加载成功
                        if (error) {
                            jm.Log.error(error);
                            option.fail && option.fail(error);
                        } else {
                            option.loadSuccess && option.loadSuccess();
                            director.loadScene(option.path, (error: null | Error, scene?: Scene) => {
                                if (error) {
                                    jm.Log.error(error);
                                    option.fail && option.fail(error);
                                } else {
                                    option.success && option.success(scene);
                                }
                            })
                        }
                    })
                // bundle.preloadScene(option.path,
                //     (finish: number, total: number, item: AssetManager.RequestItem) => {
                //         //加载过程
                //         option.onProgress && option.onProgress(finish, total, item);
                //     },
                //     (err: Error) => {
                //         if (err) {
                //             option.fail && option.fail(err);
                //             return;
                //         }
                //         //切换场景
                //         bundle.loadScene(
                //             option.path,
                //             (err: Error, sceneAsset: SceneAsset) => {
                //                 //加载结果
                //                 if (err) {
                //                     option.fail && option.fail(err);
                //                 } else {
                //                     option.success && option.success(sceneAsset);
                //                 }
                //             }
                //         );
                //     })
            },
            fail: option.fail,
        })
    }

    /**
     * 加载文件夹下同类型资源
     * @param option 
     */
    public static loadBundleFromDir(option: loadBundleFromDirParam) {
        loader.loadResDir(option.path, option.type, (error, assets, urls) => {
            if (error) {
                jm.Log.error(error);
                option.fail;
            } else {
                option.success(assets);
            }
        })
    }
}