
import { js, Node, director, Scene, find, Vec3, Canvas, UITransform, Prefab, instantiate } from "cc";
import JMBaseScene from "../base/JMBaseScene";
import baseView, { JMViewType } from "../base/JMBaseView";
import { JMConfig } from "../JMConfig";
import { JMPlatform, JMSys } from "../JMSys";
import JMCommonUtil from "../utils/JMCommonUtil";
import JMUIUtil from "../utils/JMUIUtil";
import JMAssetsMgr from "./JMAssetsMgr";

interface JMViewClass<T extends baseView> {
    new(): T;
    getPrefabUrl(): string;
    getViewType(): JMViewType;
    getViewZorder(): number;
    getBundleName(): string;
    getWipePlatforms(): Array<JMPlatform>;
    getWipePlatformsDesc(): string;
}

interface JMSceneClass<T extends JMBaseScene> {
    new(): T;
    getSceneUrl(): string;
    getBundleName(): string;
}
interface loadingViewIF {
    msg?: string,//显示内容
    progress?: number,//显示进度
    time?: number,//超时时间
    /**延时多久显示 */
    delayed?: number,
    /**1：资源加载 2：网络加载 */
    type?: 1 | 2,
    timeOutFunc?: (key: string) => void;//超时回调
}
/**
 * 基础视图
 */
export class JMBaseView extends baseView {
    /**
     * 显示
     * @param this 
     * @param param 
     * JMBaseView.show().then((self) => { jm.Log.log('2222'); })
     * jm.Log.log('1111');
     * 先打印1111 在打印2222 并且2222是在视图显示之后才执行的 加载后第二次先打印2222  在打印1111
     */
    public static show<T extends JMBaseView>(this: new () => T, ...param) {
        let _system = {
            then: (showFunc: (prefabClass: T) => void) => {
                let property = JMViewMgr.getViewProperty(<any>this);
                if (property) {
                    property.showFunc = showFunc;
                    property.prefabClass = this;
                    let node = property.node;
                    if (JMUIUtil.nodeIsValid(node) && node.active == true) {
                        property.showFunc && property.showFunc(node.getComponent(property.prefabClass));
                        delete property.showFunc;
                    }
                }
            }
        }
        JMViewMgr.showView(<any>this, ...param);
        return _system;
    }
    /**
     * 隐藏
     * @param this 
     * @param param 
     */
    public static hide<T extends JMBaseView>(this: new () => T, ...param) {
        JMViewMgr.hideView(<any>this, ...param);
    }
    /**
     * 清理视图
     */
    public static clear<T extends JMBaseView>(this: new () => T) {
        JMViewMgr.clearView(<any>this);
    }
    /**是否显示 如果正在加载中也属于显示 */
    public static isShow<T extends JMBaseView>(this: new () => T): boolean {
        return JMViewMgr.viewIsShow(<any>this);
    }
    /**执行方法 只有当节点显示，并且已经加载过后才有效
     * 
     * ***View.then((t) => {
     *     t.demo();
     * })
     */
    public static then<T extends JMBaseView>(this: new () => T, callback: (comp: T) => void) {
        if (JMViewMgr.viewIsShow(<any>this)) {
            let _comp = JMViewMgr.getViewClass(<any>this);
            if (_comp && _comp.node.active) {
                callback(_comp as any);
            }
        }
    }
    /**是否加载完成 */
    public static isLoadEnd<T extends JMBaseView>(this: new () => T): boolean {
        let prefabClass = JMViewMgr.getViewProperty(<any>this);
        return prefabClass && prefabClass.status == viewLoadStatus.Success;
    }
}

enum viewLoadStatus {
    none,//未知
    Err,//加载失败
    Success,//加载成功
    Load,//正在加载
    Destroy,//已销毁
}

/**
 * 视图属性
 */
interface viewProperty {
    status: viewLoadStatus;
    active: boolean;//视图状态
    node: Node;//节点对象
    viewType: JMViewType;//视图类型
    zIndex: number;//视图层级（节点未填入的时候生效）
    showFunc?: (prefabClass: any) => void,//执行onshow后调用的方法
    prefabClass?: any,//预制板顶的类，给showFunc使用
    param?: any;//参数
}

/**
 * UI管理器
 */
export default class JMViewMgr {
    private static baseViewList: Array<Node> = [];//基础视图节点
    private static viewList: { [key: string]: viewProperty } = js.createMap();

    public static createAllBaseNode() {
        for (let i = 0; i < JMViewType.Max; i++) {
            this.baseViewList[i] = this._createBaseNode(this.baseViewList[i]);
        }
    }
    public static getBaseNode(type: JMViewType) {
        if (!JMUIUtil.nodeIsValid(this.baseViewList[type])) {
            this.createAllBaseNode();
            // this.baseViewList[type] = this._createBaseNode(this.baseViewList[type], type);
        }
        return this.baseViewList[type];
    }
    public static removeAllBaseNode() {
        for (let i = 0; i < JMViewType.Max; i++) {
            this._clearBaseNode(this.baseViewList[i]);
        }
        this.baseViewList = [];
    }
    private static _clearBaseNode(node: Node) {
        node && JMUIUtil.nodeIsValid(node) && node.destroy() && (node = null);
    }
    private static _createBaseNode(node: Node): Node {
        if (JMUIUtil.nodeIsValid(node)) return node;
        let _parent: Node = null;
        node = new Node();
        // if (viewType >= JMViewType.SysTips) {
        //     //加入到常驻节点
        //     game.addPersistRootNode(node);
        //     _parent = director.getScene();
        //     let winSize = JMUIUtil.getViewSize();
        //     node.setPosition(new Vec3(winSize.width / 2, winSize.height / 2));
        // } else {
        _parent = find('Canvas');
        if (!_parent) {
            if (!director.getScene()) return null;
            _parent = new Node('Canvas');
            _parent.addComponent(UITransform);
            _parent.addComponent(Canvas);
            director.getScene().addChild(_parent);
        }
        node.setPosition(new Vec3(0, 0));
        // }
        _parent.addChild(node);
        return node;
    }
    private static _showLoading(key: string, progress?: number) {
        //loading功能不走loading
        if (key == JMConfig.sysViewPath + 'LoadingView') return;
        this.showLoading(key, {
            type: 1,
            progress: progress,
            delayed: 0.3,
        });
    }
    private static _hideLoading(key: string) {
        if (key == JMConfig.sysViewPath + 'LoadingView') return;
        this.hideLoading(key);
    }
    /**
     * 切换场景视图
     * @param prefabClass 
     * @param param 
     */
    private static _showSceneKey: string;
    public static showScenePath(option: { sceneUrl: string, bundleName?: string }, ...param): boolean {
        let url: string = option.sceneUrl;
        let bundleName = option.bundleName || 'resources';
        let key = bundleName + '-' + url;
        if (this._showSceneKey && key == this._showSceneKey) {
            jm.Log.warn('当前场景已加载：' + key);
            return false;
        }
        this._showSceneKey = key;
        let self = this;
        self._showLoading(url);
        JMAssetsMgr.loadBundleScene({
            bundleName: bundleName,
            path: url,
            onProgress: (finish: number, total: number) => {
                //加载过程
                self._showLoading(url, finish / total);
            },
            loadSuccess: () => {
                this._loadingTask = [];
                this.viewList = {};
                this.baseViewList = [];
            },
            success: (scene: Scene) => {
                //启动后
                let canvas = find('Canvas');
                if (canvas) {
                    let _baseClass = canvas.getComponent(JMBaseScene);
                    _baseClass && _baseClass.onShow(...param);
                    jm.Event.emit('sys.SceneSwitch');
                }
                //切换场景
                // director.runSceneImmediate(
                //     scene,
                //     () => {
                //         //启动前
                //     },
                //     () => {
                //         //启动后
                //         let canvas = find('Canvas');
                //         if (canvas) {
                //             let _baseClass = canvas.getComponent(JMBaseScene);
                //             _baseClass && _baseClass.onShow(...param);
                //         }
                //     }
                // );
            },
            fail: (err) => {
                self._hideLoading(url);
                jm.AlertView.show({
                    msg: '程序发生异常，请您重进游戏',
                    callBackC: () => {
                        JMCommonUtil.restart();
                    }
                })
                // JMViewMgr.showView(xdExitTipsView, '程序发生异常，请您退出游戏重试？');
                jm.Log.error(err);
            },
        });
        return true;
    }
    /**
     * 显示场景
     * @param prefabClass 
     * @param param 
     */
    public static showScene<T extends JMBaseScene>(prefabClass: JMSceneClass<T>, ...param: any): boolean {
        let url: string = prefabClass.getSceneUrl();
        let bundleName: string = prefabClass.getBundleName();
        return this.showScenePath({ sceneUrl: url, bundleName: bundleName }, ...param);
    }
    /**
     * 场景是否显示 用来判断指定场景类是否显示
     * @param option 
     */
    public static sceneIsShowPath(option: { sceneUrl: string, bundleName?: string }): boolean {
        let url: string = option.sceneUrl;
        let bundleName = option.bundleName || 'resources';
        let key = bundleName + '-' + url;
        if (this._showSceneKey && key == this._showSceneKey) {
            return true;
        }
        return false;
    }
    /**
     * 场景是否显示 用来判断指定场景类是否显示
     * @param prefabClass 
     */
    public static sceneIsShow<T extends JMBaseScene>(prefabClass: JMSceneClass<T>): boolean {
        let url: string = prefabClass.getSceneUrl();
        let bundleName: string = prefabClass.getBundleName();
        return this.sceneIsShowPath({ sceneUrl: url, bundleName: bundleName });
    }
    /**
     * 获取自动层级
     * @param type 
     */
    private static _getAutozIndex(type: JMViewType): number {
        let _index = 0, list: Array<viewProperty> = [];
        for (let key in this.viewList) {
            const item = this.viewList[key];
            if (item.viewType == type && item.node && item.node.active && item.zIndex < 100) {
                list.push(item);
            }
        }
        list.sort(function (a, b) {
            return a.zIndex - b.zIndex;
        })
        for (let i = 0, length = list.length; i < length; i++) {
            const item = list[i];
            item.zIndex = i;
            _index = i + 1;
        }
        return _index;
    }
    /**自动调整视图层级 */
    private static autoUpViewIndex(viewType: JMViewType) {
        let list: Array<viewProperty> = [];
        for (let key in this.viewList) {
            const item = this.viewList[key];
            if (item.viewType == viewType && item.node && item.node.active) {
                list.push(item);
            }
        }
        list.sort(function (a, b) {
            return a.zIndex - b.zIndex;
        })
        for (let i = 0, length = list.length; i < length; i++) {
            const item = list[i];
            if (item.node) {
                item.node.setSiblingIndex(i);
            }
        }
    }
    /**
     * 通过属性值创建界面
     * @param prop 属性值
     * @param param 参数
     */
    public static showViewByProp(prop: { vt: JMViewType, url: string, bn: string, zo: number }, ...param: any) {
        let viewType: JMViewType = prop.vt;
        let url: string = prop.url;
        let bundleName: string = prop.bn;
        let zOrder: number = prop.zo;
        if (!this.viewList[url]) {
            this.viewList[url] = {
                status: viewLoadStatus.none,
                active: true,
                node: null,
                viewType: viewType,
                zIndex: zOrder,
            };
            if (zOrder != 0) {
                //自定义层级在自动层级之上
                this.viewList[url].zIndex += 100;
            }
        }
        //替换参数
        var args = [];
        for (var i = 1; i < arguments.length; i++) args.push(arguments[i]);
        this.viewList[url].param = args;

        if (zOrder == 0) {
            //自动层级
            this.viewList[url].zIndex = this._getAutozIndex(viewType);
        }
        let viewData: viewProperty = this.viewList[url];
        viewData.active = true;
        if (viewData.status == viewLoadStatus.Success && JMUIUtil.nodeIsValid(viewData.node)) {
            let baseNode: JMBaseView = viewData.node.getComponent(JMBaseView);
            viewData.node.active = viewData.active;
            baseNode.onShow.apply(baseNode, this.viewList[url].param);
            this.autoUpViewIndex(viewData.viewType);
            //适配es6写法
            viewData.showFunc && viewData.prefabClass && viewData.showFunc(baseNode.getComponent(viewData.prefabClass));
            delete viewData.showFunc;
            delete viewData.prefabClass;
            jm.Event.emit(url, 'show');
        } else {
            //避免重复加载
            if (viewData.status == viewLoadStatus.Load) {
                return;
            }
            viewData.status = viewLoadStatus.Load;
            let self = this;
            self._showLoading(url);
            JMAssetsMgr.loadBundleRes({
                bundleName: bundleName,
                path: url,
                type: Prefab,
                onProgress: (finish, total) => {
                    //加载过程
                    self._showLoading(url, finish / total);
                },
                success: (assets: Prefab) => {
                    if (!this.viewList[url]) {
                        self._hideLoading(url);
                        return
                    };
                    //加载成功
                    if (this.viewList[url].status != viewLoadStatus.Destroy) {
                        this.viewList[url].status = viewLoadStatus.Success;
                        let node: Node = instantiate(assets);
                        let baseTs = node.getComponent(JMBaseView);
                        baseTs.loadBackImg(() => {
                            self._hideLoading(url);
                            node.parent = this.getBaseNode(viewType);
                            node.active = this.viewList[url].active;
                            this.viewList[url].node = node;
                            baseTs.setViewKey(url);
                            if (node.active) {
                                baseTs.onShow.apply(baseTs, this.viewList[url].param);
                            }
                            this.autoUpViewIndex(this.viewList[url].viewType);
                            //适配es6写法
                            this.viewList[url].showFunc && this.viewList[url].prefabClass && this.viewList[url].showFunc(baseTs.getComponent(this.viewList[url].prefabClass));
                            delete this.viewList[url].showFunc;
                            delete this.viewList[url].prefabClass;
                            jm.Event.emit(url, 'show');
                        })
                    } else {
                        self._hideLoading(url);
                        delete this.viewList[url].showFunc;
                        delete this.viewList[url].prefabClass;
                    }
                },
                fail: (err) => {
                    self._hideLoading(url);
                    if (!this.viewList[url]) return;
                    jm.Log.error(err);
                    this.viewList[url].status = viewLoadStatus.Err;
                    delete this.viewList[url].showFunc;
                    delete this.viewList[url].prefabClass;
                },
            })
        }
    }
    /**
     * 显示视图
     * @param prefabClass 预制绑定类
     * @param param 参数值
     */
    public static showView<T extends JMBaseView>(prefabClass: JMViewClass<T>, ...param: any) {
        let viewType: JMViewType = prefabClass.getViewType();
        let url: string = prefabClass.getPrefabUrl();
        let bundleName: string = prefabClass.getBundleName();
        let zOrder: number = prefabClass.getViewZorder();
        let wipePlatforms = prefabClass.getWipePlatforms();
        let wipePlatformsDesc = prefabClass.getWipePlatformsDesc();
        for (let i = 0, length = wipePlatforms.length; i < length; i++) {
            if (JMSys.platform == wipePlatforms[i]) {
                jm.Log.warn('被屏蔽的功能:', url);
                if (wipePlatformsDesc) {
                    jm.SoftTipsView.show(wipePlatformsDesc);
                }
                return;
            }
        }
        this.showViewByProp({
            vt: viewType,
            url: url,
            bn: bundleName,
            zo: zOrder
        }, ...param);
    }
    private static _hideView(_view: viewProperty, key: string, ...param) {
        if (_view) {
            _view.active = false;
            delete _view.showFunc;
            delete _view.prefabClass;
            if (_view.status == viewLoadStatus.Success && JMUIUtil.nodeIsValid(_view.node)) {
                _view.node.active = false;
                jm.Event.emit(key, 'hide');
            }
        }
    }
    /**
     * 隐藏视图
     * @param prefabClass 
     * @param param 
     */
    public static hideView<T extends JMBaseView>(prefabClass: JMViewClass<T>, ...param: any) {
        this._hideView(this.getViewProperty(prefabClass), prefabClass.getPrefabUrl(), ...param);
    }
    public static hideViewByKey(key: string, ...param) {
        this._hideView(this.viewList[key], key, ...param);
    }
    /**视图是否显示 */
    public static viewIsShow<T extends JMBaseView>(prefabClass: JMViewClass<T>): boolean {
        let _view = this.getViewProperty(prefabClass);
        if (_view && _view.active) {
            return true;
        }
        return false;
    }
    /**
     * 获取视图属性
     * @param prefabClass 
     */
    public static getViewProperty<T extends JMBaseView>(prefabClass: JMViewClass<T>): viewProperty {
        let _view = this.viewList[prefabClass.getPrefabUrl()];
        return _view ? _view : null;
    }
    /**
     * 获取视图节点
     * @param prefabClass 
     */
    public static getViewNode<T extends JMBaseView>(prefabClass: JMViewClass<T>): Node {
        let _view = this.getViewProperty(prefabClass);
        return _view ? _view.node : null;
    }
    /**
     * 获取视图类
     * @param prefabClass 
     */
    public static getViewClass<T extends JMBaseView>(prefabClass: JMViewClass<T>): T {
        let _node = this.getViewNode(prefabClass);
        if (_node) {
            let _class = _node.getComponent(prefabClass);
            return _class;
        } else {
            return null;
        }
    }
    private static _clearView(_view: viewProperty, ...param) {
        if (_view) {
            _view.active = false;
            if (_view.status == viewLoadStatus.Success) {
                _view.status = viewLoadStatus.Destroy;
                if (JMUIUtil.nodeIsValid(_view.node)) {
                    _view.node.getComponent(JMBaseView).onClear(...param);
                    _view.node.destroy();
                }
            }
        }
    }
    /**
     * 清除视图
     * @param prefabClass 
     * @param param 
     */
    public static clearView<T extends JMBaseView>(prefabClass: JMViewClass<T>, ...param: any) {
        this._clearView(this.getViewProperty(prefabClass));
        delete this.viewList[prefabClass.getPrefabUrl()];
    }
    public static clearViewByKey(key: string) {
        this._clearView(this.viewList[key]);
        delete this.viewList[key];
    }
    /**
     * 隐藏视图通过类型
     * @param viewType 视图类型
     */
    public static hideViewByType(viewType: JMViewType) {
        for (const key in this.viewList) {
            const element = this.viewList[key];
            if (element) {
                if (element.viewType == viewType) {
                    this._hideView(element, key);
                }
            }
        }
    }
    /**
     * 隐藏所有视图
     */
    public static hideAllView() {
        for (const key in this.viewList) {
            const element = this.viewList[key];
            if (element) {
                this._hideView(element, key);
            }
        }
    }
    /**
     * 清除视图通过类型
     * @param viewType 视图类型
     */
    public static clearViewByType(viewType: JMViewType) {
        let _clearViewKey: Array<string> = [];
        for (const key in this.viewList) {
            const element = this.viewList[key];
            if (element) {
                if (element.viewType == viewType) {
                    this._clearView(element);
                    _clearViewKey.push(key);
                }
            }
        }
        _clearViewKey.forEach(key => {
            delete this.viewList[key];
        });
    }
    /**
     * 清除所有视图
     */
    public static clearAllView() {
        for (const key in this.viewList) {
            const element = this.viewList[key];
            if (element) {
                this._clearView(element);
            }
        }
        this.viewList = {};
    }
    /**
     * loading视图管理
     * 加载这里是因为ts语言循环引用会造成问题，loading在视图管理器中使用到了无法在加载视图管理器
     */
    private static _loadingTask: Array<{ key: string, option: loadingViewIF }> = [];
    /**
     * 显示loading
     * @param key loading key
     * @param option 参数
     */
    public static showLoading(key: string, option?: loadingViewIF) {
        option = option || {};
        let _has = false;
        for (let i = 0, length = this._loadingTask.length; i < length; i++) {
            let element = this._loadingTask[i];
            if (element.key == key) {
                element.option == option;
                _has = true;
                break;
            }
        }
        if (!_has) {
            this._loadingTask.unshift({
                key: key,
                option: option,
            })
        }
        let obj = this._loadingTask[this._loadingTask.length - 1];
        if (obj.key == key) {
            jm.LoadingView.show(obj.key, {
                msg: option.msg,//显示内容
                progress: option.progress,//显示进度
                time: option.time,//超时时间
                type: option.type,
                delayed: option.delayed,
                timeOutFunc: (key) => {
                    this.hideLoading(key);
                }//超时回调
            });
        }
    }
    /**
     * 隐藏loading
     * @param key 
     */
    public static hideLoading(key: string) {
        let isfind = false;
        for (let i = 0, length = this._loadingTask.length; i < length; i++) {
            let element = this._loadingTask[i];
            if (element.key == key) {
                this._loadingTask.splice(i, 1);
                isfind = true;
                break;
            }
        }
        if (!isfind) return;
        if (this._loadingTask.length == 0) {
            jm.LoadingView.hide();
        } else {
            let obj = this._loadingTask[this._loadingTask.length - 1];
            jm.LoadingView.show(obj.key, {
                msg: obj.option.msg,//显示内容
                progress: obj.option.progress,//显示进度
                time: obj.option.time,//超时时间
                type: obj.option.type,
                delayed: obj.option.delayed,
                timeOutFunc: (key) => {
                    this.hideLoading(key);
                }//超时回调
            });
        }
    }
    /**
     * 隐藏所有loading
     */
    public static hideAllLoading() {
        this._loadingTask = [];
        jm.LoadingView.hide();
    }
}

export const xdInitViewMgr = function () {
    jm.ViewMgr = JMViewMgr as any;
}