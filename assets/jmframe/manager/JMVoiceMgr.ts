import JMSingleton from "../base/JMSingleton";
import { JMNativeEvent } from "../event/JMNativeEvent";
import { JMPlatform, JMSys } from "../JMSys";
import JMHttp from "../net/JMHttp";
import JMDataUtil from "../utils/JMDataUtil";
import JMSoundMgr from "./JMSoundMgr";

export enum JMVoiceStatus {
    /**休息状态 */
    none = 1,
    /**正在录音 */
    recording,
    /**播放录音 */
    playRecording,
}

interface baseVoice {
    /**开始录音 */
    startRecord(success: (path: string) => void, fail?: () => void): boolean;
    /**结束录音 */
    stopRecord(): boolean;
    /**取消录音 */
    cancelRecord(): boolean;
    /**播放录音 */
    playRecord(url: string, callbackStart: (code: string) => void, callbackEnd: (code: string) => void);
    /**语音状态 */
    getStatus(): JMVoiceStatus;
}

export default class JMVoiceMgr extends JMSingleton {
    protected static singletonType: SingletonType = 'sysMgr';
    private _voice: baseVoice = null;
    private get voice(): baseVoice {
        if (!this._voice) {
            switch (JMSys.platform) {
                case JMPlatform.JSB: this._voice = new nativeVoice(); break;
                case JMPlatform.WXGAME: this._voice = new wxGameVoice(); break;
            }
        }
        return this._voice;
    }
    /**是否支持 */
    isSupport(): boolean {
        return !!this.voice;
    }
    /**开始录音 */
    startRecord(success: (info: string) => void, fail?: () => void): boolean {
        if (!this.isSupport()) return false;
        return this.voice.startRecord(success, fail);
    }
    /**结束录音 */
    stopRecord(): boolean {
        if (!this.isSupport()) return false;
        return this.voice.stopRecord();
    }
    /**取消录音 */
    cancelRecord(): boolean {
        if (!this.isSupport()) return false;
        return this.voice.cancelRecord();
    }
    /**播放录音 */
    playRecord(url: string, callbackStart: (code: string) => void, callbackEnd: (code: string) => void): boolean {
        if (JMDataUtil.isURL(url)) {
            var urlArr = url.split('?');
            url = urlArr[0];
            if (url.endsWith('.mp3') || url.endsWith('.wav') || url.endsWith('.ogg') || url.endsWith('m4a')) {
                callbackStart && callbackStart('downloadStart');
                let bo = JMSoundMgr.instance().playEffect({
                    path: url,
                    isVoice: true,
                    startPay: () => {
                        callbackStart && callbackStart('playStart');
                    },//开始播放
                    fail: () => {
                        callbackEnd && callbackEnd('downloadFail');
                    },//加载失败
                    playEnd: () => {
                        callbackEnd && callbackEnd('playStartSuccess');
                    },//播放结束
                })
                if (!bo) {
                    callbackEnd && callbackEnd('downloadFail');
                }
                return;
            }
        }
        if (!this.isSupport()) return false;
        return this.voice.playRecord(url, callbackStart, callbackEnd);
    }
    /**语音状态 */
    getStatus(): JMVoiceStatus {
        if (!this.isSupport()) return JMVoiceStatus.none;
        return this.voice.getStatus();
    }
}

/**原生录音系统 */
class nativeVoice implements baseVoice {
    private voiceStatus: JMVoiceStatus = JMVoiceStatus.none;
    private recordEnd: any = null
    private playEnd: any = null
    constructor() {
        jm.Event.on(JMNativeEvent.reCallBack, this.onReCallBack, this)
        jm.Event.on(JMNativeEvent.rePlayFinish, this.onRePlayFinish, this)
    }
    //录音结束
    private onReCallBack(path) {
        this.recordEnd && this.recordEnd(path);
        this.recordEnd = null
        this.voiceStatus = JMVoiceStatus.none;
    }
    //播放结束 fail
    private onRePlayFinish(str) {
        this.voiceStatus = JMVoiceStatus.none;
        this.playEnd && this.playEnd('playStartSuccess');
        this.playEnd = null
    }
    /**开始录音 */
    startRecord(success: (path: string) => void, fail?: () => void): boolean {
        if (this.voiceStatus != JMVoiceStatus.none) return false;
        this.voiceStatus = JMVoiceStatus.recording;
        jm.Log.log('开始录音', this.voiceStatus);
        if (jm.toNative('RecordStart') !== 'ok') {
            fail && fail();
            this.voiceStatus = JMVoiceStatus.none;
        }
        this.recordEnd = success
        return true;
    }
    /**结束录音 */
    stopRecord(): boolean {
        if (this.voiceStatus != JMVoiceStatus.recording) return false;
        jm.toNative('RecordStop')
        return true;
    }
    /**取消录音 */
    cancelRecord(): boolean {
        if (this.voiceStatus != JMVoiceStatus.recording) return false;
        this.voiceStatus = JMVoiceStatus.none;
        jm.toNative('RecordStop')
        return true;
    }
    /**播放录音 */
    playRecord(url: string, callbackStart: (code: string) => void, callbackEnd: (code: string) => void) {
        if (this.voiceStatus != JMVoiceStatus.none) return;
        callbackStart && callbackStart('downloadStart');
        JMHttp.send({
            url: url,
            method: 'GET',
            timeout: 5000,//5s超时
            data: {},
            success: (res) => {
                if (this.voiceStatus != JMVoiceStatus.none) {
                    callbackEnd && callbackEnd('playStartFail');
                    return;
                }
                if (jm.toNative('RecordPlay', res) == 'ok') {
                    callbackStart && callbackStart('playStart');
                    this.voiceStatus = JMVoiceStatus.playRecording;
                    this.playEnd = callbackEnd
                } else {
                    this.voiceStatus = JMVoiceStatus.none;
                    callbackEnd && callbackEnd('playStartFail');
                }
            },
            fail: () => {
                callbackEnd && callbackEnd('downloadFail');
            },
        })
    }
    /**语音状态 */
    getStatus(): JMVoiceStatus {
        return this.voiceStatus;
    }
}

class wxGameVoice implements baseVoice {
    private voiceStatus: JMVoiceStatus = JMVoiceStatus.none;
    private recordEnd: (path: string) => void;
    private recordFail: () => void;
    private get recorderMgr() {
        return wx.getRecorderManager();
    }
    constructor() {
        this.recorderMgr.onError((res) => {
            this.recordFail && this.recordFail();
            this.recordFail = null;
            this.voiceStatus = JMVoiceStatus.none;
        })
        this.recorderMgr.onInterruptionBegin(() => {
            this.cancelRecord();
        })
        this.recorderMgr.onStart(() => {

        })
        this.recorderMgr.onStop((res) => {
            this.recordEnd && this.recordEnd(res.tempFilePath);
            this.recordEnd = null
            this.voiceStatus = JMVoiceStatus.none;
        })
    }
    startRecord(success: (path: string) => void, fail?: () => void): boolean {
        if (this.voiceStatus != JMVoiceStatus.none) return false;
        this.voiceStatus = JMVoiceStatus.recording;
        this.recordEnd = success;
        this.recordFail = fail;
        jm.Log.log('开始录音', this.voiceStatus);
        this.recorderMgr.start({
            format: 'mp3',
        });
        return true;
    }
    stopRecord(): boolean {
        if (this.voiceStatus != JMVoiceStatus.recording) return false;
        this.recorderMgr.stop();
        return true;
    }
    cancelRecord(): boolean {
        if (this.voiceStatus != JMVoiceStatus.recording) return false;
        this.voiceStatus = JMVoiceStatus.none;
        this.recorderMgr.stop();
        return true;
    }
    playRecord(url: string, callbackStart: (code: string) => void, callbackEnd: (code: string) => void) {
        if (this.voiceStatus != JMVoiceStatus.none) return;
        callbackStart && callbackStart('playStart');
        callbackEnd && callbackEnd('playStartFail');
    }
    getStatus(): JMVoiceStatus {
        return this.voiceStatus;
    }

}