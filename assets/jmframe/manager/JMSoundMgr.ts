/**
 * 声音管理器
 */

import JMAssetsManager from "./JMAssetsMgr";
import { AudioClip, Node, js, AudioSource, director, loader, __private } from "cc";
import JMSingleton from "../base/JMSingleton";
import JMSysData from "../data/JMSysData";

enum playStatus {
    play = 1,//播放
    stop,//停止
    pause//暂停
}

interface playSoundIF {
    loop?: boolean,//是否重复
}

interface playSoundVIF extends playSoundIF {
    path: string,//路径 模块名:路径 默认resources模块
    isVoice?: boolean,//是否是录音
    startPay?: () => void,//开始播放
    success?: (clup: AudioClip) => void,//加载成功
    fail?: () => void,//加载失败
    playEnd?: () => void,//播放结束
}

interface playSoundIFS extends playSoundIF {
    status: playStatus,//状态
    audioID?: number,//音频 id
    path?: string,//路径 模块名:路径 默认resources模块
    audio: AudioSource,//音频对象
    id?: string,//音频id
}

export default class JMSoundManager extends JMSingleton {
    protected static singletonType: SingletonType = 'sysMgr';
    //音效
    private effectAudioClips: { [key: string]: playSoundIFS } = js.createMap();
    //背景音乐
    private musicAudioClip: playSoundIFS;
    private _music: AudioSource = null;
    /**背景音乐 */
    private get music(): AudioSource {
        if (!this._music || !this._music.isValid) {
            let node = new Node('music');
            this._music = node.addComponent(AudioSource);
            this._music.loop = true;
            this._music.playOnAwake = true;
            this._music.volume = JMSysData.instance().musicVolume;
            // director.getScene().addChild(node);
        }
        return this._music;
    }
    private effect(): AudioSource {
        let node = new Node('effect');
        let audio = node.addComponent(AudioSource);
        audio.loop = false;
        audio.playOnAwake = true;
        audio.volume = JMSysData.instance().effectVolume;
        // director.getScene().addChild(node);
        return audio;
    }
    onInit() {
        this.musicAudioClip = {
            status: playStatus.play,
            loop: false,
            path: null,
            audio: null,
        };
    }
    onRecycle() {
        this.stopAllEffect();
        this.stopMusic();
    }
    /**
     * 播放背景音乐
     * @param option 
     */
    public playMusic(option: playSoundVIF) {
        if (!JMSysData.instance().musicOpen) {
            return;
        }
        if (this.musicAudioClip.status == playStatus.play && this.musicAudioClip.path == option.path) return;
        let loop = option.loop, path = option.path;
        if (typeof loop == 'undefined') {
            loop = true;
        }
        this.musicAudioClip.status = playStatus.play;
        this.musicAudioClip.loop = loop;
        this.musicAudioClip.path = path;

        let bundleName: string, url: string;
        if (path.indexOf(':') != -1) {
            let arr: Array<string> = path.split(':');
            bundleName = arr[0];
            url = arr[1];
        } else {
            bundleName = 'resources'
            url = path;
        }
        JMAssetsManager.loadBundleRes({
            bundleName: bundleName,
            path: url,
            type: AudioClip,
            success: (clip: AudioClip) => {
                option.success && option.success(clip);
                if (this.musicAudioClip.path === option.path &&
                    this.musicAudioClip.status != playStatus.stop) {
                    option.startPay && option.startPay();
                    this.music.clip = clip;
                    this.music.play();
                    if (option.playEnd) {
                        clip.on('ended', option.playEnd);
                    }
                    if (this.musicAudioClip.status == playStatus.pause) {
                        this.pauseMusic();
                    }
                }
            },
            fail: (err) => {
                jm.Log.warn('加载音乐失败：' + path);
                option.fail && option.fail();
            },
        })
    }
    /**
     * 播放音效
     * @param option 
     * @returns id
     */
    public playEffect(option: playSoundVIF): string {
        if (!JMSysData.instance().effectOpen && !option.isVoice) return;
        if (!JMSysData.instance().voiceOpen && option.isVoice) return;

        let loop = option.loop, path = option.path;
        if (typeof loop == 'undefined') {
            loop = false;
        }
        let success = (clip: AudioClip) => {
            option.success && option.success(clip);
            if (this.effectAudioClips[coment.id] &&
                coment.status != playStatus.stop) {
                option.startPay && option.startPay();
                coment.audio.clip = clip;
                coment.audio.play();
                if (coment.status == playStatus.pause) {
                    this.pauseEffect(path);
                }
                if (!option.loop) {
                    clip.on('ended', () => {
                        if (coment) {
                            if (coment.audio.node)
                                coment.audio.node.destroy();
                            this.effectAudioClips[coment.id] && delete this.effectAudioClips[coment.id];
                        }
                        option.playEnd && option.playEnd();
                    });
                }
            }
        }
        let fail = (err) => {
            jm.Log.warn('加载音效失败：' + path);
            option.fail && option.fail();
        }
        let coment = {
            status: playStatus.play,
            loop: loop,
            audio: this.effect(),
            id: Date.now().toString(),
        }
        this.effectAudioClips[coment.id] = coment;
        let bundleName: string, url: string = path;
        if (url.startsWith('http://') || url.startsWith('https://')) {
            //外网加载
            bundleName = null;
            let loadParam = {
                url: url,
                type: '',
            };
            if (url.endsWith('mp3')) {
                loadParam.type = 'mp3';
            } else if (url.endsWith('ogg')) {
                loadParam.type = 'ogg';
            } else if (url.endsWith('wav')) {
                loadParam.type = 'wav';
            } else if (url.endsWith('m4a')) {
                loadParam.type = 'm4a';
            } else {
                jm.Log.warn('不支持的外部音频：', url);
                return;
            }
            loader.load(loadParam, function (err, clip: AudioClip) {
                if (err) {
                    fail(err);
                } else {
                    success(clip);
                }
            });
        } else {
            //内部加载
            if (path.indexOf(':') != -1) {
                let arr: Array<string> = path.split(':');
                bundleName = arr[0];
                url = arr[1];
            } else {
                bundleName = 'resources'
            }
            JMAssetsManager.loadBundleRes({
                bundleName: bundleName,
                path: url,
                type: AudioClip,
                success: (clip: AudioClip) => {
                    success(clip);
                },
                fail: (err) => {
                    fail(err);
                },
            })
        }
        return coment.id;
    }
    /**
     * 设置音乐大小
     * @param value 
     */
    public setMusicVolume(value: number) {
        JMSysData.instance().musicVolume = value;
        this.music.volume = value;
    }
    /**
     * 开启音乐
     */
    public openMusic() {
        JMSysData.instance().musicOpen = true;
        if (this.musicAudioClip.path) {
            this.playMusic({
                path: this.musicAudioClip.path,
            });
        }
    }
    /**
     * 停止音乐（背景音乐）
     */
    public stopMusic() {
        this.musicAudioClip.status = playStatus.stop;
        this.music.stop();
    }
    public closeMusic() {
        this.stopMusic();
        JMSysData.instance().musicOpen = false;
    }
    /**
     * 暂停音乐（背景音乐）
     */
    public pauseMusic() {
        this.musicAudioClip.status = playStatus.pause;
        this.music.pause();
    }
    /**
     * 恢复音乐（背景音乐）
     */
    public resumeMusic() {
        if (!JMSysData.instance().musicOpen) return;
        this.musicAudioClip.status = playStatus.play;
        this.music.play();
    }
    /**
     * 背景音乐是否正在播放
     */
    public isMusicPlaying(): boolean {
        return this.music.state == AudioSource.AudioState.PLAYING;

    }
    /**
     * 设置音效大小
     * @param value 
     */
    public setEffectVolume(value: number) {
        JMSysData.instance().effectVolume = value;
        for (const key in this.effectAudioClips) {
            const element = this.effectAudioClips[key];
            element.audio.volume = value;
        }
    }
    /**
     * 开启音效
     */
    public openEffect() {
        JMSysData.instance().effectOpen = true;
    }
    /**
     * 恢复音效
     * @param id 路径
     */
    public resumeEffect(id: string) {
        if (!JMSysData.instance().effectOpen) return;
        if (this.effectAudioClips[id] &&
            this.effectAudioClips[id].audioID &&
            this.effectAudioClips[id].status == playStatus.play) {
            this.effectAudioClips[id].status = playStatus.pause;
            this.effectAudioClips[id].audio.play();
        }
    }
    /**
     * 恢复所有音效
     */
    public resumeAllEffect() {
        if (!JMSysData.instance().effectOpen) return;
        for (const key in this.effectAudioClips) {
            this.resumeEffect(key);
        }
    }
    /**
     * 暂停音效
     * @param id 路径
     */
    public pauseEffect(id: string) {
        if (this.effectAudioClips[id] &&
            this.effectAudioClips[id].audioID &&
            this.effectAudioClips[id].status == playStatus.play) {
            this.effectAudioClips[id].status = playStatus.pause;
            this.effectAudioClips[id].audio.pause();
        }
    }
    /**
     * 暂停所有音效
     */
    public pauseAllEffect() {
        for (const key in this.effectAudioClips) {
            this.pauseEffect(key);
        }
    }
    /**
     * 停止音效
     * @param id 路径
     */
    public stopEffect(id: string) {
        if (this.effectAudioClips[id] &&
            this.effectAudioClips[id].audioID) {
            this.effectAudioClips[id].audio.stop();
            this.effectAudioClips[id].audio.node.destroy();
            delete this.effectAudioClips[id];
        }
    }
    /**
     * 停止所有音效
     */
    public stopAllEffect() {
        for (const key in this.effectAudioClips) {
            const element = this.effectAudioClips[key];
            element.audio.stop();
            element.audio.node.destroy();
        }
        this.effectAudioClips = {};
    }
    public closeEffect() {
        this.stopAllEffect();
        JMSysData.instance().effectOpen = false;
    }
}