import { js } from "cc";
import { XDBaseMgr } from "../base/XDBaseData";

/**基础管理类型 */
interface registerBaseMgrPlugInIF<T extends XDBaseMgr> { new(): T }
/**基础插件类型 */
interface registerBasePlugInIF<T extends xdBasePlugIn> { new(): T }

/**基础插件类 */
export class xdBasePlugIn {
    /**初始化 */
    protected init() { }
    /**销毁 */
    protected destory() { }
}

class demo extends xdBasePlugIn {

}
/**
 * 插件管理器
 * 插件系统会将注册的类在特定的环境下进行初始化
 */
export default class xdPlugInMgr {
    /**管理类型插件注册 */
    private static _mgrPlugInClass: { [key: string]: any } = js.createMap();
    /**插件类 */
    private static _plugInClass: { [key: string]: any } = js.createMap();
    /**注册管理类型 */
    public static registerBaseMgrPlugIn<T extends XDBaseMgr>(_obj: registerBaseMgrPlugInIF<T>) {
        let name = js.getClassName(<any>_obj) || (<any>_obj).name;
        this._mgrPlugInClass[name] = _obj;
    }
    /**执行基础管理插件初始化 */
    public static runBaseMgrPlugInInit() {
        for (let key in this._mgrPlugInClass) {
            const coment = this._mgrPlugInClass[key];
            coment.instance();
        }
    }
    /**销毁基础管理类插件 */
    public static destoryBaseMgrPlugIn() {
        this._mgrPlugInClass = {};
    }

    /**注册插件 */
    public static registerPlugIn<T extends xdBasePlugIn>(_obj: registerBasePlugInIF<T>) {
        let name = js.getClassName(<any>_obj) || (<any>_obj).name;
        this._plugInClass[name] = _obj;
    }
    /**执行插件 */
    public static runPlugInInit() {
        for (let key in this._plugInClass) {
            const coment = this._plugInClass[key];
            coment.init();
        }
    }
    /**销毁插件 */
    public static destoryPlugin() {
        for (let key in this._plugInClass) {
            const coment = this._plugInClass[key];
            coment.destory();
        }
        this._plugInClass = {};
    }
}
