import { JMPlatform, JMSys } from "../JMSys";

export default class JMFile {
    /**
     * 获取文件字符串数据
     * @param filename 
     */
    public static getStringFromFile(filename: string): string {
        if (JMSys.platform == JMPlatform.JSB) {
            if (this.isFileExist(filename)) {
                return jsb.fileUtils.getStringFromFile(filename);
            } else {
                return null;
            }
        }
        return null;
    }
    /**
     * 获取文件二进制数据
     * @param filename 
     */
    public static getDataFromFile(filename: string): Uint8Array {
        if (JMSys.platform == JMPlatform.JSB) {
            if (this.isFileExist(filename)) {
                return (jsb.fileUtils as any).getDataFromFile(filename);
            } else {
                return null;
            }
        }
        return null;
    }
    /**
     * 文件是否存在
     * @param filename 相对路径或者绝对路径 相对路径会去搜索路径优先选择
     */
    public static isFileExist(filename): boolean {
        if (JMSys.platform == JMPlatform.JSB) {
            return jsb.fileUtils.isFileExist(filename);
        }
        return false;
    }
    /**
     * 是否为绝对路径
     * @param path 路径
     */
    public static isAbsolutePath(path: string): boolean {
        if (JMSys.platform == JMPlatform.JSB) {
            return jsb.fileUtils.isAbsolutePath(path);
        }
        return false;
    }
    /**
     * 写入数据字符串
     * @param dataStr 字符串数据
     * @param fullPath 路径
     */
    public static writeStringToFile(dataStr: string, fullPath: string): boolean {
        if (JMSys.platform == JMPlatform.JSB) {
            return jsb.fileUtils.writeStringToFile(dataStr, fullPath);
        }
        return false;
    }
    /**
     * 写入数据二进制
     * @param data 二进制数据
     * @param fullPath 路径
     */
    public static writeDataToFile(data: Uint8Array, fullPath: string): boolean {
        if (JMSys.platform == JMPlatform.JSB) {
            return (jsb.fileUtils as any).writeDataToFile(data, fullPath);
        }
        return false;
    }
    /**
     * 目录是否存在
     * @param assetsPath 目录
     */
    public static isDirectoryExist(assetsPath: string): boolean {
        if (JMSys.platform == JMPlatform.JSB) {
            return jsb.fileUtils.isDirectoryExist(assetsPath);
        }
        return false;
    }
    /**
     * 创建目录
     */
    public static createDirectory(assetsPath: string): boolean {
        if (JMSys.platform == JMPlatform.JSB) {
            return (jsb.fileUtils as any).createDirectory(assetsPath);
        }
        return false;
    }
    /**
     * 删除一个目录
     * @param dirPath 
     */
    public static removeDirectory(dirPath: string): boolean {
        if (JMSys.platform == JMPlatform.JSB) {
            return jsb.fileUtils.removeDirectory(dirPath);
        }
        return false;
    }
    /**获取可写路径 */
    public static getWritablePath(): string {
        if (JMSys.platform == JMPlatform.JSB) {
            return jsb.fileUtils.getWritablePath();
        }
        return '';
    }
    /**获取根路径 */
    public static getDefaultResourceRootPath(): string {
        if (JMSys.platform == JMPlatform.JSB) {
            return jsb.fileUtils.getDefaultResourceRootPath();
        }
        return '';
    }
    /**删除一个文件 */
    public static removeFile(filepath: string): boolean {
        if (JMSys.platform == JMPlatform.JSB) {
            return jsb.fileUtils.removeFile(filepath);
        }
        return false;
    }
    /**重命名指定目录下的文件 */
    public static renameFile(path: string, oldname: string, name: string): boolean {
        if (JMSys.platform == JMPlatform.JSB) {
            return (jsb.fileUtils as any).renameFile(path, oldname, name);
        }
        return false;
    }
    /**添加搜索路径 */
    public static addSearchPath(path: string, front: boolean = false) {
        if (JMSys.platform == JMPlatform.JSB) {
            let array = this.getSearchPaths();
            if (array.indexOf(path) == -1) {
                jsb.fileUtils.addSearchPath(path, front);
            }
        }
    }
    /**设置搜索路径 */
    public static setSearchPaths(searchPaths: Array<string>) {
        if (JMSys.platform == JMPlatform.JSB) {
            jsb.fileUtils.setSearchPaths(searchPaths);
        }
    }
    /**获取搜索路径 */
    public static getSearchPaths(): Array<string> {
        if (JMSys.platform == JMPlatform.JSB) {
            return jsb.fileUtils.getSearchPaths();
        }
    }
}
