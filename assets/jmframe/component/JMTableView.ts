import jmToggleGroup from "./JMToggleGroup";
import jmToggle from "./JMToggle";
import jmTableCell from "./JMTableCell";
import { Enum, _decorator, ScrollView, Prefab, Widget, instantiate, NodePool, Node, Vec3, EventHandler, rect, UITransform, Mask, EventTouch, Vec2 } from "cc";


const { ccclass, property } = _decorator;

@ccclass('JMTableview')
export default class JMTableview extends ScrollView {
    /**
     * 滑动方向
     */
    public static VerticalOffsetDirection = Enum({
        Top: 0,
        Center: 1,
        Bottom: 2,
    });
    public static HorizontalOffsetDirection = Enum({
        Left: 0,
        Center: 1,
        Right: 2,
    });
    /**
     * 滑动方向
     */
    public static ScrollDirection = Enum({
        None: 0,
        Up: 1,
        Down: 2,
        Left: 3,
        Right: 4
    });
    /**
     * 滑动模式
     * Horizontal: 水平方向
     * Vertical: 垂直方向
     */
    public static ScrollModel = Enum({
        Horizontal: 0,
        Vertical: 1,
    });

    @property({
        type: Prefab,
        displayName: 'ViewCell预制',
    })
    cell = null;
    @property({
        type: JMTableview.ScrollModel,
        displayName: '滑动模式',
    })
    scrollModel = JMTableview.ScrollModel.Horizontal;
    @property({
        displayName: '启用网格模式',
    })
    enableGrid = true;
    @property({
        displayName: '启用居中模式',
    })
    enableCenter = false;
    @property({
        displayName: '启用触摸事件',
    })
    enableTouchEvent = false;
    @property({
        displayName: '开启适配内容项',
    })
    adapCell = false;
    @property({
        type: JMTableview.HorizontalOffsetDirection,
        displayName: '水平偏移方向',
    })
    horizontalOffsetDirection = JMTableview.HorizontalOffsetDirection.Left;
    @property({
        type: JMTableview.VerticalOffsetDirection,
        displayName: '垂直偏移方向',
    })
    verticalOffsetDirection = JMTableview.VerticalOffsetDirection.Top;
    // 元数据
    private _sourceData: any = null;
    // 标记, 是否初始化完成
    private _initialize: boolean = false;
    // 标记, 是否延迟初始化
    private _delayInitialize: boolean = false;
    // 标记, 是否已经完成了初始化
    private _completeInitialize: boolean = false;
    // ViewCell的单元池
    private _viewCellPool: NodePool = null;
    // 工作节点总数(实际参与工作的节点数量)
    private _workNodeCount: number = 0;
    // 数据节点总数(原则上要显示的节点数量)
    private _dataNodeCount: number = 0;
    // 可见节点总数(实际可以看见的节点数量)
    private _visualNodeCount: number = 0;
    // 节点, 触摸事件节点
    private _layTouch: Node = null;
    // 节点, 可见视图截取节点
    private _layView: Node = null;
    // cell起始下标
    private _minGroupCellIndex: number = null;
    // cell最大下标 
    private _maxGroupCellIndex: number = null;
    // content 偏移
    private _lastOffset: Vec2 = null;
    // 当前滚动方向
    private _scrollDirection: number = JMTableview.ScrollDirection.None;
    // 触摸事件
    //private _touchListener:any = null;
    //临时刷新一次
    private _refreshOnce: boolean = false;
    private _contentUI: UITransform = null;
    private _viewUI: UITransform = null

    init(data: Array<any>, checkedIndex: number = undefined) {
        // 数据源仅限数组类型
        if (!(data instanceof Array)) {
            jm.Log.error('异常: iTableView.init 传参需要Array类型.');
            return;
        }

        if (!this.content) {
            jm.Log.error('异常: iTableView.init 未设置Content容器节点.');
            return;
        }

        this._sourceData = data;

        // 设置iToggle的默认选中的索引
        if (checkedIndex != undefined) {
            this._triggerToggleChecked(checkedIndex);
        }

        if (!this._initialize) {
            this._layView = this.content.parent;
            this._viewUI = this._layView.getComponent(UITransform)
            if (this.enableTouchEvent) {
                this.initTouchLayer();
                this._registerEvents();
            }

            if (this.scrollModel === JMTableview.ScrollModel.Horizontal) {
                this.horizontal = true;
                this.vertical = false;
            }

            if (this.scrollModel === JMTableview.ScrollModel.Vertical) {
                this.horizontal = false;
                this.vertical = true;
            }

            // 带有Widget自适应组件的情况下, 延迟到下一帧获取最新的大小进行初始化
            if (this.node.getComponent(Widget) || this._layView.getComponent(Widget) || this.content.getComponent(Widget)) {
                this._delayInitialize = true;
                this.scheduleOnce(this._initTableView);
            } else {
                this._initTableView();
            }

            this._initialize = true;
        } else {
            if (this._completeInitialize) {
                this._refreshTableView();
            }
            else if (!this._delayInitialize) {
                this._initTableView();
            }
        }
    }
    //添加cell数据
    pushCellData(data: Array<any>) {
        if (!this._initialize) { jm.Log.warn('请先初始化'); return; }
        let list = [];
        if (data instanceof Array) {
            list = data;
        } else {
            list.push(data);
        }
        if (list.length == 0) return;
        this._sourceData = this._sourceData.concat(list);
        if ((this.scrollModel === JMTableview.ScrollModel.Horizontal && this._viewUI.width > this._contentUI.width)
            || this.scrollModel === JMTableview.ScrollModel.Vertical && this._viewUI.height > this._contentUI.height) {
            this.init(this._sourceData);
            return;
        }
        let groupCell = this._getGroupCell();
        let groupCellSize = groupCell.getComponent(UITransform).contentSize;
        this._dataNodeCount = Math.ceil(this._sourceData.length / groupCell.children.length);
        if (this.scrollModel === JMTableview.ScrollModel.Horizontal) {
            this._contentUI.width = this._dataNodeCount * groupCellSize.width;
        } else if (this.scrollModel === JMTableview.ScrollModel.Vertical) {
            this._contentUI.height = this._dataNodeCount * groupCellSize.height;
        }
    }
    clear() {
        for (var i = this.content.children.length - 1; i >= 0; --i) {
            this._viewCellPool.put(this.content.children[i]);
        }

        // 元数据
        this._sourceData = null;
        // 标记, 是否初始化完成
        this._initialize = false;
        // 标记, 是否延迟初始化
        this._delayInitialize = false;
        // 标记, 是否已经完成了初始化
        this._completeInitialize = false;

        // 工作节点总数(实际参与工作的节点数量)
        this._workNodeCount = 0;
        // 数据节点总数(原则上要显示的节点数量)
        this._dataNodeCount = 0;
        // 可见节点总数(实际可以看见的节点数量)
        this._visualNodeCount = 0;
    }

    onTouchBegan(ev) {
        if (this.scrollModel === JMTableview.ScrollModel.Horizontal) {
            this.horizontal = false;
        }

        if (this.scrollModel === JMTableview.ScrollModel.Vertical) {
            this.vertical = false;
        }
    }

    onTouchMoved(ev) {
        if (this.horizontal === this.vertical) {
            var startL = ev.getStartLocation();
            var l = ev.getLocation();
            if (this.scrollModel === JMTableview.ScrollModel.Horizontal) {
                if (Math.abs(l.x - startL.x) <= 7) {
                    return;
                }
                this.horizontal = true;
            }

            if (this.scrollModel === JMTableview.ScrollModel.Vertical) {
                if (Math.abs(l.y - startL.y) <= 7) {
                    return;
                }
                this.vertical = true;
            }
        }
    }

    onTouchEnded(ev) {
        if (this.scrollModel === JMTableview.ScrollModel.Horizontal) {
            this.horizontal = true;
        }

        if (this.scrollModel === JMTableview.ScrollModel.Vertical) {
            this.vertical = true;
        }
    }

    _registerEvents() {
        // 监听窗口大小改变事件, 给ScrollBar重定位
        if (this.verticalScrollBar) {
            this.verticalScrollBar.node.on('size-changed', this._onUpdateScrollBar, this);
        }
        if (this.horizontalScrollBar) {
            this.horizontalScrollBar.node.on('size-changed', this._onUpdateScrollBar, this);
        }

        // 禁止iTableView点击事件向父级传递
        this.node.on(Node.EventType.TOUCH_START, this._onStopPropagation);
        this.node.on(Node.EventType.TOUCH_MOVE, this._onStopPropagation);
        this.node.on(Node.EventType.TOUCH_END, this._onStopPropagation);
        this.node.on(Node.EventType.TOUCH_CANCEL, this._onStopPropagation);
    }

    _onUpdateScrollBar() {
        let p = this['_getHowMuchOutOfBoundary']();
        this['_updateScrollBar'](new Vec2(p.x, p.y));
    }

    _onStopPropagation(ev: TouchEvent) {
        ev.stopPropagation()
    }

    _initTableView() {
        this._delayInitialize = false;
        this._contentUI = this.content.getComponent(UITransform)
        var isFill = false; // 是否填满
        var groupCell = this._getGroupCell();
        var groupCellSize = groupCell.getComponent(UITransform).contentSize;

        this._dataNodeCount = Math.ceil(this._sourceData.length / groupCell.children.length);

        if (this.scrollModel === JMTableview.ScrollModel.Horizontal) {
            this._contentUI.width = this._dataNodeCount * groupCellSize.width;

            this._workNodeCount = Math.ceil(this._viewUI.width / groupCellSize.width) + 1;
            if (this._workNodeCount > this._dataNodeCount) {
                this._workNodeCount = this._dataNodeCount;
                this._visualNodeCount = this._workNodeCount;
            } else {
                this._visualNodeCount = this._workNodeCount - 1;
            }

            isFill = this._viewUI.width < this._contentUI.width;

            this.stopAutoScroll();
            this.scrollToLeft();

            // 设置偏移方向
            var offsetValue;
            if (!this.enableGrid) {
                offsetValue = this._viewUI.height - groupCellSize.height;
            } else {
                offsetValue = this._viewUI.height % groupCellSize.height;
            }
            switch (this.verticalOffsetDirection) {
                case JMTableview.VerticalOffsetDirection.Top:
                    this.content.setPosition(this.content.getPosition().x, 0, this.content.getPosition().z);
                    break;
                case JMTableview.VerticalOffsetDirection.Bottom:
                    this.content.setPosition(this.content.getPosition().x, -offsetValue, this.content.getPosition().z);
                    break;
                case JMTableview.VerticalOffsetDirection.Center:
                    this.content.setPosition(this.content.getPosition().x, -offsetValue / 2, this.content.getPosition().z);
                    break;
            }

            // 可见内容不足以填满, 根据设定启用居中显示策略
            if (!isFill && this.enableCenter) {
                this.content.setPosition(0, this.content.getPosition().y, this.content.getPosition().z);
            }
        }

        if (this.scrollModel === JMTableview.ScrollModel.Vertical) {
            this._contentUI.height = this._dataNodeCount * groupCellSize.height;

            this._workNodeCount = Math.ceil(this._viewUI.height / groupCellSize.height) + 1;
            if (this._workNodeCount > this._dataNodeCount) {
                this._workNodeCount = this._dataNodeCount;
                this._visualNodeCount = this._workNodeCount;
            } else {
                this._visualNodeCount = this._workNodeCount - 1;
            }

            isFill = this._viewUI.height < this._contentUI.height;

            this.stopAutoScroll();
            this.scrollToTop();

            // 设置偏移方向
            let offsetValue = (this._viewUI.width % groupCellSize.width) / 2;
            if (!this.enableGrid) {
                offsetValue += (Math.floor(this._viewUI.width / groupCellSize.width) - 1) * groupCellSize.width / 2;
            }
            switch (this.horizontalOffsetDirection) {
                case JMTableview.HorizontalOffsetDirection.Left:
                    this.content.setPosition(0, this.content.getPosition().y, this.content.getPosition().z);
                    break;
                case JMTableview.HorizontalOffsetDirection.Right:
                    this.content.setPosition(this._viewUI.width - groupCellSize.width, this.content.getPosition().y, this.content.getPosition().z);
                    break;
                case JMTableview.HorizontalOffsetDirection.Center:
                    this.content.setPosition(offsetValue, this.content.getPosition().y, this.content.getPosition().z);
                    break;
            }

            // 可见内容不足以填满, 根据设定启用居中显示策略
            if (!isFill && this.enableCenter) {
                this.content.setPosition(this.content.getPosition().x, 0, this.content.getPosition().z);
            }
        }

        this._minGroupCellIndex = 0;
        this._maxGroupCellIndex = this._workNodeCount - 1;
        this._lastOffset = this.getScrollOffset();

        for (var i = this.content.children.length; i <= this._maxGroupCellIndex; ++i) {
            var _groupCell = this._getGroupCell();
            this._setGroupCellAttr(_groupCell, i);
            this._setGroupCellPosition(_groupCell, i);
            this.content.addChild(_groupCell);
            this._initGroupCell(_groupCell);
        }

        this._completeInitialize = true;
    }

    _refreshTableView() {
        let isFill = false; // 是否填满
        let groupCell = this._getGroupCell();
        let groupCellSize = groupCell.getComponent(UITransform).contentSize;

        this._dataNodeCount = Math.ceil(this._sourceData.length / groupCell.children.length);

        if (this.scrollModel === JMTableview.ScrollModel.Horizontal) {
            this._contentUI.width = this._dataNodeCount * groupCellSize.width;

            this._workNodeCount = Math.ceil(this._viewUI.width / groupCellSize.width) + 1;
            if (this._workNodeCount > this._dataNodeCount) {
                this._workNodeCount = this._dataNodeCount;
                this._visualNodeCount = this._workNodeCount;
            } else {
                this._visualNodeCount = this._workNodeCount - 1;
            }

            isFill = this._viewUI.width < this._contentUI.width;

            this.stopAutoScroll();
            this.scrollToLeft();

            // 设置偏移方向
            var offsetValue;
            if (!this.enableGrid) {
                offsetValue = this._viewUI.height - groupCellSize.height;
            } else {
                offsetValue = this._viewUI.height % groupCellSize.height;
            }
            switch (this.verticalOffsetDirection) {
                case JMTableview.VerticalOffsetDirection.Top:
                    this.content.setPosition(this.content.getPosition().x, 0, this.content.getPosition().z);
                    break;
                case JMTableview.VerticalOffsetDirection.Bottom:
                    this.content.setPosition(this.content.getPosition().x, -offsetValue, this.content.getPosition().z);
                    break;
                case JMTableview.VerticalOffsetDirection.Center:
                    this.content.setPosition(this.content.getPosition().x, -offsetValue / 2, this.content.getPosition().z);
                    break;
            }

            // 可见内容不足以填满, 根据设定启用居中显示策略
            if (!isFill && this.enableCenter) {
                this.content.setPosition(0, this.content.getPosition().y, this.content.getPosition().z);
            }
        }

        if (this.scrollModel === JMTableview.ScrollModel.Vertical) {
            this._contentUI.height = this._dataNodeCount * groupCellSize.height;

            this._workNodeCount = Math.ceil(this._viewUI.height / groupCellSize.height) + 1;
            if (this._workNodeCount > this._dataNodeCount) {
                this._workNodeCount = this._dataNodeCount;
                this._visualNodeCount = this._workNodeCount;
            } else {
                this._visualNodeCount = this._workNodeCount - 1;
            }

            isFill = this._viewUI.height < this._contentUI.height;

            this.stopAutoScroll();
            this.scrollToTop();

            // 设置偏移方向
            let offsetValue = (this._viewUI.width % groupCellSize.width) / 2;
            if (!this.enableGrid) {
                offsetValue += (Math.floor(this._viewUI.width / groupCellSize.width) - 1) * groupCellSize.width / 2;
            }
            switch (this.horizontalOffsetDirection) {
                case JMTableview.HorizontalOffsetDirection.Left:
                    this.content.setPosition(0, this.content.getPosition().y, this.content.getPosition().z);
                    break;
                case JMTableview.HorizontalOffsetDirection.Right:
                    this.content.setPosition(this._viewUI.width - groupCellSize.width, this.content.getPosition().y, this.content.getPosition().z);
                    break;
                case JMTableview.HorizontalOffsetDirection.Center:
                    this.content.setPosition(offsetValue, this.content.getPosition().y, this.content.getPosition().z);
                    break;
            }

            // 可见内容不足以填满, 根据设定启用居中显示策略
            if (!isFill && this.enableCenter) {
                this.content.setPosition(this.content.getPosition().x, 0, this.content.getPosition().z);
            }
        }

        this._minGroupCellIndex = 0;
        this._maxGroupCellIndex = this._workNodeCount - 1;
        this._lastOffset = this.getScrollOffset();

        for (let i = this.content.children.length; i <= this._maxGroupCellIndex; ++i) {
            var _groupCell = this._getGroupCell();
            this.content.addChild(_groupCell);
        }

        for (let i = 0; i < this.content.children.length; i++) {
            let _groupCell = this.content.children[i];
            _groupCell.setSiblingIndex(i);
            _groupCell.name = i.toString();
        }

        for (var i = 0; i < this.content.children.length; i++) {
            let _groupCell = this.content.children[i];
            this._setGroupCellPosition(_groupCell, i);
            this._initGroupCell(_groupCell);
        }
    }

    _triggerToggleChecked(checkedIndex) {
        var group = this.content.getComponent(jmToggleGroup);

        if (group) {
            var fn = function () {
                if (group._toggles.length > 0) {
                    this.unschedule(fn);
                } else {
                    return;
                }
                group.setCheckedOnlyFlag(checkedIndex);
                var toggle = group._toggles[0];
                var data = this._sourceData[checkedIndex];
                EventHandler.emitEvents(toggle.checkedEvents, null, data);
            }.bind(this);
            if (this._sourceData[checkedIndex]) {
                this.schedule(fn, 0.1);
            }
        } else {
            jm.Log.warn('警告: iTableView下的content必须带有iToggleGroup组件, 否则默认选中索引不会生效.');
        }
    }

    initTouchLayer() {
        /*
        var layTouch = new Node('lay_touch');
        this._layTouch = layTouch;
        this._layView.addChild(layTouch);

        var widget = layTouch.addComponent(cc.Widget);
        widget.isAlignTop = true;
        widget.isAlignBottom = true;
        widget.isAlignLeft = true;
        widget.isAlignRight = true;
        widget.top = widget.bottom = 0;
        widget.left = widget.right = 0;
        widget.alignMode = Widget.AlignMode.ALWAYS;

        var self = this;
        this._touchListener = EventListener.create({
            event: EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: false,
            ower: layTouch,
            mask: this._findMaskParent(layTouch),
            onTouchBegan: function(touch, ev) {
                var pos = touch.getLocation();
                var node = this.ower;

                if (node._hitTest(pos, this)) {
                    self.onTouchBegan(touch);
                    return true;
                }
                return false;
            },
            onTouchMoved: function(touch, ev) {
                self.onTouchMoved(touch);
            },
            onTouchEnded: function(touch, ev) {
                self.onTouchEnded(touch);
            },
        });

        if (cc.sys.isNative) {
            this._touchListener.retain();
        }
        // 加入到事件管理器中
        eventManager.addListener(this._touchListener, layTouch);
        */
    }

    _findMaskParent(node: Node) {
        var i = 0,
            curr = node;

        while (curr && curr instanceof Node) {
            i++;
            curr = curr.parent;

            if (curr.getComponent(Mask)) {
                return {
                    index: i,
                    node: curr
                }
            }
        }
        return null;
    }

    _getGroupCell(): Node {
        if (this._viewCellPool == null) {
            this._viewCellPool = new NodePool();
        }

        if (this._viewCellPool.size() == 0) {
            // 容量大小(累计)
            var capacity = 0;
            // 单元大小
            var cellSize = (instantiate(this.cell) as Node).getComponent(UITransform).contentSize;

            var nodeGroup = new Node()
            nodeGroup.addComponent(UITransform)

            if (this.scrollModel === JMTableview.ScrollModel.Horizontal) {
                nodeGroup.getComponent(UITransform).width = cellSize.width;
                var screenHeight = this._viewUI.height;
                var len = this.enableGrid ? Math.floor(screenHeight / cellSize.height) : 1;
                let itemHeight;
                if (this.adapCell) {
                    itemHeight = screenHeight / len;
                } else {
                    itemHeight = cellSize.height;
                }
                for (var i = 0; i < len; i++) {
                    var nodeCell = instantiate(this.cell) as Node;
                    let ui = nodeCell.getComponent(UITransform)
                    nodeCell.setPosition((ui.anchorX - 0.5) * ui.width, (screenHeight / 2) - itemHeight * (1 - ui.anchorY) - capacity, nodeCell.getPosition().z)
                    nodeGroup.addChild(nodeCell);
                    capacity += itemHeight;
                }
                if (this.adapCell) {
                    nodeGroup.getComponent(UITransform).height = screenHeight;
                } else {
                    nodeGroup.getComponent(UITransform).height = capacity;
                }
            }

            if (this.scrollModel === JMTableview.ScrollModel.Vertical) {
                nodeGroup.getComponent(UITransform).height = cellSize.height;
                var screenWidth = this._viewUI.width;
                var len = this.enableGrid ? Math.floor(screenWidth / cellSize.width) : 1;
                let itemWidth;
                if (this.adapCell) {
                    itemWidth = screenWidth / len;
                } else {
                    itemWidth = cellSize.width;
                }
                for (var i = 0; i < len; ++i) {
                    var nodeCell = instantiate(this.cell) as Node;
                    let ui = nodeCell.getComponent(UITransform)
                    nodeCell.setPosition(-(screenWidth / 2 - itemWidth * ui.anchorX) + capacity, (ui.anchorY - 0.5) * ui.height, nodeCell.getPosition().z)
                    nodeGroup.addChild(nodeCell);
                    capacity += itemWidth;
                }
                if (this.adapCell) {
                    nodeGroup.getComponent(UITransform).width = screenWidth;
                } else {
                    nodeGroup.getComponent(UITransform).width = capacity;
                }
            }

            this._viewCellPool.put(nodeGroup);
        }

        return this._viewCellPool.get();
    }

    _initGroupCell(groupCell: Node) {
        var count = groupCell.children.length;
        var tag = Number(groupCell.name) * count;

        for (var i = 0; i < count; ++i) {
            var idx = tag + i;
            var node = groupCell.children[i];

            if (idx >= this._sourceData.length) {
                node.active = false;
            } else {
                node.active = true;

                var fn = function (n: Node) {
                    var t = n.getComponent(jmToggle);
                    if (t) {
                        t.setOnlyFlag(idx);
                        var g = this.content.getComponent(jmToggleGroup);
                        if (g) {
                            t.setStatus(idx == g._checkedOnlyFlag);
                        }
                    } else {
                        // for (var x = 0; x < n.children.length; x++) {
                        //     fn(n.children[x]);
                        // }
                    }
                }.bind(this);
                var viewCell = node.getComponent(jmTableCell);
                viewCell && viewCell.updateView(idx, this._sourceData[idx]);
                fn(node);
            }
        }
    }

    _setGroupCellAttr(groupCell: Node, idx: number) {
        groupCell.setSiblingIndex(idx);
        groupCell.name = idx.toString();
    }

    _setGroupCellPosition(groupCell: Node, idx: number) {
        let cellUI = groupCell.getComponent(UITransform)
        if (this.scrollModel === JMTableview.ScrollModel.Horizontal) {
            var _posV3 = groupCell.getPosition()
            if (idx == 0) {
                _posV3.x = -this._contentUI.width * this._contentUI.anchorX + cellUI.width * cellUI.anchorX;
            } else {
                _posV3.x = this.content.getChildByName((idx - 1).toString()).getPosition().x + cellUI.width;
            }
            _posV3.y = (cellUI.anchorY - this._contentUI.anchorY) * cellUI.height;
            groupCell.setPosition(_posV3)
        }
        else if (this.scrollModel === JMTableview.ScrollModel.Vertical) {
            var _posV3 = groupCell.getPosition()
            if (idx == 0) {
                _posV3.y = this._contentUI.height * (1 - this._contentUI.anchorY) - cellUI.height * (1 - cellUI.anchorY);
            } else {
                _posV3.y = this.content.getChildByName((idx - 1).toString()).getPosition().y - cellUI.height;
            }
            _posV3.x = (cellUI.anchorX - this._contentUI.anchorX) * cellUI.width;
            groupCell.setPosition(_posV3)
        }
    }

    // ############# ScrollView相关 #############

    stopAutoScroll() {
        if (!this._delayInitialize) {
            this._scrollDirection = JMTableview.ScrollDirection.None;
            super.stopAutoScroll();
        } else {
            this.scheduleOnce(this.stopAutoScroll);
        }
    }

    scrollToBottom(timeInSecond?: number, attenuated?: boolean) {
        if (!this._delayInitialize) {
            this._scrollDirection = JMTableview.ScrollDirection.Up;
            super.scrollToBottom(timeInSecond, attenuated);
        } else {
            this.scheduleOnce(function () {
                this.scrollToBottom(timeInSecond, attenuated);
            });
        }
    }

    scrollToTop(timeInSecond?: number, attenuated?: boolean) {
        if (!this._delayInitialize) {
            this._scrollDirection = JMTableview.ScrollDirection.Down;
            super.scrollToTop(timeInSecond, attenuated);
        } else {
            this.scheduleOnce(function () {
                this.scrollToTop(timeInSecond, attenuated);
            });
        }
    }

    // ############# iViewCell相关 #############

    // ############# 单元显示相关 #############

    _updateScrollDirection() {
        var lastOffset = this._lastOffset;
        var currOffset = this.getScrollOffset();

        this._scrollDirection = JMTableview.ScrollDirection.None;

        if (this.scrollModel === JMTableview.ScrollModel.Horizontal) {
            let offX = currOffset.x - lastOffset.x
            if (offX > 0) {
                this._scrollDirection = JMTableview.ScrollDirection.Right;
            }
            else if (offX < 0) {
                this._scrollDirection = JMTableview.ScrollDirection.Left;
            }
        }

        if (this.scrollModel === JMTableview.ScrollModel.Vertical) {
            let offY = currOffset.y - lastOffset.y
            if (offY < 0) {
                this._scrollDirection = JMTableview.ScrollDirection.Down;
            }
            else if (offY > 0) {
                this._scrollDirection = JMTableview.ScrollDirection.Up;
            }
        }
        this._lastOffset = currOffset;
    }

    _updateViewCellRender() {
        if (this.scrollModel === JMTableview.ScrollModel.Horizontal) {

            if (this._scrollDirection === JMTableview.ScrollDirection.Left) {
                if (this._maxGroupCellIndex < this._dataNodeCount - 1) {
                    var viewBox = this._getBoundingBoxToWorld(this._layView);
                    do {
                        var node = this.content.getChildByName(this._minGroupCellIndex.toString());
                        var nodeBox = this._getBoundingBoxToWorld(node);
                        if (nodeBox.xMax <= (viewBox.xMin - this._viewUI.width / 2)) {
                            node.setPosition(this.content.getChildByName(this._maxGroupCellIndex.toString()).getPosition().x + node.getComponent(UITransform).width, node.getPosition().y, node.getPosition().z)
                            this._minGroupCellIndex++;
                            this._maxGroupCellIndex++;
                            this._setGroupCellAttr(node, this._maxGroupCellIndex);
                            this._initGroupCell(node);
                        } else {
                            break;
                        }
                    } while (this._maxGroupCellIndex !== this._dataNodeCount - 1);
                }
            }

            if (this._scrollDirection === JMTableview.ScrollDirection.Right) {
                if (this._minGroupCellIndex > 0) {
                    var viewBox = this._getBoundingBoxToWorld(this._layView);
                    do {
                        var node = this.content.getChildByName(this._maxGroupCellIndex.toString());
                        var nodeBox = this._getBoundingBoxToWorld(node);
                        if (nodeBox.xMin >= (viewBox.xMax - this._viewUI.width / 2)) {
                            node.setPosition(this.content.getChildByName(this._minGroupCellIndex.toString()).getPosition().x - node.getComponent(UITransform).width, node.getPosition().y, node.getPosition().z)
                            this._minGroupCellIndex--;
                            this._maxGroupCellIndex--;
                            this._setGroupCellAttr(node, this._minGroupCellIndex);
                            this._initGroupCell(node);
                        } else {
                            break;
                        }
                    } while (this._minGroupCellIndex !== 0);
                }
            }

        }

        if (this.scrollModel === JMTableview.ScrollModel.Vertical) {

            if (this._scrollDirection === JMTableview.ScrollDirection.Up) {
                if (this._maxGroupCellIndex < this._dataNodeCount - 1) {
                    var viewBox = this._getBoundingBoxToWorld(this._layView);
                    do {
                        var node = this.content.getChildByName(this._minGroupCellIndex.toString());
                        var nodeBox = this._getBoundingBoxToWorld(node);
                        if (nodeBox.yMin >= (viewBox.yMax - this._viewUI.height / 2)) {
                            node.setPosition(node.getPosition().x, this.content.getChildByName(this._maxGroupCellIndex.toString()).getPosition().y - node.getComponent(UITransform).height, node.getPosition().z)
                            this._minGroupCellIndex++;
                            this._maxGroupCellIndex++;
                            this._setGroupCellAttr(node, this._maxGroupCellIndex);
                            this._initGroupCell(node);
                        } else {
                            break;
                        }
                    } while (this._maxGroupCellIndex !== this._dataNodeCount - 1);
                }
            }

            if (this._scrollDirection === JMTableview.ScrollDirection.Down) {
                if (this._minGroupCellIndex > 0) {
                    var viewBox = this._getBoundingBoxToWorld(this._layView);
                    do {
                        var node = this.content.getChildByName(this._maxGroupCellIndex.toString());
                        var nodeBox = this._getBoundingBoxToWorld(node);
                        if (nodeBox.yMax <= viewBox.yMin - this._viewUI.height / 2) {
                            node.setPosition(node.getPosition().x, this.content.getChildByName(this._minGroupCellIndex.toString()).getPosition().y + node.getComponent(UITransform).height, node.getPosition().z)
                            this._minGroupCellIndex--;
                            this._maxGroupCellIndex--;
                            this._setGroupCellAttr(node, this._minGroupCellIndex);
                            this._initGroupCell(node);
                        } else {
                            break;
                        }
                    } while (this._minGroupCellIndex !== 0);
                }
            }

        }
    }

    _getBoundingBoxToWorld(node: Node) {
        let pos = node.getWorldPosition();
        return rect(pos.x, pos.y, node.getComponent(UITransform).width, node.getComponent(UITransform).height);
    }

    update(dt: number) {
        super.update(dt);

        if (this._refreshOnce) {
            // 临时刷新一次
            this._refreshOnce = false;
        }
        else if (!this._completeInitialize || this._workNodeCount === this._visualNodeCount) {
            return;
        }
        this._updateScrollDirection();
        this._updateViewCellRender();
    }
}
