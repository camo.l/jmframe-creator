import { Component, _decorator, EventHandler, Node, CCObject } from "cc";
import JMToggle from "./JMToggle";

const { ccclass, property, menu, inspector } = _decorator;

@ccclass
@menu('JM Frame/ToggleGroup')
@inspector('packages://jmframe/toggleGroup/inspector.js')
export default class JMToggleGroup extends Component {
    @property({
        displayName: '允许多选',
    })
    allowMultiple = false;
    private _checkedOnlyFlag: number = null;
    public _toggles: Array<JMToggle> = [];

    onDestroy() {
        this._toggles = [];
    }

    joinToggle(toggle: JMToggle) {
        if (!(toggle instanceof JMToggle)) {
            return jm.Log.warn('警告: 不是一个有效的jmToggle组件.');
        }

        // var eventHandler = new Component.EventHandler();
        // eventHandler.target = this.node;
        // eventHandler.component = 'jmToggleGroup';
        // eventHandler.handler = 'onToggleChange';
        // toggle.checkedEvents.push(eventHandler);
        toggle.parent = this;
        this._toggles.push(toggle);

        // iTableView环境下
        if (this._checkedOnlyFlag != null) {
            toggle.setStatus(toggle.onlyFlag == this._checkedOnlyFlag);
        } else {
            if (toggle.checked) {
                if (window['CC_EDITOR']) return;
                for (var i = 0; i < toggle.checkedEvents.length; i++) {
                    var handler = toggle.checkedEvents[i];
                    EventHandler.emitEvents([handler], {
                        target: toggle.node
                    }, handler.customEventData);
                }
            }
        }
    }

    setCheckedOnlyFlag(onlyFlag: number) {
        var toggles = this._toggles;
        for (var i = 0; i < toggles.length; i++) {
            var toggle = toggles[i];
            if (toggle && toggle.node.active) {
                toggle.setStatus(toggle.onlyFlag == onlyFlag);
            }
        }
        this._checkedOnlyFlag = onlyFlag;
    }

    uncheckAllToggle() {
        if (this._checkedOnlyFlag != null) {
            this._checkedOnlyFlag = null;
        }

        var toggles = this._toggles;
        for (var i = 0; i < toggles.length; i++) {
            var toggle = toggles[i];
            if (toggle && toggle.node.active) {
                toggle.setStatus(false);
            }
        }
    }

    onToggleChange(ev: Event) {
        if (!ev || !ev.target) {
            return;
        }

        var sender = ev.target as any as Node;
        var toggles = this._toggles;
        var toggle = sender.getComponent(JMToggle);

        // 普通结构
        if (toggle.onlyFlag == null) {
            for (var i = 0; i < toggles.length; i++) {
                var _toggle = toggles[i];
                if (_toggle && _toggle.node.active) {
                    if (_toggle.node['_id'] !== sender['_id']) {
                        if (!this.allowMultiple) {
                            _toggle.setStatus(false);
                        }
                    }
                }
            }
        }
        // iTableView结构
        else {
            for (var i = 0; i < toggles.length; i++) {
                var _toggle = toggles[i];
                if (_toggle && _toggle.node.active) {
                    if (_toggle.onlyFlag !== toggle.onlyFlag) {
                        if (!this.allowMultiple) {
                            _toggle.setStatus(false);
                        }
                    }
                }
            }
            this._checkedOnlyFlag = toggle.onlyFlag;
        }
    }
}
