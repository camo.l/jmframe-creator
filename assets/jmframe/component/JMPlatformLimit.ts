import { _decorator, Component } from 'cc';
import { JMPlatform, JMSys } from '../JMSys';
const { ccclass, property } = _decorator;
/**节点平台限制 */
@ccclass('JMPlatformLimit')
export class JMPlatformLimit extends Component {
    @property
    JSB: boolean = true;
    @property
    WEB: boolean = true;
    @property
    WXWEB: boolean = true;
    @property
    WXGAME: boolean = true;
    @property
    XQWEB: boolean = true;

    onLoad() {
        this.node.active = this.isSupport();
    }
    isSupport() {
        switch (JMSys.platform) {
            case JMPlatform.JSB: return this.JSB;
            case JMPlatform.WEB: return this.WEB;
            case JMPlatform.WXWEB: return this.WXWEB;
            case JMPlatform.WXGAME: return this.WXGAME;
            default:
                return false;
        }
    }
}
