import { _decorator } from "cc";
import JMBaseNode from "../base/JMBaseNode";

const { ccclass, property } = _decorator;

@ccclass('JMTableCell')
export default class JMTableCell extends JMBaseNode {
  protected data: any = null;

  updateView(idx: number, data: any) {
    this.data = data;
    // todo ...
  }
}
