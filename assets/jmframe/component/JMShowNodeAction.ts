import { _decorator, Component, Node, Enum, v3, tween, Tween, UITransform, Widget, SystemEventType } from 'cc';
import JMUIUtil from '../utils/JMUIUtil';
const { ccclass, property } = _decorator;
/**显示节点的动画组件 */
const nodeActionType = Enum({
    none: -1,
    //弹框效果
    pop_up: 1,
    /**向左移动显示 */
    move_left: 2,
    /**向右移动显示 */
    move_right: 3,
})
@ccclass('JMShowNodeAction')
export class JMShowNodeAction extends Component {
    @property({
        type: nodeActionType,
    })
    enum_actionType = nodeActionType.none;

    /**还原方法 */
    private _restoreFunc: () => void = null;
    private _tween: Tween<Node> = null;
    onEnable() {
        let widget = this.getComponent(Widget);
        if (widget) {
            this.node.once(SystemEventType.TRANSFORM_CHANGED, this.runAction, this);
            widget.destroy();
        } else {
            this.runAction();
        }
    }
    private runAction() {
        let _actionEnd = () => {
            this._restoreFunc = null;
            this._tween = null;
        }
        this.stopAction();
        switch (this.enum_actionType) {
            case nodeActionType.pop_up:
                this._restoreFunc = () => {
                    this.node.setScale(v3(1, 1, 1));
                }
                this.node.setScale(v3(0.8, 0.8, 1));
                this._tween = tween(this.node)
                    .to(0.2, { scale: v3(1, 1, 1) }, { easing: 'backOut' })
                    .call(_actionEnd)
                    .start()
                break;
            case nodeActionType.move_left:
                var size = JMUIUtil.getViewSize();
                var spos = this.node.getPosition().clone();
                this._restoreFunc = () => {
                    this.node.position = spos;
                }
                this.node.position = v3((size.width + this.node.getComponent(UITransform).width) / 2, spos.y, spos.z);
                this._tween = tween(this.node)
                    .to(0.2, { position: spos })
                    .call(_actionEnd)
                    .start()
                break;
            case nodeActionType.move_right:
                var size = JMUIUtil.getViewSize();
                var spos = this.node.getPosition().clone();
                this._restoreFunc = () => {
                    this.node.position = spos;
                }
                this.node.position = v3(-(size.width + this.node.getComponent(UITransform).width) / 2, spos.y, spos.z);
                this._tween = tween(this.node)
                    .to(0.2, { position: spos })
                    .call(_actionEnd)
                    .start()
                break;
        }
    }
    private stopAction() {
        /**动画没执行完的情况下回复坐标 */
        this._restoreFunc && this._restoreFunc();
        delete this._restoreFunc;
        this._tween && this._tween.stop();
        delete this._tween;
    }
    onDisable() {
        this.stopAction();
    }
}
