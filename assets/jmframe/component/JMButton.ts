import JMButtonSound from "./JMButtonSound";

import { _decorator, Button, EventHandler, EventTouch, Color } from "cc";

/**
 * 自定义button组件 带默认点击音效 
 */
const { ccclass, property, executeInEditMode, menu, help, inspector } = _decorator;

@ccclass
@executeInEditMode
@menu('JM Frame/Button')
@help('i18n:COMPONENT.help_url.button')
@inspector('packages://jmframe/button/inspector.js')
export default class JMButton extends Button {
    @property({
        displayName: "音效路径",
        tooltip: `resources:jmframe/sound/ui_click
        分包名:资源名
        `,
        type: '',
        multiline: true,
        formerlySerializedAs: '_N$string'
    })
    audioUrl = 'resources:jmframe/sound/ui_click';

    @property({ override: true })
    protected _disabledColor: Color = new Color(255, 255, 255, 255);
    @property({ override: true })
    protected _normalColor: Color = new Color(255, 255, 255, 255);
    @property({ override: true })
    protected _hoverColor: Color = new Color(255, 255, 255, 255);
    @property({ override: true })
    protected _pressColor: Color = new Color(211, 211, 211, 255);
    @property({ override: true })
    protected _transition = Button.Transition.COLOR;

    // 长按触发
    @property({ displayName: "是否开启长按事件" })
    openLongPress = false;

    // 触发时间
    @property({ displayName: "长按时间" })
    longPressTime = 1;
    longPressFlag = false;

    private longPressTimer = null;
    onEnable() {
        super.onEnable();
    }
    onDisable() {
        if (this.longPressTimer) {
            clearTimeout(this.longPressTimer);
            this.longPressTimer = null;
        }
        super.onDisable();
    }

    /** 重写 */
    _onTouchBegan(event: EventTouch) {
        if (!this.interactable || !this.enabledInHierarchy) return;

        if (this.openLongPress && !this.longPressFlag) {    // 开启长按
            if (this.longPressTimer) clearTimeout(this.longPressTimer);
            this.longPressTimer = setTimeout(function () {
                // 还在触摸中 触发事件
                if (this["_pressed"]) {
                    this.node.emit('longclickStart', this);
                    this.longPressFlag = true;
                }
            }.bind(this), this.longPressTime * 1000);
        }

        this["_pressed"] = true;
        this["_updateState"]();
        if (event) {
            event.propagationStopped = true;
        }
    }
    _onTouchEnded(event: EventTouch) {
        if (!this.interactable || !this.enabledInHierarchy) return;
        if (this["_pressed"] && this.longPressFlag) {
            this.node.emit('longclickEnd', this);
            this.longPressFlag = false;
        } else if (this["_pressed"]) {
            EventHandler.emitEvents(this.clickEvents, event);
            this.node.emit(JMButton.EventType.CLICK, this);
            let _btnSound = this.getComponent(JMButtonSound);
            if (_btnSound) {
                this.audioUrl = _btnSound.getAudioUrl();
            }
            if (this.audioUrl != '') {
                // xdSoundManager.instance().playEffect({ path: this.audioUrl });
            }
        }
        this["_pressed"] = false;
        this["_updateState"]();
        if (event) {
            event.propagationStopped = true;
        }
    }
    _onTouchCancel() {
        if (!this.interactable || !this.enabledInHierarchy) return;
        if (this["_pressed"] && this.longPressFlag) {
            this.node.emit('longclickEnd', this);
            this.longPressFlag = false;
        }
        this["_pressed"] = false;
        this["_updateState"]();
    }
    /** 添加一个长按事件 */
    addLongClick(startFunc: Function, endFunc: Function, target: Object) {
        this.node.off('longclickStart');
        this.node.off('longclickEnd');
        this.node.on('longclickStart', startFunc, target);
        this.node.on('longclickEnd', endFunc, target);
    }
}