/**相机适配 */

import { _decorator, Camera, Component, view, renderer, v3, size } from "cc";

const { ccclass, menu, requireComponent } = _decorator;

@ccclass
@requireComponent(Camera)
@menu('JM Frame/FitWidthCamera')
export class JMFitWidthCamera extends Component {
    private _camera: Camera;
    onLoad() {
        this._camera = this.getComponent(Camera)!;
        this.updateFov();
    }
    updateFov() {
        var dsize = size(1350, 720);
        var csize = view.getCanvasSize();
        if (this._camera.projection == renderer.scene.CameraProjection.PERSPECTIVE) {
            var dc = csize.width / csize.height;
            var dd = dsize.width / dsize.height;
            if (dc < dd) {
                let _defaultTanHalfFov = Math.tan(this._camera.fov * 0.5 / 180 * Math.PI);
                let tanHalfFov2 = view.getVisibleSize().height / dsize.height * _defaultTanHalfFov;
                this._camera.fov = Math.atan(tanHalfFov2) / Math.PI * 180 * 2;
            }
        } else {
            var dc = csize.width / csize.height;
            var dd = dsize.width / dsize.height;
            if (dc < dd) {
                let bl = (1 + dd - dc)
                this._camera.orthoHeight *= bl;
                let defp = this._camera.node.position;
                this._camera.node.position = v3(defp.x * bl, defp.y * bl, defp.z * bl);
            }
        }
    }
}