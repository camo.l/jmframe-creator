import { _decorator, Enum, Vec3, UITransform } from "cc";
import JMBaseNode from "../base/JMBaseNode";
import JMUIUtil from "../utils/JMUIUtil";

export const JMAdapNodeType = Enum({
    BgSize: 1,//背景大小适配
    BgScale: 2,//背景大小适配
    AllScreen: 3,//全面屏适配（适配到刘海区域）
    BangScreen: 4,//刘海屏适配（不适配到刘海区域）
    NodeAdapAll: 5,//节点全面屏适配（按照当前节点size等比放大缩小至设计分辨率，且长宽长度等比适配）
    NodeAdapBang: 6,//节点刘海屏适配（按照当前节点size等比放大缩小至设计分辨率，且长宽长度等比适配）
})
const { ccclass, property, menu } = _decorator;

@ccclass
@menu('JM Frame/AdapNode')
export default class JMAdapNode extends JMBaseNode {
    private static bangLength: number = 100;//齐刘海占用大小

    @property({
        type: JMAdapNodeType,
        displayName: '适配类型',
        tooltip: `BgSize 背景大小适配
        BgScale 背景大小适配
        AllScreen 全面屏适配
        BangScreen 刘海屏适配
        NodeAdapAll 节点全面屏适配
        NodeAdapBang 节点刘海屏适配
        `,
    })
    public adapType = JMAdapNodeType.BangScreen;

    onLoad() {
        super.onLoad();
        this.onAdap();
    }
    /**
     * 执行适配
     */
    onAdap() {
        let transform = this.getComponent(UITransform);
        if (!transform) transform = this.addComponent(UITransform);
        let winSize = JMUIUtil.getViewSize();
        let _xs = winSize.width / transform.width;
        let _ys = winSize.height / transform.height;
        let _bl: number;
        switch (this.adapType) {
            case JMAdapNodeType.AllScreen:
                transform.width = winSize.width;
                transform.height = winSize.height;
                break;
            case JMAdapNodeType.BangScreen:
                transform.height = winSize.height;
                if (JMUIUtil.isAllScreen()) {
                    transform.width = winSize.width - JMAdapNode.bangLength;
                } else {
                    transform.width = winSize.width;
                }
                break;
            case JMAdapNodeType.BgScale:
                _bl = _xs > _ys ? _xs : _ys;
                this.node.setScale(new Vec3(_bl, _bl, 1));
                break;
            case JMAdapNodeType.BgSize:
                _bl = _xs > _ys ? _xs : _ys;
                transform.width = _bl * transform.width;
                transform.height = _bl * transform.height;
                break;
            case JMAdapNodeType.NodeAdapAll:
                _bl = _xs < _ys ? _xs : _ys;
                this.node.setScale(new Vec3(_bl, _bl, 1));
                if (_xs < _ys) {
                    transform.height += (winSize.height - transform.height * _bl) / _bl;
                } else {
                    transform.width += (winSize.width - transform.width * _bl) / _bl;
                }
                break;
            case JMAdapNodeType.NodeAdapBang:
                _bl = _xs < _ys ? _xs : _ys;
                this.node.setScale(new Vec3(_bl, _bl, 1));
                if (_xs < _ys) {
                    transform.height += (winSize.height - transform.height * _bl) / _bl;
                } else {
                    if (JMUIUtil.isAllScreen()) {
                        transform.width += (winSize.width - transform.width * _bl - JMAdapNode.bangLength) / _bl;
                    } else {
                        transform.width += (winSize.width - transform.width * _bl) / _bl;
                    }
                }
                break;
        }
    }
}