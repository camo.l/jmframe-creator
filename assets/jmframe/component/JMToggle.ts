import { Sprite, Enum, _decorator, Label, Color, Node, EventHandler, Scene, Canvas, SpriteFrame } from "cc";
import JMToggleGroup from "./JMToggleGroup";

export const xdTouchModel = Enum({
    TouchStart: 0,
    TouchEnd: 1,
})

export const xdStyleModel = Enum({
    Normal: 1,
    Sprite: 2,
})

export const xdEventType = Enum({
    Checked: 'toggle.checked',
})

const { ccclass, property } = _decorator;

@ccclass("JMToggle")
export default class JMToggle extends Sprite {
    /**更新状态触发 */
    public static upStatus = 'upStatus';
    //private _styleModel: number = xdStyleModel.Normal;
    //private _spr_checked: SpriteFrame = null;
    //private _spr_uncheck: SpriteFrame = null;
    @property({
        type: xdStyleModel,
        displayName: '样式模式',
    })
    styleModel = xdStyleModel.Normal;
    // set styleModel(value: number) {
    //     if (value === xdStyleModel.Normal) {
    //         this.spriteFrame = null;
    //         this.spr_uncheck = null;
    //         this.spr_checked = null;
    //     } else {
    //         this.node_checked && (this.node_checked.active = false);
    //         this.node_uncheck && (this.node_uncheck.active = false);
    //         this.node_uncheck = null;
    //         this.node_checked = null;
    //     }
    //     this._styleModel = value;
    // }
    // get styleModel(): number {
    //     return this._styleModel;
    // }
    @property({
        type: SpriteFrame,
        displayName: '选中纹理',
    })
    spr_checked: SpriteFrame = null;
    // set spr_checked(value: SpriteFrame) {
    //     if (!this.spriteFrame || this.checked) {
    //         this.spriteFrame = this.spr_checked;
    //     }
    //     this._spr_checked = value
    // }
    // get spr_checked(): SpriteFrame {
    //     return this._spr_checked;
    // }
    @property({
        type: SpriteFrame,
        displayName: '未选中纹理',
    })
    spr_uncheck: SpriteFrame = null;
    // set spr_uncheck(value: SpriteFrame) {
    //     if (!this.spriteFrame || !this.checked) {
    //         this.spriteFrame = this.spr_uncheck;
    //     }
    //     this._spr_uncheck = value
    // }
    // get spr_uncheck(): SpriteFrame {
    //     return this._spr_uncheck;
    // }
    @property({
        type: Node,
        displayName: '选中节点',
    })
    node_checked: Node = null;
    @property({
        type: Node,
        displayName: '未选中节点',
    })
    node_uncheck: Node = null;
    @property({
        type: Label,
        displayName: '显示文本',
    })
    lbl_display: Label = null;
    @property({
        displayName: '选中文本颜色',
    })
    clr_checked = new Color(255, 255, 255);
    @property({
        displayName: '未选中字体颜色',
    })
    clr_uncheck = new Color(214, 214, 214);
    @property({
        displayName: '启用文本控制',
    })
    enableLabel = false;
    @property
    protected _checked: boolean = false;
    @property({
        displayName: '默认选中',
    })
    set checked(value: boolean) {
        if (this.styleModel === xdStyleModel.Normal) {
            this.node_checked && (this.node_checked.active = value);
            this.node_uncheck && (this.node_uncheck.active = !value);
        } else {
            if (value) {
                this.spriteFrame = this.spr_checked;
            } else {
                this.spriteFrame = this.spr_uncheck;
            }
        }
        this._checked = value;
        this.updateNodeColor()
    }
    get checked(): boolean {
        return this._checked;
    }
    @property
    @property({
        type: xdTouchModel,
        displayName: '触摸模式',
    })
    touchModel = xdTouchModel.TouchEnd;
    @property({
        type: EventHandler
    })
    checkedEvents = [];
    @property({
        displayName: '是否交互',
    })
    _interactable = true;

    set interactable(value: boolean) {
        this._interactable = value;
        this.grayscale = value ? false : true;
    }
    get interactable(): boolean {
        return this._interactable;
    }

    public onlyFlag: number = null;
    public parent: JMToggleGroup = null;

    onLoad() {
        this.initViewToTableView();
        this.initViewToGroup();
        this.initViewToStyleModel();
    }

    onEnable() {
        super.onEnable();

        if (this.touchModel === xdTouchModel.TouchStart) {
            this.node.on(Node.EventType.TOUCH_START, this.onTouchEvent, this);
        }
        else if (this.touchModel === xdTouchModel.TouchEnd) {
            this.node.on(Node.EventType.TOUCH_END, this.onTouchEvent, this);
        }
        this.node.on(xdEventType.Checked, this.onEmitEvent, this);
    }

    onDisable() {
        super.onDisable();

        if (this.touchModel === xdTouchModel.TouchStart) {
            this.node.off(Node.EventType.TOUCH_START, this.onTouchEvent, this);
        }
        else if (this.touchModel === xdTouchModel.TouchEnd) {
            this.node.off(Node.EventType.TOUCH_END, this.onTouchEvent, this);
        }
        this.node.off(xdEventType.Checked, this.onEmitEvent, this);
    }

    initViewToGroup() {

        var envState = false;
        var node = this.node.parent
        do {
            if (node instanceof Scene || node.getComponent(Canvas)) {
                break;
            }
            var group = node.getComponent(JMToggleGroup);
            if (group) {
                envState = true;
                group.joinToggle(this);
                this.setStatus(this.checked);
                break;
            }
            node = node.parent;
        } while (node);
    }

    initViewToTableView() {
        // 找出iViewCell类
        var group = null;
        var node = this.node;
        //do {
        //if (node instanceof Scene || node.getComponent(Canvas)) {
        //return;
        //}
        var cell = node.getComponent('xdViewCell');
        if (cell) {
            group = cell.node.parent;
            return;
        }
        //node = node.parent;
        //} while (node);

        if (group) {
            this.setOnlyFlag(group.tag);
        }
    }

    initViewToStyleModel() {
        // 初始化显示状态
        if (this.styleModel === xdStyleModel.Normal) {
            this.node_checked && (this.node_checked.active = this.checked);
            this.node_uncheck && (this.node_uncheck.active = !this.checked);
        } else {
            if (this.checked && this.spr_checked) {
                this.spriteFrame = this.spr_checked;
            }
            else if (!this.checked && this.spr_uncheck) {
                this.spriteFrame = this.spr_uncheck;
            }
        }
        this.updateNodeColor()
    }

    onEmitEvent(ev: Event) {
        if (!this.checked) {
            this.setStatus(true);
            // Component.EventHandler.emitEvents(this.checkedEvents);
            for (var i = 0; i < this.checkedEvents.length; i++) {
                var handler = this.checkedEvents[i];
                EventHandler.emitEvents([handler], {
                    target: this.node
                }, handler.customEventData);
            }

            if (!ev) {//界面默认选中特殊处理
                let group = this.parent;
                if (group && !group.allowMultiple) {
                    let _ev = {}
                    _ev['target'] = this.node
                    group.onToggleChange(_ev as Event)
                }
            }
        }
    }

    onTouchEvent(ev: Event) {
        if (!this.interactable) {
            return;
        }
        //ev.stopPropagation();

        var group = this.parent;

        // 多选模式, 任何状态都响应
        if (!group || group.allowMultiple) {
            // xdSoundManager.instance().playEffect({ path: 'resources:xdframe/sound/ui_click' })
            this.setStatus(!this.checked);
            EventHandler.emitEvents(this.checkedEvents, ev);
            if (group) {
                group.onToggleChange(ev)
            }
        }
        // 单选模式, 仅对选中响应
        else if (!this.checked) {
            // xdSoundManager.instance().playEffect({ path: 'resources:xdframe/sound/ui_click' })
            this.setStatus(true);
            EventHandler.emitEvents(this.checkedEvents, ev);
            if (group) {
                group.onToggleChange(ev)
            }
        }
    }

    setOnlyFlag(flag: number) {
        this.onlyFlag = flag;
    }

    setStatus(checked: boolean) {
        checked = !!checked;

        if (this._checked == checked) {
            return;
        }

        if (this.styleModel === xdStyleModel.Normal) {
            this.node_checked && (this.node_checked.active = checked);
            this.node_uncheck && (this.node_uncheck.active = !checked);
        } else {
            if (checked) {
                this.spriteFrame = this.spr_checked;
            } else {
                this.spriteFrame = this.spr_uncheck;
            }
        }
        if (this.enableLabel && this.lbl_display) {
            this.lbl_display.color = checked ? this.clr_checked : this.clr_uncheck
        }
        this._checked = checked;
        this.node.emit(JMToggle.upStatus);
    }
    /**
     * 解决label未执行start设置颜色失败问题
     */
    updateNodeColor() {
        if (this.enableLabel && this.lbl_display) {
            this.lbl_display.color = this._checked ? this.clr_checked : this.clr_uncheck
        }
    }
}
