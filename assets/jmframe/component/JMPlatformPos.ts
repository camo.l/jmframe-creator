import { _decorator, Component, Vec3 } from 'cc';
import { JMPlatform, JMSys } from '../JMSys';
const { ccclass, property } = _decorator;
/**平台坐标自适应
 * 这个类主要用于小Q 和微信小程序自动调整坐标使用
 */
@ccclass('JMPlatformPos')
export class JMPlatformPos extends Component {
    @property
    WXGAME: boolean = true;

    @property({ type: Vec3 })
    WXGAME_POS: Vec3 = new Vec3();

    start() {
        switch (JMSys.platform) {
            case JMPlatform.WXGAME: if (this.WXGAME) this.node.position = this.WXGAME_POS; break;
            default:
                return false;
        }
    }
}
