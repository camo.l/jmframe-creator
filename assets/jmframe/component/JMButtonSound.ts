import { _decorator, Component, Enum } from 'cc';
const { ccclass, property } = _decorator;

/**
 * openSound: 打开音效
 * hideSound: 关闭音效
 */
const SoundModel = Enum({
    openSound: 0,
    hideSound: 1,
});

/**按钮音效控制 */
@ccclass('JMButtonSound')
export default class JMButtonSound extends Component {
    @property({
        type: SoundModel,
        displayName: '界面音效模式',
    })
    soundModel = SoundModel.openSound;

    onEnable() {
    }

    getAudioUrl(): string {
        let url = '';
        switch (this.soundModel) {
            case SoundModel.hideSound:
                url = 'resources:jmframe/sound/click_close'
                break;
            case SoundModel.openSound:
                url = 'resources:jmframe/sound/click_open'
                break;
            default:
                break;
        }
        return url;
    }

}
