import { _decorator, Component, Node, Enum, UITransform, v3, CCFloat } from 'cc';
const { ccclass, property } = _decorator;

/**
 * 方向
 * Horizontal: 水平方向
 * Vertical: 垂直方向
 */
export const JMLayoutModel = Enum({
    Horizontal: 0,
    Vertical: 1,
});
/**
 * 对齐模式
 * Left靠左
 * Centern居中
 * Right靠右
 * Top靠上
 * Bottom靠下
 */
export const JMLayoutAlign = Enum({
    Top: 1,
    Left: 2,
    Center: 3,
    Right: 4,
    Bottom: 5,
})

@ccclass('JMLayout')
export class JMLayout extends Component {
    /**方向 */
    @property({
        type: JMLayoutModel,
        displayName: '模式方向',
        tooltip: 'Horizontal: 水平方向\nVertical: 垂直方向'
    })
    layoutModel = JMLayoutModel.Horizontal;
    /**对齐模式 */
    @property({
        type: JMLayoutAlign,
        displayName: '对齐模式',

    })
    layoutAlign = JMLayoutAlign.Center;
    /**间隔如果为0则选择第一个为计算公式 */
    @property({
        type: CCFloat,
        displayName: '间隔大小',
    })
    num_itemL: number = 0;

    start() {
        if (!this.node.children[0]) return;
        if (this.layoutModel == JMLayoutModel.Horizontal) {
            this._upWidthPos();
        } else if (this.layoutModel == JMLayoutModel.Vertical) {
            this._upHeightPos();
        }
    }
    private _upWidthPos() {
        let itemL = this.num_itemL || -this.node.children[0].getComponent(UITransform).width;
        let i, length, showNode: Array<Node> = []
        for (i = 0, length = this.node.children.length; i < length; i++) {
            let cnode = this.node.children[i];
            if (cnode.active) {
                showNode.push(cnode);
            }
        }
        for (i = 0, length = showNode.length; i < length; i++) {
            let node = showNode[i];
            switch (this.layoutAlign) {
                case JMLayoutAlign.Left: node.position = v3(itemL * (i + 0.5), 0, 0); break;
                case JMLayoutAlign.Center: node.position = v3(-itemL * (length / 2 - (i + 0.5)), 0, 0); break;
                case JMLayoutAlign.Right: node.position = v3(-itemL * (i + 0.5), 0, 0); break;
                default: continue;
            }
        }
    }
    private _upHeightPos() {
        let itemL = this.num_itemL || -this.node.children[0].getComponent(UITransform).height;
        let i, length, showNode: Array<Node> = []
        for (i = 0, length = this.node.children.length; i < length; i++) {
            let cnode = this.node.children[i];
            if (cnode.active) {
                showNode.push(cnode);
            }
        }
        for (let i = 0, length = showNode.length; i < length; i++) {
            let node = showNode[i];
            switch (this.layoutAlign) {
                case JMLayoutAlign.Top: node.position = v3(0, -itemL * (i + 0.5), 0); break;
                case JMLayoutAlign.Center: node.position = v3(0, itemL * (length / 2 - (i + 0.5)), 0); break;
                case JMLayoutAlign.Bottom: node.position = v3(0, itemL * (i + 0.5), 0); break;
                default: continue;
            }
        }
    }
}