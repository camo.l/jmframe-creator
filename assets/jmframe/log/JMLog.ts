import { JMPlatform, JMSys } from "../JMSys";

export { }
// 字体样式
enum JMLogFontStyle {
    /**警告字体颜色*/
    Warn = 'color:#e6b74d',
    /**异常字体颜色 */
    Error = 'color:#f00',
    /**通用字体颜色 */
    Adopt = 'color:#0fe029',
    /**淡绿字体颜色 */
    LightGreen = 'color:#31dcad',
    /**弱体字体颜色 */
    Slight = 'color:#999999',
};
class JMLog {
    public static log = console.log;
    public static warn = console.warn;
    public static error = console.error;
    public static log_sys = console.log;
    public static log_socket = console.log;
}
jm.Log = JMLog as any;
jm.LogFontStyle = JMLogFontStyle as any;

if (JMSys.platform == JMPlatform.JSB) {
    JMLog.log = JMLog.warn = JMLog.error = JMLog.log_sys = JMLog.log_socket = function () {
        var args = Array.prototype.slice.apply(arguments);
        for (var i = 0, length = args.length; i < length; i++) {
            if (typeof args[i] == 'object') {
                args[i] = JSON.stringify(args[i]);
            }
        }
        console.log.apply(console, args);
    }
}
