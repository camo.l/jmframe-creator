import { js } from "cc";
import protobuf from "protobufjs"
import { JMBaseSocket, JMBaseSocketDelegate, JMSocketState, JMWebSocket, JMWxSocket } from "../base/JMBaseSocket";
import JMSysEvents from "../event/JMSysEvents";
import { JMConfig } from "../JMConfig";
import { JMPlatform, JMSys } from "../JMSys";
//接受数据格式
type messageFormat = string | ArrayBuffer;

/**
 * socket 心跳
 */
class socketHeartbeat {
    public static retryMaxCount = 2;
    private socket: JMBaseSocket;
    //发送心跳数据
    private heartbeatDataSend: messageFormat = '{"cmd":1}';
    //接受心跳数据
    private heartbeatDataRecv: messageFormat = '{\n	"cmd" : 1\n}\n';
    //发送心跳时间
    private sendTime: number;
    //延时Ping
    private networkDelay: number = 0;;
    //定时器ID
    private intervalId: number;
    /**重试次数 心跳接受超时重新发送次数 总共2次 */
    private retryCount: number = 0;
    //超时定时器ID
    private overTimeId: number;
    constructor(socket: JMBaseSocket, sendData: messageFormat, recvData: messageFormat) {
        this.socket = socket;
        this.setHeartbeatData(sendData, recvData);
    }
    /**
     * 设置心跳数据
     * @param sendData 
     * @param recvData 
     */
    public setHeartbeatData(sendData: messageFormat, recvData: messageFormat) {
        if (sendData) {
            this.heartbeatDataSend = sendData;
        }
        if (recvData) {
            this.heartbeatDataRecv = recvData;
        }
    }
    /**
     * 获取网络延时值
     */
    public getNetworkDelay(): number {
        return this.networkDelay;
    }
    /**
     * 消息处理
     * @param data 
     */
    public onMessage(data: messageFormat): boolean {
        //清除超时定时器
        if (this.overTimeId) {
            clearTimeout(this.overTimeId);
            this.overTimeId = undefined;
        }
        this.retryCount = 0;
        //心跳消息记录一次网络延时
        if (data === this.heartbeatDataRecv) {
            this.networkDelay = Date.now() - this.sendTime;
            return true;
        }
        return false;
    }
    /**
     * 关闭心跳
     */
    public close() {
        if (this.intervalId) {
            clearInterval(this.intervalId);
            this.intervalId = undefined;
        }
        if (this.overTimeId) {
            clearTimeout(this.overTimeId);
            this.overTimeId = undefined;
        }
    }
    //开启心跳
    public open() {
        if (this.intervalId) {
            jm.Log.warn(`%c心跳定时器 [${this.socket.getName()}] 已存在`);
        } else {
            this.sendMsg();
            this.intervalId = setInterval(() => {
                this.sendMsg();
            }, JMConfig.socketHeartbeatInterval);
        }
    }
    //发送心跳数据
    private sendMsg() {
        if (this.socket.getState() == JMSocketState.OPEN) {
            this.retryCount++;
            this.socket.send(this.heartbeatDataSend);
            this.sendTime = Date.now();
            if (this.overTimeId) {
                clearTimeout(this.overTimeId);
                this.overTimeId = undefined;
            }
            //连接超时即关闭socket
            this.overTimeId = setTimeout(() => {
                this.overTimeId = undefined;
                //没有重试次数的情况下断开网络
                if (this.retryCount >= socketHeartbeat.retryMaxCount) {
                    this.retryCount = 0;
                    this.socket.close();
                } else {
                    this.sendMsg();
                }
            }, JMConfig.socketHeartbeatOverTime);
        } else {
            this.close();
            jm.Log.log_socket(`%c[${this.socket.getName()}] 未连接 发送心跳失败 关闭心跳定时器`, jm.LogFontStyle.Slight)
        }
    }
}
interface cerifyCoerceMsgIF {
    timeoutID: number,
    overtimeCount: number,
    data: messageFormat,
}
/**服务校验 */
class serviceCerify {
    /**发送超时时间 */
    private static sendOvertimeNum = 2000;
    /**发送超时次数 */
    private static sendOvertimeCount = 2;
    /**强制消息队列 重复发送事件id */
    private _coerceMsg: { [cmd: number]: cerifyCoerceMsgIF } = js.createMap();
    private _key: string;
    constructor(_key: string) {
        this._key = _key;
    }
    /**清除一个强制消息 */
    clearCoerceMsg(cmd) {
        if (this._coerceMsg[cmd]) {
            clearTimeout(this._coerceMsg[cmd].timeoutID);
            delete this._coerceMsg[cmd];
        }
    }
    /**关闭 */
    close() {
        for (let cmd in this._coerceMsg) {
            if (this._coerceMsg[cmd].timeoutID) {
                clearTimeout(this._coerceMsg[cmd].timeoutID);
            }
        }
        this._coerceMsg = {};
    }
    /**使用服务校验发送消息 */
    sendMsg(cmd: number, data: messageFormat) {
        if (this._coerceMsg[cmd]) {
            jm.Log.warn(`丢弃的消息 cmd:${cmd} data:`, data);
            return;
        };
        this._coerceMsg[cmd] = {
            timeoutID: undefined,
            overtimeCount: 0,
            data: data,
        }
        this._sendMsg(cmd, this._coerceMsg[cmd].data);
    }
    private _sendMsg(cmd: number, data: messageFormat) {
        let socket = JMSocket.getProperty(this._key).socket;
        this._coerceMsg[cmd].timeoutID = setTimeout(() => {
            this._coerceMsg[cmd].timeoutID = undefined;
            if (++this._coerceMsg[cmd].overtimeCount >= serviceCerify.sendOvertimeCount) {
                socket.close();
            } else {
                this._sendMsg(cmd, this._coerceMsg[cmd].data);
            }
        }, serviceCerify.sendOvertimeNum);
        socket.send(data);
    }
}
/**
 * 消息阻塞
 */
class serviceClog {
    /**阻塞注册cmd表 */
    private _serviceCmds: { [cmd in number | string]: number } = js.createMap();
    /**是否阻塞 */
    private _isClog: boolean = false;
    /**已经被阻塞的消息列表 */
    private _clogMsgs: Array<any> = [];
    private _key: string;
    private get property(): socketProperty {
        return JMSocket.getProperty(this._key);
    };
    constructor(_key: string) {
        this._key = _key;
    }
    /**开启阻塞 */
    openClog() {
        this._isClog = true;
    }
    /**
     * 关闭阻塞 会推送所有阻塞消息
     */
    closeClog() {
        this._isClog = false;
        this._serviceCmds = {};
        this.emitClogMsgs();
    }
    /**清理 */
    clear() {
        this._isClog = false;
        this._serviceCmds = {};
    }
    /**
     * 添加注册阻塞cmd
     * @param cmdObj 
     */
    addRegisterCmd(cmdObj: CMDFORMAT) {
        if (!this._serviceCmds[cmdObj.cmd]) {
            this._serviceCmds[cmdObj.cmd] = 0;
        }
        this._serviceCmds[cmdObj.cmd]++;
    }
    /**
     * 注册阻塞cmd列表
     * 注意：注册会累计引用次数，请避免重复注册
     * @param list 需要阻塞的cmd
     */
    registerCmds(list: Array<CMDFORMAT>) {
        for (let i = 0, length = list.length; i < length; i++) {
            this.addRegisterCmd(list[i]);
        }
    }
    /**
     * 释放注册阻塞cmd
     * @param cmdObj 
     */
    releaseCmd(cmdObj: CMDFORMAT) {
        this.releaseCmdByKey(cmdObj.cmd);
    }
    /**
     * 释放注册阻塞cmd列表
     * @param list 需要释放阻塞的cmd
     */
    releaseCmds(list: Array<CMDFORMAT>) {
        for (let i = 0, length = list.length; i < length; i++) {
            let key = list[i].cmd;
            if (this._serviceCmds[key]) {
                this._serviceCmds[key]--;
            }
            if (this._serviceCmds[key] <= 0) {
                delete this._serviceCmds[key];
            }
        }
        this.emitClogMsgs();
    }
    /**
     * 释放消息如果还有存在的阻塞不会推送消息
     */
    private releaseCmdByKey(key: number | string) {
        if (this._serviceCmds[key]) {
            this._serviceCmds[key]--;
        }
        if (this._serviceCmds[key] <= 0) {
            delete this._serviceCmds[key];
            this.emitClogMsgs();
        }
    }
    /**推送消息阻塞 */
    private emitClogMsgs() {
        let idx = this._clogMsgs.length - 1;
        while (idx >= 0) {
            let _obj = this._clogMsgs[idx];
            if (_obj) {
                //释放过程中cmd加入阻塞 停止释放
                if (this.isCmdClog(_obj.cmd)) {
                    --idx;
                    continue;
                }
                //开始释放
                this._clogMsgs.splice(idx, 1);
                //释放阻塞时间
                _obj.releaseClogTime = new Date().getTime();
                this.emitMsg(_obj);
            }
            --idx;
        }
    }
    /**推送消息 */
    private emitMsg(obj) {
        if (JMConfig.Debug) {
            if (obj.releaseClogTime && obj.insertClogTime) {
                let _ct = (obj.releaseClogTime - obj.insertClogTime) / 1000;
                jm.Log.log(`%c【${this._key}】阻塞释放：`, 'color:#ea681c', JSON.parse(JSON.stringify(obj)));
                jm.Log.log(`%c【${this._key}】阻塞时长：`, 'color:#ea681c', _ct + 's');
            } else {
                jm.Log.log(`%c【${this._key}】接收：`, 'color:#ea681c', JSON.parse(JSON.stringify(obj)));
            }
        }
        let cmdData: CMDFORMAT = {
            cmd: obj.cmd,
            socketName: this._key,
            type: 3,
        }
        //优先推送定义socket的事件
        if (!jm.Event.hasEventKey(cmdData)) {
            cmdData.socketName = null;
        }
        jm.Event.emit(cmdData, obj);
    }
    /**cmd是否阻塞 */
    private isCmdClog(key: number | string) {
        if (this._isClog) {
            if (this._serviceCmds && this._serviceCmds[key] > 0) {
                return true;
            }
        }
        return false;
    }
    /**
     * 释放所有阻塞列表
     */
    releaseAllCmds() {
        for (let i in this._serviceCmds) {
            this.releaseCmdByKey(i);
        }
    }
    /**解析消息 */
    parseMsg(data: messageFormat) {
        if (typeof data == 'string') {
            let json;
            try {
                json = JSON.parse(data);
            } catch (error) {
                jm.Log.error('未识别的消息包:' + data);
                return;
            }
            this.property.serviceCerify.clearCoerceMsg(json.cmd);
            /**阻塞的情况 */
            if (this.isCmdClog(json.cmd)) {
                //插入阻塞时间
                json.insertClogTime = new Date().getTime();
                if (JMConfig.Debug) {
                    jm.Log.log(`%c【${this._key}】阻塞：`, 'color:#ea681c', JSON.parse(JSON.stringify(json)));
                }
                this._clogMsgs.unshift(json);
                return;
            }
            this.emitMsg(json);
        } else if (data instanceof ArrayBuffer) {

        }
    }
}

/**
 * socket 代理事件
 */
class socketDelegate implements JMBaseSocketDelegate {
    private messageV: (data: messageFormat) => void;
    private _key: string;
    private get property(): socketProperty {
        return JMSocket.getProperty(this._key);
    };
    constructor(_key: string, messageV: (data: messageFormat) => void) {
        this._key = _key;
        this.messageV = messageV;
    }
    onOpen() {
        this.property.heartbeat.open();
        jm.Event.emit(JMSysEvents.SocketOpen, this._key);
    }
    onMessage(data: messageFormat) {
        //处理心跳消息
        if (this.property.heartbeat.onMessage(data)) {
            return;
        }
        //自定义消息解析器
        if (typeof this.messageV == 'function') {
            this.messageV(data);
        } else {
            this.property.serviceClog.parseMsg(data);
        }
    }
    /**是否定制消息接受 */
    isCustomMsg() {
        return typeof this.messageV == 'function';
    }
    onError(err: any) {
        this.property.serviceCerify.close();
        this.property.heartbeat.close();
        // this.property.serviceClog.clear();
        jm.Event.emit(JMSysEvents.SocketClose, this._key);
    }
    onClose(msg: string) {
        this.property.serviceCerify.close();
        this.property.heartbeat.close();
        // this.property.serviceClog.clear();
        jm.Event.emit(JMSysEvents.SocketClose, this._key);
    }
}
/**socket 属性 */
interface socketProperty {
    /**socket */
    socket: JMBaseSocket,
    /**心跳 */
    heartbeat: socketHeartbeat,
    /**服务阻塞 */
    serviceClog: serviceClog,
    /**代理 */
    delegate: socketDelegate,
    /**服务校验 */
    serviceCerify: serviceCerify,
}
/**
 * socket管理器
 */
export default class JMSocket {
    private static _property: { [name: string]: socketProperty } = js.createMap();
    /**
     * 连接socket 重连socket
     * @param name 名字
     * @param url 地址
     * @param messageV 自定义消息解析器
     * @param binaryType 消息类型 默认是 blob类型
     */
    public static connect(name: string, url: string, messageV?: (data: messageFormat) => void, binaryType?: 'blob'): socketProperty {
        let property = this._property[name];
        if (!property) {
            this._property[name] = property = { socket: null, heartbeat: null, serviceClog: null, delegate: null, serviceCerify: new serviceCerify(name) };
            let delegate = new socketDelegate(name, messageV);
            let ws;
            if (JMSys.platform == JMPlatform.WXGAME) {
                ws = new JMWxSocket(name, url, delegate);
            } else {
                ws = new JMWebSocket(name, url, delegate, binaryType);
            }
            property.socket = ws;
            property.heartbeat = new socketHeartbeat(ws, null, null);
            property.serviceClog = new serviceClog(name);
            property.delegate = delegate;
        }
        property.socket.setUrl(url);
        property.socket.connect();
        return property;
    }
    /**
     * 获取网络延时值
     * @param name 名字
     */
    public static getNetworkDelay(name: string): number {
        let property = this._property[name];
        if (property) {
            return property.heartbeat.getNetworkDelay();
        } else {
            jm.Log.warn(`%cheartbeat [${name}] 不存在`);
        }
        return 0;
    }
    /**
     * 设置心跳数据
     * @param name 
     * @param sendData 
     * @param recvData 
     */
    public static setHeartbeatData(name: string, sendData: messageFormat, recvData: messageFormat) {
        let property = this._property[name];
        if (property) {
            property.heartbeat.setHeartbeatData(sendData, recvData);
        } else {
            jm.Log.warn(`%cheartbeat [${name}] 不存在`);
        }
    }
    /**
     * 获取当前所有连接器属性
     */
    public static getAllProperty(): { [name: string]: socketProperty } {
        return this._property;
    }
    /**获取连接器属性 */
    public static getProperty(name: string): socketProperty {
        return this._property[name];
    }
    /**获取阻塞器 */
    public static getServiceClog(name: string): serviceClog {
        let property = this._property[name];
        if (property) {
            return property.serviceClog
        }
        return null;
    }
    /**
     * 关闭socket
     * @param name 
     */
    public static closeSocket(name: string) {
        let property = this._property[name];
        if (property) {
            property.socket.close();
            property.serviceCerify.close();
            property.heartbeat.close();
            property.serviceClog.clear();
        }
    }
    /**
     * 关闭所有连接
     */
    public static closeAllSocket() {
        for (const key in this._property) {
            this.closeSocket(key);
        }
    }
    /**
     * 发送消息
     * @param name 
     * @param data 
     * @param cmd
     */
    public static sendMsg(name: string, data: messageFormat, cmd: CMDFORMAT) {
        let property = this._property[name];
        if (!property) {
            jm.Log.warn(`%csocket [${name}] 不存在`);
            return;
        }
        if (JMConfig.Debug) {
            jm.Log.log(`%c【${name}】发送：`, jm.LogFontStyle.LightGreen, data);
        };
        /**只有使用默认消息处理才有效 */
        if (cmd.type == 4 && !property.delegate.isCustomMsg()) {
            property.serviceCerify.sendMsg(cmd.cmd, data);
        } else {
            property.socket.send(data);
        }
    }
}
