import { loader } from "cc";
import JMViewMgr from "../manager/JMViewMgr";
import JMDataUtil from "../utils/JMDataUtil";


export default class JMHttp {
    /**
     * 发送http消息
     * @param obj 
     *      url:地址
     *      method:请求类型 默认GET
     *      timeout:超时时间 ms 默认5000
     *      success:成功回调
     *      fail:失败回调
     *      ontimeout:超时回调
     *      data:发送数据
     *      isLoading:是否显示转圈 默认false
     *      loadingDelayed loading显示的延时 默认0.3s
     *      loadingDesc:转圈提示内容
     * 
        xdhttp.send({
            url: "",
            method: 'GET',
            success: (res?: any) => {

            },
            fail: (res?: any) => {

            },
            data: "",
        });
     */
    public static send(obj: {
        url: string,
        method: 'GET' | 'POST',
        timeout?: number,
        success?: (res?: any) => any,
        fail?: (res: 'errcode' | 'error' | 'timeout') => any,
        data?: string | Object,
        isLoading?: false | true,
        loadingDelayed?: number,
        loadingDesc?: string,
        responseType?: XMLHttpRequestResponseType,
    }) {
        //是否显示loading
        let isLoading = obj.isLoading;

        //成功回调
        let success = function (res) {
            let ishide = true;
            if (typeof obj.success == 'function') {
                ishide = !obj.success(res);
            }
            if (isLoading && ishide) {
                JMViewMgr.hideLoading(obj.url);
            }
        }
        //失败回调
        let fail = function (err) {
            let ishide = true;
            if (typeof obj.fail == 'function') {
                ishide = !obj.fail(err);
            }
            if (isLoading && ishide) {
                JMViewMgr.hideLoading(obj.url);
            }
        }
        let url = obj.url;
        if (isLoading) {
            let delayed = 0.3;
            if (typeof obj.loadingDelayed == 'number') delayed = obj.loadingDelayed;
            JMViewMgr.showLoading(obj.url, { delayed: delayed, msg: obj.loadingDesc });
        }
        if (jm.SafeMode) {
            url = url.replace('http://', 'https://');
        }
        if (obj.method == 'GET') {
            if (typeof obj.data == 'object') {
                for (var i in obj.data) url = JMDataUtil.addUrlParam(url, i, obj.data[i]);
            }
        }
        //发送http请求
        let xhr: XMLHttpRequest = loader.getXMLHttpRequest();
        if (obj.timeout) {
            xhr.timeout = obj.timeout;
        } else {
            xhr.timeout = 5000;
        }
        if (obj.responseType) xhr.responseType = obj.responseType;
        xhr.open(obj.method, url);
        xhr.onreadystatechange = (ev: Event) => {
            if (xhr.readyState != 4) {
                return;
            }
            if (xhr.status >= 200 && xhr.status < 300) {
                if (xhr.responseType == 'arraybuffer' || xhr.responseType == 'blob') {
                    success(xhr.response);
                } else {
                    success(xhr.responseText);
                }
            } else {
                fail('errcode');
            }
        }
        //错误
        xhr.onerror = () => {
            fail('error');
        }
        //超时
        xhr.ontimeout = () => {
            fail('timeout');
        }
        switch (obj.method) {
            case 'GET': xhr.send(); break;
            case 'POST':
                xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                if (typeof obj.data == 'object') {
                    var pars = [];
                    for (var k in obj.data) { pars[pars.length] = k + '=' + obj.data[k]; }
                    xhr.send(pars.join('&'));
                } else {
                    xhr.send(obj.data);
                }
                break;
        }
    }
}
