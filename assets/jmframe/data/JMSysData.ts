import { _decorator } from "cc";
import JMBaseData from "../base/JMBaseData";
const { ccclass } = _decorator;
/**
 * 音效数据
 */
interface SoundIF {
    musicVolume: number,//音乐（背景音乐）声音大小
    effectVolume: number,//音效声音大小
    musicOpen: boolean,//音乐 是否开启
    effectOpen: boolean,//音效 是否开启
    voiceOpen: boolean,//录音 是否开启
}

@ccclass('JMSysData')
export default class JMSysData extends JMBaseData {
    //声音属性
    private soundAttr: SoundIF;
    //音乐大小
    public set musicVolume(value: number) {
        this.soundAttr.musicVolume = value;
        this.saveLocalData('sound', this.soundAttr);
    }
    public get musicVolume() {
        return this.soundAttr.musicVolume;
    }
    //音效大小
    public set effectVolume(value: number) {
        this.soundAttr.effectVolume = value;
        this.saveLocalData('sound', this.soundAttr);
    }
    public get effectVolume() {
        return this.soundAttr.effectVolume;
    }
    //音乐是否开启
    public set musicOpen(value: boolean) {
        this.soundAttr.musicOpen = value;
        this.saveLocalData('sound', this.soundAttr);
    }
    public get musicOpen() {
        return this.soundAttr.musicOpen;
    }
    //音效是否开启
    public set effectOpen(value: boolean) {
        this.soundAttr.effectOpen = value;
        this.saveLocalData('sound', this.soundAttr);
    }
    public get effectOpen() {
        return this.soundAttr.effectOpen;
    }
    //录音是否开启
    public set voiceOpen(value: boolean) {
        this.soundAttr.voiceOpen = value;
        this.saveLocalData('sound', this.soundAttr);
    }
    public get voiceOpen() {
        return this.soundAttr.voiceOpen;
    }
    onInit() {
        super.onInit();
        let localSoundIF = this.getLocalData('sound') || {};
        this.soundAttr = {
            musicVolume: typeof localSoundIF.musicVolume == 'undefined' ? 1 : localSoundIF.musicVolume,
            effectVolume: typeof localSoundIF.effectVolume == 'undefined' ? 1 : localSoundIF.effectVolume,
            musicOpen: typeof localSoundIF.musicOpen == 'undefined' ? true : localSoundIF.musicOpen,
            effectOpen: typeof localSoundIF.effectOpen == 'undefined' ? true : localSoundIF.effectOpen,
            voiceOpen: typeof localSoundIF.voiceOpen == 'undefined' ? true : localSoundIF.voiceOpen,
        }
        jm.Log.log(this.soundAttr)
    }
}
