
import { game, systemEvent, SystemEvent, Game, EventKeyboard, KeyCode } from "cc";
import JMSingleton from "../base/JMSingleton";

export default class JMSysEvents extends JMSingleton {
    //socket 连接成功
    public static SocketOpen = 'sys.SocketOpen';
    //socket 连接断开
    public static SocketClose = 'sys.SocketClose';
    //切换前台
    public static GameShow = 'sys.EventShow';
    //切换后台
    public static GameHide = 'sys.EventHide';
    //小Q数据回调
    public static xiaoqGameConfigRecv = 'sys.xiaoqGameConfigRecv';
    /**场景切换事件 */
    public static SceneSwitch = 'sys.SceneSwitch';

    /**游戏后台的时间 */
    private _gameHideTime: number = 0;
    public onInit() {
        this.bindEvents();
    }
    public onRecycle() {
        game.targetOff(this);
        systemEvent.targetOff(this);
    }
    public bindEvents() {
        game.on(Game.EVENT_HIDE, this.onGameHide, this);
        game.on(Game.EVENT_SHOW, this.onGameShow, this);
        systemEvent.on(SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    }
    /**
     * 切换后台
     */
    public onGameHide() {
        jm.Log.log_socket(`%c切换后台`, jm.LogFontStyle.Slight);
        this._gameHideTime = Date.now();
        jm.Event.emit(JMSysEvents.GameHide);
    }
    /**
     * 切换前台
     */
    public onGameShow() {
        let _hideTime = Date.now() - this._gameHideTime;
        jm.Log.log_socket(`%c切换前台  ${_hideTime}ms`, jm.LogFontStyle.Slight);
        jm.Event.emit(JMSysEvents.GameShow, {
            hideTime: _hideTime,
        });
    }
    /**
     * 按下按键
     * @param event 
     */
    public onKeyDown(e: EventKeyboard) {
        switch (e.keyCode) {
            // case 32://Android 后退键
            case KeyCode.SPACE://Windows 空格键
                break;
        }
    }
}