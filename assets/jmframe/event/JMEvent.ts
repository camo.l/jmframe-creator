
import { js, Node } from "cc";
import { JMPlatform, JMSys } from "../JMSys";

/**
 * 事件对象
 */
export class JMElementEvent {
    callback: Function;
    target: Object;
    isOnce: boolean;
    constructor(callback: Function, target: Object, isOnce?: boolean) {
        this.callback = callback;
        this.target = target;
        this.isOnce = isOnce || false;
    }
};

class TargetEventMgr {
    private _allEventKeys: { [key: string]: boolean } = js.createMap();
    addKey(eventKey: string) {
        if (!this._allEventKeys[eventKey]) {
            this._allEventKeys[eventKey] = true;
        }
    }
    removeKey(eventKey: string) {
        delete this._allEventKeys[eventKey];
    }
    removeAllKeys() {
        this._allEventKeys = {};
    }
    getAllEventKeys(): { [key: string]: boolean } {
        return this._allEventKeys;
    }
}

const getTargetEventMgr = function (tag: number, target: Object, isOnce: Boolean = false): TargetEventMgr {
    var _target = target as any, key = '_targetEventMgr' + tag;
    key += isOnce ? 'once' : '';
    if (!_target[key]) {
        _target[key] = new TargetEventMgr();
    }
    return _target[key];
}

let idx = 0;
const getEventTag = function () {
    return ++idx;
}
export class JMBaseEvent {
    private _allEvents: { [key: string]: Array<JMElementEvent> } = js.createMap();
    private _tag: number;
    constructor() {
        this._tag = getEventTag();
    }
    public hasEventListener(eventKey: string, callback: Function, target: Object): boolean {
        const array = this._allEvents[eventKey];
        if (!array) {
            return false;
        }
        for (let i = array.length - 1; i >= 0; i--) {
            let element = array[i];
            if (!element) {
                array.splice(i, 1);
                continue;
            };
            if (element.callback === callback && element.target === target) {
                return true;
            }
        }
        return false;
    }
    /**
     * 是否存在事件key
     * @param eventKey 
     */
    public hasEventKey(eventKey: string): boolean {
        let key = eventKey;
        if (!key) {
            return false;
        }
        return !!this._allEvents[key];
    }
    /**
     * 监听事件
     * @param eventKey 事件名
     * @param callback 方法
     * @param target 绑定对象
     * @param isOnce 是否只执行一次
     */
    public on(eventKey: string, callback: Function, target: Object, isOnce?: boolean) {
        let key = eventKey;
        if (!key) {
            xd.Log.log_sys(eventKey, '不支持监听事件');
            return;
        }
        //避免相同的事件重复注册
        if (this.hasEventListener(eventKey, callback, target)) {
            return;
        }
        if (!this._allEvents[key]) {
            this._allEvents[key] = [];
        }
        //debug模式下开启事件校验防止意外出错 发布版本关闭提升性能
        if (xdconfig.Debug) {
            this._allEvents[key].forEach(element => {
                if (element.target === target && element.callback === callback) {
                    jm.Log.warn('事件：', eventKey, ' 已经被对象绑定过了:', target);
                }
            });
        }
        getTargetEventMgr(this._tag, target, isOnce).addKey(eventKey);
        this._allEvents[key].unshift(new JMElementEvent(callback, target, isOnce));
    }
    /**
     * 推送事件
     * @param eventKey 事件名
     * @param param 参数
     */
    public emit(eventKey: string, ...param: any) {
        let key = eventKey;
        if (!key) {
            jm.Log.log_sys(eventKey, '不支持推送事件');
            return false;
        }
        let array = this._allEvents[key];
        if (!array) return false;
        for (let i = array.length - 1; i >= 0; i--) {
            let element = array[i];
            if (!element) {
                getTargetEventMgr(this._tag, element.target).removeKey(eventKey);
                array.splice(i, 1);
                continue;
            };
            element.callback.call(element.target, ...param);
            if (element.isOnce) {
                getTargetEventMgr(this._tag, element.target, element.isOnce).removeKey(eventKey);
                array.splice(i, 1);
                continue;
            }
        }
        return true
    }
    /**
     * 取消监听
     * @param eventKey 事件名
     * @param callback 方法
     * @param target 绑定对象
     */
    public off(eventKey: string, callback: Function, target: Object) {
        let key = eventKey;
        if (!key) {
            jm.Log.log_sys(eventKey, '不支持取消监听');
            return;
        }
        let array = this._allEvents[key];
        if (!array) return;
        for (let i = array.length - 1; i >= 0; i--) {
            let element = array[i];
            if (element && element.callback === callback && element.target === target) {
                array.splice(i, 1);
            }
        }
        if (array.length === 0) {
            this._allEvents[key] = null;
            delete this._allEvents[key];
        }
    }
    /**
     * 取消绑定在对象上的所有事件
     * @param obj 绑定对象
     */
    public targetOff(obj: Object) {
        let targetOffFunc = (target: Object, isOnce: boolean = false) => {
            let eventKeys = getTargetEventMgr(this._tag, target, isOnce).getAllEventKeys();
            for (const key in eventKeys) {
                if (eventKeys[key]) {
                    const array = this._allEvents[key];
                    if (array) {
                        for (let i = array.length - 1; i >= 0; i--) {
                            let element = array[i];
                            if (element && element.target === target) {
                                array.splice(i, 1);
                            }
                        }
                        if (array.length == 0) {
                            delete this._allEvents[key];
                        }
                    }
                }
            }
            getTargetEventMgr(this._tag, target, isOnce).removeAllKeys();
        }
        targetOffFunc(obj);
        targetOffFunc(obj, true);
    }
    /**
     * 清理事件通过事件名
     * @param eventName 事件名
     */
    public clearEvent(eventKey: string) {
        let key = eventKey;
        if (!key) {
            jm.Log.log_sys(eventKey, ' 清理事件');
            return;
        }
        this._allEvents[key] = null;
        delete this._allEvents[key];
    }
    /**
     * 获取所有事件
     */
    public getAllEvents() {
        return this._allEvents;
    }
}

export class JMEvent {
    private static event = new JMBaseEvent();
    /**
     * 获取事件key值
     * @param eventKey 
     */
    private static _getKey(eventKey: string | CMDFORMAT): string {
        let key = '';
        if (typeof eventKey == 'string') {
            key = eventKey;
        } else if (typeof eventKey === 'object') {
            if (eventKey.cmd) {
                if (eventKey.type && eventKey.type == 2) {
                    return null;
                }
                key += 'ws';
                if (eventKey.socketName) {
                    key += '.' + eventKey.socketName;
                }
                key += '.' + eventKey.cmd;
            } else if (eventKey.action) {
                key += 'http';
                key += '.' + eventKey.action;
            }
        }
        return key;
    }
    /**
     * 是否存在事件key
     * @param eventKey 
     */
    public static hasEventKey(eventKey: string | CMDFORMAT): boolean {
        let key = this._getKey(eventKey);
        if (!key) {
            return false;
        }
        return this.event.hasEventKey(key);
    }
    /**
     * 监听事件
     * @param eventKey 事件名
     * @param callback 方法
     * @param target 绑定对象
     * @param isOnce 是否只执行一次
     */
    public static on(eventKey: string | CMDFORMAT, callback: Function, target: Object, isOnce?: boolean) {
        let key = this._getKey(eventKey);
        if (!key) {
            jm.Log.log_sys(eventKey, '不支持监听事件');
            return;
        }
        this.event.on(key, callback, target, isOnce);
    }
    /**
     * 推送事件
     * @param eventKey 事件名
     * @param param 参数
     */
    public static emit(eventKey: string | CMDFORMAT, ...param: any) {
        let key = this._getKey(eventKey);
        if (!key) {
            jm.Log.log_sys(eventKey, '不支持推送事件');
            return false;
        }
        return this.event.emit(key, ...param);
    }
    /**
     * 取消监听
     * @param eventKey 事件名
     * @param callback 方法
     * @param target 绑定对象
     */
    public static off(eventKey: string | CMDFORMAT, callback: Function, target: Object) {
        let key = this._getKey(eventKey);
        if (!key) {
            jm.Log.log_sys(eventKey, '不支持取消监听');
            return;
        }
        return this.event.off(key, callback, target);
    }
    /**
     * 取消绑定在对象上的所有事件
     * @param target 绑定对象
     */
    public static targetOff(target: Object) {
        this.event.targetOff(target);
    }
    /**
     * 清理事件通过事件名
     * @param eventName 事件名
     */
    public static clearEvent(eventKey: string | CMDFORMAT) {
        let key = this._getKey(eventKey);
        if (!key) {
            jm.Log.log_sys(eventKey, ' 清理事件');
            return;
        }
        this.event.clearEvent(key);
    }
    /**
     * 获取所有事件
     */
    public static getAllEvents() {
        return this.event.getAllEvents();
    }
}

export const xdInitEvent = function () {
    jm.Event = JMEvent;
    jm.ElementEvent = JMElementEvent;
    if (JMSys.platform == JMPlatform.JSB) {
        window['NativeToJs'] = (action, param) => {
            jm.Event.emit(action, param);
        }
    }
}