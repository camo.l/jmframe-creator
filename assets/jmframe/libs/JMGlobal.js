
//定义全局变量
if (CC_EDITOR) {
    window.jm = {};
} else {
    var _global = typeof window === 'undefined' ? global : window;

    _global.jm = _global.jm || {};
}
/**自动cmd */
var _cmdIndex = 1000;
jm.AudoCmd = function () {
    return _cmdIndex++;
}

/**
* 调用原生接口
* @param className 类名
* @param param 参数
* @returns 返回的是string字符串 为undefined表示接口不存在或者原生接口处理不符合规则
*/
jm.toNative = function (className, param) {
    if (!cc.sys.isNative) return 'fail';
    let paramStr, ret;
    if (typeof param == 'string') {
        paramStr = param;
    } else if (typeof param == 'object') {
        paramStr = JSON.stringify(param);
    } else if (typeof param == 'number') {
        paramStr = param.toString();
    }
    if (cc.sys.os == 'Android') {
        if (paramStr) {
            console.log(className, paramStr);
            ret = jsb.reflection.callStaticMethod('bridge/JsToJava', className, '(Ljava/lang/String;)Ljava/lang/String;', paramStr);
        } else {
            console.log(className);
            ret = jsb.reflection.callStaticMethod('bridge/JsToJava', className, '()Ljava/lang/String;');
        }
    } else if (cc.sys.os == 'iOS') {
        if (paramStr) {
            console.log(className, paramStr);
            ret = jsb.reflection.callStaticMethod('JsToOc', className + ':', paramStr);
        } else {
            console.log(className);
            ret = jsb.reflection.callStaticMethod('JsToOc', className);
        }
    }
    return ret;
}