import { sys } from "cc";
import { JMPlatform, JMSys, JMSysOs } from "../JMSys";
import JMMD5 from "../libs/JMMd5";
var hexcase = 0;  /* hex output format. 0 - lowercase; 1 - uppercase        */
var b64pad = ""; /* base-64 pad character. "=" for strict RFC compliance   */
var chrsz = 8;  /* bits per input character. 8 - ASCII; 16 - Unicode      */

/*
 * These are the functions you'll usually want to call
 * They take string arguments and return either hex or base-64 encoded strings
 */
function hex_sha1(s: string): string { return binb2hex(core_sha1(str2binb(s), s.length * chrsz)); }
function b64_sha1(s: string): string { return binb2b64(core_sha1(str2binb(s), s.length * chrsz)); }
function str_sha1(s: string): string { return binb2str(core_sha1(str2binb(s), s.length * chrsz)); }
function hex_hmac_sha1(key: string, data: string): string { return binb2hex(core_hmac_sha1(key, data)); }
function b64_hmac_sha1(key: string, data: string): string { return binb2b64(core_hmac_sha1(key, data)); }
function str_hmac_sha1(key: string, data: string): string { return binb2str(core_hmac_sha1(key, data)); }

/*
 * Calculate the SHA-1 of an array of big-endian words, and a bit length
 */
function core_sha1(x: number[], len: number): number[] {
    /* append padding */
    x[len >> 5] |= 0x80 << (24 - len % 32);
    x[((len + 64 >> 9) << 4) + 15] = len;

    var w = Array(80);
    var a = 1732584193;
    var b = -271733879;
    var c = -1732584194;
    var d = 271733878;
    var e = -1009589776;

    for (var i = 0; i < x.length; i += 16) {
        var olda = a;
        var oldb = b;
        var oldc = c;
        var oldd = d;
        var olde = e;

        for (var j = 0; j < 80; j++) {
            if (j < 16) w[j] = x[i + j];
            else w[j] = rol(w[j - 3] ^ w[j - 8] ^ w[j - 14] ^ w[j - 16], 1);
            var t = safe_add(safe_add(rol(a, 5), sha1_ft(j, b, c, d)),
                safe_add(safe_add(e, w[j]), sha1_kt(j)));
            e = d;
            d = c;
            c = rol(b, 30);
            b = a;
            a = t;
        }

        a = safe_add(a, olda);
        b = safe_add(b, oldb);
        c = safe_add(c, oldc);
        d = safe_add(d, oldd);
        e = safe_add(e, olde);
    }
    return Array(a, b, c, d, e);

}

/*
 * Perform the appropriate triplet combination function for the current
 * iteration
 */
function sha1_ft(t: number, b: number, c: number, d: number): number {
    if (t < 20) return (b & c) | ((~b) & d);
    if (t < 40) return b ^ c ^ d;
    if (t < 60) return (b & c) | (b & d) | (c & d);
    return b ^ c ^ d;
}

/*
 * Determine the appropriate additive constant for the current iteration
 */
function sha1_kt(t: number): number {
    return (t < 20) ? 1518500249 : (t < 40) ? 1859775393 :
        (t < 60) ? -1894007588 : -899497514;
}

/*
 * Calculate the HMAC-SHA1 of a key and some data
 */
function core_hmac_sha1(key: string, data: string): number[] {
    var bkey = str2binb(key);
    if (bkey.length > 16) bkey = core_sha1(bkey, key.length * chrsz);

    var ipad = Array(16), opad = Array(16);
    for (var i = 0; i < 16; i++) {
        ipad[i] = bkey[i] ^ 0x36363636;
        opad[i] = bkey[i] ^ 0x5C5C5C5C;
    }

    var hash = core_sha1(ipad.concat(str2binb(data)), 512 + data.length * chrsz);
    return core_sha1(opad.concat(hash), 512 + 160);
}

/*
 * Add integers, wrapping at 2^32. This uses 16-bit operations internally
 * to work around bugs in some JS interpreters.
 */
function safe_add(x: number, y: number): number {
    var lsw = (x & 0xFFFF) + (y & 0xFFFF);
    var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
    return (msw << 16) | (lsw & 0xFFFF);
}

/*
 * Bitwise rotate a 32-bit number to the left.
 */
function rol(num: number, cnt: number): number {
    return (num << cnt) | (num >>> (32 - cnt));
}

/*
 * Convert an 8-bit or 16-bit string to an array of big-endian words
 * In 8-bit function, characters >255 have their hi-byte silently ignored.
 */
function str2binb(str: string): number[] {
    var bin = Array();
    var mask = (1 << chrsz) - 1;
    for (var i = 0; i < str.length * chrsz; i += chrsz)
        bin[i >> 5] |= (str.charCodeAt(i / chrsz) & mask) << (32 - chrsz - i % 32);
    return bin;
}

/*
 * Convert an array of big-endian words to a string
 */
function binb2str(bin: number[]): string {
    var str = "";
    var mask = (1 << chrsz) - 1;
    for (var i = 0; i < bin.length * 32; i += chrsz)
        str += String.fromCharCode((bin[i >> 5] >>> (32 - chrsz - i % 32)) & mask);
    return str;
}

/*
 * Convert an array of big-endian words to a hex string.
 */
function binb2hex(binarray: number[]): string {
    var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
    var str = "";
    for (var i = 0; i < binarray.length * 4; i++) {
        str += hex_tab.charAt((binarray[i >> 2] >> ((3 - i % 4) * 8 + 4)) & 0xF) +
            hex_tab.charAt((binarray[i >> 2] >> ((3 - i % 4) * 8)) & 0xF);
    }
    return str;
}

/*
 * Convert an array of big-endian words to a base-64 string
 */
function binb2b64(binarray: number[]): string {
    var tab = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    var str = "";
    for (var i = 0; i < binarray.length * 4; i += 3) {
        var triplet = (((binarray[i >> 2] >> 8 * (3 - i % 4)) & 0xFF) << 16)
            | (((binarray[i + 1 >> 2] >> 8 * (3 - (i + 1) % 4)) & 0xFF) << 8)
            | ((binarray[i + 2 >> 2] >> 8 * (3 - (i + 2) % 4)) & 0xFF);
        for (var j = 0; j < 4; j++) {
            if (i * 8 + j * 6 > binarray.length * 32) str += b64pad;
            else str += tab.charAt((triplet >> 6 * (3 - j)) & 0x3F);
        }
    }
    return str;
}


export default class JMDataUtil {
    private static _key: string = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    /**哈希值 */
    public static hex_sha1(str: string) {
        return hex_sha1(str);
    }
    public static str_sha1(str: string) {
        return str_sha1(str);
    }
    public static b64_sha1(str: string) {
        return b64_sha1(str);
    }
    /**gcj02坐标转百度坐标 */
    public static gcj02_To_Bd09(gg_lon, gg_lat) {
        let pi = 3.141592653589793 * 3000.0 / 180.0;
        let x = gg_lon, y = gg_lat;
        let z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * pi);
        let theta = Math.atan2(y, x) + 0.000003 * Math.cos(x * pi);
        let bd_lon = z * Math.cos(theta) + 0.0065;
        let bd_lat = z * Math.sin(theta) + 0.006;
        return {
            bd_lon: bd_lon,
            bd_lat: bd_lat
        };
    }
    /**秒转 天 时 分 秒*/
    public static secondsToDHMS(s: number) {
        let _d = Math.floor(s / (60 * 60 * 24));
        let _h = Math.floor((s / (60 * 60)) % 24);
        let _ms = Math.floor((s / 60) % 60);
        let _s = Math.floor(s % 60);
        let str = '';
        if (_d > 0) {
            str += _d + '天';
        }
        if (_h > 0) {
            str += _h + '时';
        }
        if (_ms > 0) {
            str += this.prefixInteger(_ms) + '分';
        }
        str += this.prefixInteger(_s) + '秒';
        return str;
    }
    /**秒转 时 分 秒*/
    public static secondsToHMS(s: number) {
        let _h = Math.floor((s / (60 * 60)));
        let _ms = Math.floor((s / 60) % 60);
        let _s = Math.floor(s % 60);
        return this.prefixInteger(_h) + ':' + this.prefixInteger(_ms) + ':' + this.prefixInteger(_s);
    }
    /**获取现在的时间 */
    public static getNowTime(): string {
        let _time = new Date();
        let _y = _time.getFullYear();
        let _m = _time.getMonth() + 1;
        let _d = _time.getDate();
        let _h = _time.getHours();
        let _ms = _time.getMinutes();
        let _s = _time.getSeconds();
        return _y + '-' + _m + '-' + _d + ' ' + _h + ':' + _ms + ':' + _s;
    }
    /*判断obj是否为一个整数*/
    public static isInteger(obj: number) {
        return Math.floor(obj) === obj;
    }
    /**
    * 将一个浮点数转换成整数，返回整数和倍数
    * 如 3.14 》》314 倍数是100
    *
    */
    public static toInteger(floatNum: number) {
        var ret = { times: 1, num: 0 };
        //是整数
        if (this.isInteger(floatNum)) {
            ret.num = floatNum;
            return ret;
        }
        var strfi = floatNum + '';
        //查找小数点的下标
        var dotPos = strfi.indexOf('.');
        //获取小数的位数
        var len = strfi.substr(dotPos + 1).length;
        //Math.pow(10,len)指定10的len次幂。
        var time = Math.pow(10, len);
        //将浮点数转化为整数
        var intNum = parseInt((floatNum * time + 0.5).toString(), 10);
        ret.times = time;
        ret.num = intNum;
        return ret;
    }
    /**
     * 计算公式 = - * /
     * 三个参数分别是要运算的两个数和运算符
     * 因为js 0.1+0.2运算是错误的
     * @param a 
     * @param b 
     * @param op 运算符 
     */
    public static operation(a: number, b: number, op: '+' | '-' | '*' | '/') {
        var o1 = this.toInteger(a);
        var o2 = this.toInteger(b);
        var n1 = o1.num;
        var n2 = o2.num;
        var t1 = o1.times;
        var t2 = o2.times;
        var max = t1 > t2 ? t1 : t2;
        var result = null;
        switch (op) {
            case '+':
                if (t1 === t2) {
                    result = n1 + n2;
                } else if (t1 > t2) {
                    result = n1 + n2 * (t1 / t2);
                } else {
                    result = n1 * (t2 / t1) + n2;
                }
                return result / max;
            case '-':
                if (t1 === t2) {
                    result = n1 - n2;
                } else if (t1 > t2) {
                    result = n1 - n2 * (t1 / t2);
                } else {
                    result = n1 * (t2 / t1) - n2;
                }
                return result / max;
            case '*':
                result = (n1 * n2) / (t1 * t2);
                return result;
            case '/':
                result = (n1 / n2) / (t2 / t1);
                return result;
        }
    }
    /**
     * 数字转化为万 千万 忆
     * @param num 数字
     * @param w 位数
     */
    public static numConversion(num: number, w: number = 2): string {
        num = num || 0;
        let _f = '';
        if (num < 0) {
            _f = '-';
        }
        num = Math.abs(num);
        let cz = function (n, a, b) {
            let bb = JMDataUtil.operation(n / a, Math.floor(n / a), '-');
            let pow = Math.pow(10, b)
            if (bb * pow > 0) {
                return (Math.floor(JMDataUtil.operation(n / a, pow, '*')) / pow).toString();
            } else {
                return Math.floor(n / a);
            }
        }
        if (typeof num == 'string') {
            num = parseInt(num);
        }
        if (num >= 100000000) {
            return _f + cz(num, 100000000, w) + '亿';
        }
        // if (num >= 10000000) {
        //     return _f + cz(num, 10000000, w) + '千万';
        // }
        if (num >= 10000) {
            return _f + cz(num, 10000, w) + '万';
        }
        return _f + num.toString();
    }
    /**
     * 前置加0
     * @param num 数字
     * @param length 最大位数
     */
    public static prefixInteger(num: number, length: number = 2) {
        return (Array(length).join('0') + num).slice(-length);
    }
    /**
     * 判断当前字符串是否是URL
     * @param url 
     */
    public static isURL(url: string): boolean {
        return typeof url == 'string' && (url.indexOf('http://') != -1 || url.indexOf('https://') != -1);
    }
    /**
     * 截取字符串
     * @param str 截取字符
     * @param maxC 最大截取数
     * @param endstr 替换字符
     */
    public static captureStr(str: string, maxC: number = 6, endstr: string = ''): string {
        let _str = '';
        if (!str || str == '') return _str
        let len = 0;
        for (let i = 0; i < str.length; i++) {
            let c = str.charCodeAt(i);
            if ((c >= 0x0001 && c <= 0x007e) || (0xff60 <= c && c <= 0xff9f)) {
                len++;
            } else {
                len += 2;
            }
            if (len > 2 * maxC) {
                _str += endstr;
                break;
            }
            _str += str[i];
        }
        return _str;
    }
    /**
     * 为字符串去前后空格
     * @param text 字符串
     */
    public static trim(text) {
        if (typeof (text) == 'string') {
            return text.replace(/^\s*|\s*$/g, '');
        } else {
            return text;
        }
    }
    /**
     * 合并对象, 将模板对象的所有属性合并到源对象上
     * 注意: 源对象已有的属性不会被模板对象的属性覆盖, 且合并后源对象会被修改
     */
    public static merge(tempObj: object, sourceObj: object) {
        if (typeof sourceObj == 'undefined') {
            sourceObj = {};
        }
        for (var k in tempObj) {
            if (typeof sourceObj[k] === 'undefined') {
                sourceObj[k] = tempObj[k];
            }
        }
        return sourceObj;
    }
    /**
     * 
     * @param url 链接
     * @param key key
     * @param value 值
     */
    public static addUrlParam(url: string, key, value) {
        value = encodeURIComponent(value);
        if (/\?/g.test(url)) {
            var reg = new RegExp('[?|&]' + key + '=([^&]*)', 'g');
            var res = reg.exec(url);
            if (res) {
                url = url.substr(0, res.index) + res[0].replace(res[1], value) + url.substr(res.index + res[0].length);
            } else {
                url += "&" + key + "=" + value;
            }
        } else {
            url += "?" + key + "=" + value;
        }
        return url;
    }
    /**
     * md5加密
     * @param str 
     */
    public static md5(str: string): string {
        return new JMMD5().hex_md5(str);
    }
    /**
     * base64加密
     * @param str 
     */
    public static encodeBase64(input: string): string {
        input = escape(input);
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;
        while (i < input.length) {
            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);
            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;
            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }
            output = output +
                JMDataUtil._key.charAt(enc1) + JMDataUtil._key.charAt(enc2) +
                JMDataUtil._key.charAt(enc3) + JMDataUtil._key.charAt(enc4);
        }
        return output;

    }
    /**
     * base64解密
     * @param str 
     */
    public static decodeBase64(input: string): string {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;
        while (i < input.length) {
            enc1 = JMDataUtil._key.indexOf(input.charAt(i++));
            enc2 = JMDataUtil._key.indexOf(input.charAt(i++));
            enc3 = JMDataUtil._key.indexOf(input.charAt(i++));
            enc4 = JMDataUtil._key.indexOf(input.charAt(i++));
            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;
            output = output + String.fromCharCode(chr1);
            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }
        }
        output = unescape(output);
        return output;
    }
    /**
     * utf8 转 string
     * @param array Uint8Array
     */
    public static Utf8ArrayToStr(array: Uint8Array): string {
        let out, i, len, c;
        let char2, char3;

        out = "";
        len = array.length;
        i = 0;
        while (i < len) {
            c = array[i++];
            switch (c >> 4) {
                case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7:
                    // 0xxxxxxx
                    out += String.fromCharCode(c);
                    break;
                case 12: case 13:
                    // 110x xxxx   10xx xxxx
                    char2 = array[i++];
                    out += String.fromCharCode(((c & 0x1F) << 6) | (char2 & 0x3F));
                    break;
                case 14:
                    // 1110 xxxx  10xx xxxx  10xx xxxx
                    char2 = array[i++];
                    char3 = array[i++];
                    out += String.fromCharCode(((c & 0x0F) << 12) |
                        ((char2 & 0x3F) << 6) |
                        ((char3 & 0x3F) << 0));
                    break;
            }
        }
        return out;
    }
    public static utf8to16(str: string): string {
        var out, i, len, c;
        var char2, char3;
        out = "";
        len = str.length;
        i = 0;
        while (i < len) {
            c = str.charCodeAt(i++);
            switch (c >> 4) {
                case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7:
                    // 0xxxxxxx
                    out += str.charAt(i - 1);
                    break;
                case 12: case 13:
                    // 110x xxxx 10xx xxxx
                    char2 = str.charCodeAt(i++);
                    out += String.fromCharCode(((c & 0x1F) << 6) | (char2 & 0x3F));
                    break;
                case 14:
                    // 1110 xxxx 10xx xxxx 10xx xxxx
                    char2 = str.charCodeAt(i++);
                    char3 = str.charCodeAt(i++);
                    out += String.fromCharCode(((c & 0x0F) << 12) |
                        ((char2 & 0x3F) << 6) |
                        ((char3 & 0x3F) << 0));
                    break;
            }
        }
        return out;
    }
    /**
     * string 转 Uint8Array
     * @param string string
     */
    public static stringToUint8Array(string: string): Uint8Array {
        let pos = 0;
        const len = string.length;

        let at = 0;  // output position
        let tlen = Math.max(32, len + (len >> 1) + 7);  // 1.5x size
        let target = new Uint8Array((tlen >> 3) << 3);  // ... but at 8 byte offset

        while (pos < len) {
            let value = string.charCodeAt(pos++);
            if (value >= 0xd800 && value <= 0xdbff) {
                // high surrogate
                if (pos < len) {
                    const extra = string.charCodeAt(pos);
                    if ((extra & 0xfc00) === 0xdc00) {
                        ++pos;
                        value = ((value & 0x3ff) << 10) + (extra & 0x3ff) + 0x10000;
                    }
                }
                if (value >= 0xd800 && value <= 0xdbff) {
                    continue;  // drop lone surrogate
                }
            }

            // expand the buffer if we couldn't write 4 bytes
            if (at + 4 > target.length) {
                tlen += 8;  // minimum extra
                tlen *= (1.0 + (pos / string.length) * 2);  // take 2x the remaining
                tlen = (tlen >> 3) << 3;  // 8 byte offset

                const update = new Uint8Array(tlen);
                update.set(target);
                target = update;
            }

            if ((value & 0xffffff80) === 0) {  // 1-byte
                target[at++] = value;  // ASCII
                continue;
            } else if ((value & 0xfffff800) === 0) {  // 2-byte
                target[at++] = ((value >> 6) & 0x1f) | 0xc0;
            } else if ((value & 0xffff0000) === 0) {  // 3-byte
                target[at++] = ((value >> 12) & 0x0f) | 0xe0;
                target[at++] = ((value >> 6) & 0x3f) | 0x80;
            } else if ((value & 0xffe00000) === 0) {  // 4-byte
                target[at++] = ((value >> 18) & 0x07) | 0xf0;
                target[at++] = ((value >> 12) & 0x3f) | 0x80;
                target[at++] = ((value >> 6) & 0x3f) | 0x80;
            } else {
                // FIXME: do we care
                continue;
            }

            target[at++] = (value & 0x3f) | 0x80;
        }

        return target.slice(0, at);
    }
    /**
     * 根据身份证号计算年龄
     * @param UUserCard 身份证号
     */
    public static AgeIdCard(UUserCard: string): number {
        var myDate = new Date();
        var month = myDate.getMonth() + 1;
        var day = myDate.getDate();
        var age = myDate.getFullYear() - Number(UUserCard.substring(6, 10)) - 1;
        if (Number(UUserCard.substring(10, 12)) < month || Number(UUserCard.substring(10, 12)) == month && Number(UUserCard.substring(12, 14)) <= day) {
            age++;
        }
        return age;
    }
    /**
     * 获取设备唯一标识
     */
    private static _uuid: string;
    public static generateUUID(): string {
        if (this._uuid) return this._uuid;
        let uuid = "";
        if (JMSys.platform == JMPlatform.WEB || (
            JMSys.platform == JMPlatform.JSB && JMSys.os == JMSysOs.Windows
        ) || JMSys.platform == JMPlatform.WXGAME) {
            uuid = sys.localStorage.getItem('generateUUID');
            if (!uuid) {
                let d = new Date().getTime();
                uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                    let r = (d + Math.random() * 16) % 16 | 0;
                    d = Math.floor(d / 16);
                    return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
                });
                sys.localStorage.setItem('generateUUID', uuid);
            }
        } else if (JMSys.platform == JMPlatform.JSB) {
            uuid = jm.toNative('getUDID');
        }
        this._uuid = uuid;
        return this._uuid;
    }
    /**
     * 拷贝对象
     * @param obj 对象
     */
    public static copyObj<T>(obj: T): T {
        if (obj === null) {
            return null;
        }
        return JSON.parse(JSON.stringify(obj));
    }
    /**
     * 生成随机数 四舍五入计算
     * @param min 最小值
     * @param max 最大值
     */
    public static random(min: number, max: number): number {
        return Math.round(Math.random() * (max - min) + min);
    }
    /**
     * 复制内容
     * @param content 内容
     * @param callback 回调
     */
    public static toCopy(content: string, callback?: (isok: boolean) => void) {
        if (typeof callback != 'function') {
            callback = function (isok) { };
        }
        if (JMSys.platform == JMPlatform.JSB) {
            callback(!!jm.toNative('copyStr', content));
        } else if (JMSys.platform == JMPlatform.WXGAME) {
            wx.setClipboardData({
                data: content,
                success: () => {
                    callback(true);
                },
                fail: () => {
                    callback(false);
                }
            })
        } else if (sys.isBrowser) {
            if (!document.execCommand) {
                jm.Log.warn(`警告: 该浏览器不支持面板复制功能`);
            } else if (typeof ClipboardJS == 'undefined') {
                jm.Log.warn(`警告: 插件未载入, 复制失败!`);
            } else {
                ClipboardJS.copy(content, function (isok) {
                    if (!isok) {
                        jm.Log.warn(`警告: 复制失败了!`);
                        var div = document.createElement('div');
                        div.style.background = 'rgba(255, 255, 255, 0.0)';
                        div.style.position = 'absolute';
                        div.style.width = '100%';
                        div.style.height = '100%';
                        div.style.zIndex = '1000';
                        document.body.appendChild(div);

                        var div2 = document.createElement('div');
                        div2.style.background = 'rgba(255, 255, 255, 0.9)';
                        div2.style.position = 'absolute';
                        div2.style.left = '50%';
                        div2.style.top = '50%';
                        div2.style.marginLeft = '-100px';
                        div2.style.marginTop = '-30px';
                        div2.style.width = '200px';
                        div2.style.height = '60px';
                        div2.style.lineHeight = '60px';
                        div2.innerText = '点击复制内容~';
                        div2.style.color = '#000';
                        div2.style.borderRadius = '8px';
                        div.appendChild(div2);

                        div.addEventListener('click', function () {
                            document.body.removeChild(div);
                            ClipboardJS.copy(content, callback);
                        });
                    } else {
                        callback(true);
                    }
                });
            }
        }
    };
}

