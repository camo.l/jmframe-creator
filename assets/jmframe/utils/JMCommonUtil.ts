import { director, game, sys } from "cc";
import { JMPlatform, JMSys } from "../JMSys";
import JMSoundMgr from "../manager/JMSoundMgr";

export default class JMCommonUtil {
    /**振动 */
    public static vibrate() {
        switch (JMSys.platform) {
            case JMPlatform.JSB: break;
            case JMPlatform.WXGAME: wx.vibrateLong({}); break;
        }
    }
    /**
     * 退出游戏
     */
    public static exitGame(): boolean {
        if (JMSys.platform == JMPlatform.JSB) {
            game.end();
        } else if (JMSys.platform == JMPlatform.WXGAME) {
            wx.exitMiniProgram({});
        } else {
            jm.Log.warn('不支持退出游戏');
            return false;
        }
        return true;
    }
    /**
     * 重启游戏
     */
    public static restart() {
        JMSoundMgr.instance().pauseMusic()
        if (JMSys.platform == JMPlatform.JSB) {
            director.getScheduler().setTimeScale(1);
            game.restart();
        } else if (JMSys.platform == JMPlatform.WXGAME) {
            wx.getUpdateManager().applyUpdate();
        } else {
            location.href = location.href;
        }
    }
    /**
     * 打开URL
     * @param url 地址
     */
    public static openURL(url: string) {
        if (url.indexOf('http://') == -1 && url.indexOf('https://') == -1) {
            url = 'https://' + url;
        }
        if (JMSys.platform == JMPlatform.JSB) {
            sys.openURL(url);
        } else {
            window.location.href = url;
        }
    }
    /**
     * 版本比对
     * _v1 > _v2   :   1
     * _v1 = _v2   :   0
     * _v1 < _v2   :   -1
     * @param _v1 版本1
     * @param _v2 版本2
     */
    public static versionVS(_v1: string, _v2: string) {
        let _v1s = _v1.split('.');
        let _v2s = _v2.split('.');
        for (let i = 0; (i < _v1s.length || i < _v2s.length); ++i) {
            let _v1n = parseInt(_v1s[i] || '0');
            let _v2n = parseInt(_v2s[i] || '0');
            if (_v1n > _v2n) {
                return 1;
            } else if (_v1n < _v2n) {
                return -1;
            }
        }
        return 0;
    }
    /**
     * 计算两点之间的距离
     * @param Longitude1 经度1
     * @param Latitude1 纬度1
     * @param Longitude2 经度2
     * @param Latitude2 纬度2
     */
    public static getLByLongitudeLatitude(Longitude1: number, Latitude1: number, Longitude2: number, Latitude2: number): number {
        let rad = function (d) {
            return d * Math.PI / 180.0;
        }
        let EARTH_RADIUS = 6378137.0;
        let radLat1 = rad(Latitude1);
        let radLng1 = rad(Longitude1);
        let radLat2 = rad(Latitude2);
        let radLng2 = rad(Longitude2);
        let a = radLat1 - radLat2;
        let b = radLng1 - radLng2;
        let result = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2))) * EARTH_RADIUS;
        return result;
    }
}