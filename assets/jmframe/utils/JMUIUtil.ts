import { Node, Size, UITransform, find, Canvas, director, RenderTexture, game } from "cc";


export default class JMUIUtil {
    /**获取UI Canvas */
    public static getUICanvas(): Node {
        let node = find('Canvas');
        if (!node) {
            node = new Node();
            node.addComponent(UITransform);
            node.addComponent(Canvas);
            director.getScene().insertChild(node, 0);
        }
        return node;
    }
    /**
     * 获取视图大小
     */
    public static getViewSize(): Size {
        let transform = this.getUICanvas().getComponent(UITransform)
        return new Size(transform.width, transform.height);//view.getVisibleSizeInPixel();
    }
    /**
     * 是否是全面屏手机
     */
    public static isAllScreen(): boolean {
        let viewSize = this.getViewSize();
        return viewSize.width / viewSize.height >= 2.0;
    }
    /**
     * 判断节点是否可用
     * @param node 节点
     */
    public static nodeIsValid(node: Node): boolean {
        return node && node.isValid;
    }
    /**
     * 更新所有节点Group
     * @param node 操作节点
     * @param group 更新的group
     */
    public static upAllNodeGroup(node: Node, layer: number) {
        node.layer = layer;
        let children = node.children;
        if (children.length == 0) return;
        children.forEach(element => {
            this.upAllNodeGroup(element, layer);
        });
    }
    /**截屏base64格式 */
    public static capture(): string {
        // if (JMSys.platform == JMPlatform.WEB ||
        //     JMSys.platform == JMPlatform.JSB) {
        //     let node = xdUIUtil.getUICanvas();
        //     let renderTex: RenderTexture = new RenderTexture();
        //     let winSize = xdUIUtil.getViewSize();
        //     renderTex.initialize({
        //         width: Math.floor(winSize.width),
        //         height: Math.floor(winSize.height),
        //     })
        //     let camera = node.getComponent(Canvas);
        //     camera.targetTexture = renderTex;
        //     director.root.pipeline.render([camera.camera.view]);
        //     let data = this.copyRenderTex(renderTex);
        //     camera.targetTexture = null;
        //     return data;
        // }
        // return null;
        return null;
    }
    private static copyRenderTex(renderTex: RenderTexture): string {
        // let arrayBuffer = new ArrayBuffer(renderTex.width * renderTex.height * 4);
        // let region = new GFXBufferTextureCopy();
        // region.texOffset.x = 0;
        // region.texOffset.y = 0;
        // region.texExtent.width = renderTex.width;
        // region.texExtent.height = renderTex.height;
        // director.root.device.copyFramebufferToBuffer(renderTex.window.framebuffer, arrayBuffer, [region]);
        // if (JMSys.platform == JMPlatform.JSB) {
        //     let path = xdfile.getWritablePath() + 'capture.jpg';
        //     this.saveImageData(arrayBuffer, renderTex.width, renderTex.height, path);
        //     return path;
        // } else {
        //     return this.toB64(arrayBuffer);
        // }
        return null;
    }
    private static saveImageData(arrayBuffer: ArrayBuffer, w: number, h: number, path: string) {
        // let imageU8Data = new Uint8Array(arrayBuffer);
        // //翻转图片
        // let picData = new Uint8Array(w * h * 4);
        // let rowBytes = w * 4;
        // for (let row = 0; row < h; row++) {
        //     let srow = h - 1 - row;
        //     let start = srow * w * 4;
        //     let reStart = row * w * 4;
        //     // save the piexls data
        //     for (let i = 0; i < rowBytes; i++) {
        //         picData[reStart + i] = imageU8Data[start + i];
        //     }
        // }
        // jsb.saveImageData(picData, w, h, path);
    }
    private static toB64(arrayBuffer: ArrayBuffer): string {
        let canvas = document.createElement('canvas');
        let winSize = JMUIUtil.getViewSize();
        let width = canvas.width = Math.floor(winSize.width);
        let height = canvas.height = Math.floor(winSize.height);
        let ctx = canvas.getContext('2d');
        let imageU8Data = new Uint8Array(arrayBuffer);
        let rowBytes = width * 4;
        let rowBytesh = height * 4;
        for (let row = 0; row < rowBytesh; row++) {
            let sRow = height - 1 - row;
            let imageData = ctx.createImageData(width, 1);
            let start = sRow * rowBytes;
            for (let i = 0; i < rowBytes; i++) {
                imageData.data[i] = imageU8Data[start + i];
            }
            ctx.putImageData(imageData, 0, row);
        }
        var base64 = canvas.toDataURL("image/jpeg", 0.7); //压缩语句
        return base64;
    }
}
